![curvedtm](./curvedtm_logo.png)

## About
CurvedTM is a python package that extends [Tissue Miner](https://github.com/mpicbg-scicomp/tissue_miner) to calculate tissue and cell properties of nonplanar polygonal cell networks. You can test the package using the [example data set](#dataset).

## Features
* Combine the tissue heightmaps with the 2D polygonal cell network from Tissue Miner to create a curved polygonal mesh in 3D.
* Triangulate the polygonal network to obtain a triangular mesh.
* Use triangular mesh to calculate the following geometric properties
    - Area
    - Unit normal
    - Triangle elongation tensor
    - Integrated mean and Gaussian curvatures
    - Mean and Gaussian curvatures on triangles.
- Given the quantities defined on the triangles, calculate the area, elongation and curvatures of the cells.
- Coasegrain and smoothen tensors defined on the triangles and cells using a coordinate free method.
- Find nematic defects in the nematic field on the mesh using contour integration.

Cells colored by mean curvature            |  Cells colored by Gaussian curvature
:-------------------------:|:-------------------------:
<img src="./images/mean_curvature.png" alt="drawing" width="300"/>  |   <img src="./images/gaussian_curvature.png" alt="drawing" width="300"/>

Cell elongation magnitude (color) and direction (bar)            |  Smooth field of triangle elongation tensors
:-------------------------:|:-------------------------:
<img src="./images/cell_elongation.png" alt="drawing" width="300"/> | <img src="./images/nematic_smooth_field.png" alt="drawing" width="300"/>

## Installation
CurvedTM can be downloaded as a [zip file](https://gitlab.pks.mpg.de/paijmans/CurvedTM/-/archive/master/CurvedTM-master.zip) or by cloning the repository from the PKS GitLab server using 
```none
git clone https://gitlab.pks.mpg.de/paijmans/CurvedTM.git
```
## Dependencies
CurvedTM is a full Python package (Python >=3.6) and requires the packages
- Numpy
- Pandas
- SciPy
- Skimage
- Matplotlib (optional)
- [igraph for Python (optional)](https://anaconda.org/conda-forge/python-igraph) 

Note: Python igraph is only required for using the coarsegraining and defect detection on a curved mesh. Make sure you have the latest version of igraph: v0.8.2! To force the latets version:
```none
conda install -c conda-forge python-igraph=0.82
```

## Documentation
The definition of the quantities calculated as well as the tables generated by CurvedTM can be found in the [package documentation](https://gitlab.pks.mpg.de/paijmans/CurvedTM/-/blob/master/doc/curved_tissue_miner_implementation.pdf) which is located in the directory ``doc/`` in the root of this package.

Furtheremore, check out the example Jupyter notebooks (.ipyng), which help to use this package. There are notebooks
- ``EXAMPLE_CurvedTM``: Load and process the Tissue Miner database and create all tables from the curved polygonal network.
- ``EXAMPLE_smoothing_and_defect_detection``: Smooth tensors on the triangular mesh and detect nematic defects in the field of triangle elongation tensors.
- ``EXAMPLE_cell_alignment_correlation``: Calculate the alignment the elongation tensor of a cell with its neighbors. 
- ``EXAMPLE_curv_of_trianglular_mesh``: Calculate the mean and gaussian curvature of a stand-alone triangular mesh.

## Datasets
<a name="dataset"></a>
An example data set of an everting wing disk can be downloaded from the PKS GitLab 

> [https://gitlab.pks.mpg.de/paijmans/CurvedTM_dataset](https://gitlab.pks.mpg.de/paijmans/CurvedTM_dataset)

Further instructions are in the README of the repository.

## License
MIT License
