#!/home/paijmans/anaconda3/bin/python3
# -*- coding: utf-8 -*-

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore
import pickle
import argparse

from curvedtm import MMDef, MMTool, MMGeom, MMCurv


### Functions for VTK File output
def VTKOutput_Polygons(vertex_indices_per_face, f):
    """ Write polygon topology to vtk file.
    
    Parameters:
    -----------
    vertex_indices_per_face : dict {int:list}
        Dictionary mapping face_id to list of vertex ids.
    f : file pointer
        Pointer to vtk file.
        
    Returns:
    --------
    None - writes vtk file.
    """
    
    total_face_neighbour_number = 0
    for face_id, vertices_indx in vertex_indices_per_face.items():
        total_face_neighbour_number += len(vertices_indx)
    
    print('POLYGONS ' + str(len(vertex_indices_per_face)) + ' ' + str(len(vertex_indices_per_face) + total_face_neighbour_number), file=f)
    
    for face_id, vertices_indx in vertex_indices_per_face.items():
                      
        print(len(vertices_indx), end='', file=f)
        for vidx in vertices_indx:
            print('\t' + str(vidx), end='', file=f)
        print('', file=f)

    print('', file=f)


def VTKOutput_Vertices(vertices, f):
    """ Write vertex positions to vtk file.
    
    Parameters:
    -----------
    vertices : Pandas.DataFrame
        Table containing the vertex positions with columns ['x_pos', 'y_pos', 'z_pos'].
    f : file pointer
        Pointer to vtk file.
        
    Returns:
    --------
    None - writes vtk file.
    """
    
    for name in ['x_pos', 'y_pos', 'z_pos']:
        assert name in vertices.columns

    print('POINTS ' + str(len(vertices)) + ' double', file=f)

    print(vertices[['x_pos', 'y_pos', 'z_pos']].to_string(header=False, index=False), file=f)

    print('', file=f)   


def VTKOutput_FaceScalar(faces, dataName, f):
    """ Write scalar values for face coloring.
    
    Parameters:
    -----------
    faces : Pandas.DataFrame
        Table containing the scalar values for face coloring. Should contain dataName in columns.
    dataName : string
        Name of column in faces to use for coloring the faces.
    f : file pointer
        Pointer to vtk file.
        
    Returns:
    --------
    None - writes vtk file.
    """    
    
    if dataName not in faces.columns:
        return None
    
    print('SCALARS ' + dataName + ' double 1', file=f)
    print('LOOKUP_TABLE default', file=f)

    print(faces[dataName].to_string(header=False, index=False), file=f)

    print('', file=f)


def VTKOutput_FaceVector(faces, dataName, f):
    """ Write vectors to display on faces.
    
    Parameters:
    -----------
    faces : Pandas.DataFrame
        Table containing the vector components. Should contain dataName in columns.
    dataName : string
        List of vector name and name of vector components. See function 'generate_tensor_component_names()' for exact definition.
        Format:  [vector_name] + generate_tensor_component_names(...)
    f : file pointer
        Pointer to vtk file.
        
    Returns:
    --------
    None - writes vtk file.
    """    
    
    for name in dataName[1:]:
        if name not in faces.columns:
            return None

    vector_name = dataName[0]
    faces_column_names = dataName[1:]

    print('VECTORS ' + vector_name + ' double', file=f)

    print(faces[faces_column_names].to_string(header=False, index=False), file=f)

    print('', file=f)


def create_vtk_file(vertices, faces, vertex_indices_per_face, VTK_filename, face_scalar_name_list = [],
                    tensor_name_eigenvalue_propery_pair_list = [], data_type='CELL_DATA'):
    """ Create vtk file containg vertex positions, face, face colors and face vectors.
    
    Parameters:
    -----------
    vertices : Pandas.DataFrame
        Table containing the vertex positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    faces : Pandas.DataFrame
        Table containing data for face coloring and vectors. 
    vertex_indices_per_face : dict {int:list}
        Dictionary mapping face_id to list of vertex ids.
    VTK_filename : string
        Filename of vtk output file.
    face_scalar_name_list : list
        List of properties contained in faces.columns to output for face coloring.
    tensor_name_eigenvalue_propery_pair_list : list of pairs, [(), ...]
        List of pairs with (tensor name, eigenvalue propertie). 
        For a neamtic tensor with pair ('elongation_tensor', 'max'), the eigenvector with maximum eigenvalue is loaded and plotted.
    data_type : [CELL_DATA', 'POINT_DATA']
        Determines vtk poutput type:
            - CELL_DATA : Output are faces with coloring, vertices are of cell or triangle mesh.
            - POINT_DATA: Output are vectors, vertices are center of faces.
        
    Returns:
    --------
    True - VTK file successfully created on disk.
    """       
    
    # Create VTK file pointer.
    VTKfile = open(VTK_filename, 'w')

    ### Write VTK head to VTK file.
    print('# vtk DataFile Version 1.0\nCurved vertex model data\nASCII\n', file=VTKfile)
    print('DATASET POLYDATA', file=VTKfile)
    print('', file=VTKfile)

    ### Write network vertex positions to VTK file.
    VTKOutput_Vertices(vertices, f = VTKfile)

    ### Write Network faces to VTK file.
    if isinstance(vertex_indices_per_face, dict):
        VTKOutput_Polygons(vertex_indices_per_face, VTKfile)

    ### Write data for each faces (for coloring or to show vector) to vtk file.
    print(data_type + ' ' + str(len(faces)), file=VTKfile)

    # Write scalars for each face to color faces with data.
    for face_scalar_name in face_scalar_name_list:
        VTKOutput_FaceScalar(faces, face_scalar_name, VTKfile)

    # Write vectors for each face to show vector/nematic.
    for face_tensor_name, tensor_eigenvalue_property in tensor_name_eigenvalue_propery_pair_list:
        # Create list of vector name and component names.
        face_tensor_components_names = generate_tensor_component_names(face_tensor_name, tensor_eigenvalue_property)
        face_tensor_names_list = [face_tensor_name + '_' + tensor_eigenvalue_property] + face_tensor_components_names

        VTKOutput_FaceVector(faces, face_tensor_names_list, VTKfile)

    VTKfile.close()

    return True


### Functions to load MovieData data format.
def load_triangle_network_data(frameIdx, datapath_frames, network_type):
    """ Load vertices and faces of the triangle network from CurvedTM database.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load.
    datapath_frames : string
        Path to CurvedTM Database.
    network_type : ['subcellular_triangles', 'dual_triangles']
        Triangulation type to load.
        
    Returns:
    --------
    vertices : Pandas.DataFrame
        Table containing the vertex positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    vertex_indices_per_triangle_dict : dict {int:list}
        Dictionary mapping triangle_id to list of 3 vertex ids.   
    triangles : Pandas.DataFrame
        Table containing data for triangle coloring and vectors. 
    """   
    
    # Read data from picke file and put in dictionary data.
    triangles = MMTool.table_io_per_frame(datapath_frames, 'triangles', frameIdx, network_type=network_type)
    vertices  = MMTool.table_io_per_frame(datapath_frames, 'vertices',  frameIdx, network_type=network_type)

    if not isinstance(vertices, pd.DataFrame) or not isinstance(triangles, pd.DataFrame):
        return None, None, None

    triangle_vertex_indices = ['vertex_id_1', 'vertex_id_2', 'vertex_id_3']

    # Make dict of ordered vertex indices per triangle.
    vertex_indices_per_triangle_dict = dict(zip(triangles.index, triangles[triangle_vertex_indices].values))

    return vertices, vertex_indices_per_triangle_dict, triangles


def load_cell_network_data(frameIdx, datapath_frames):
    """ Load vertices and faces of the triangle network from CurvedTM database.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load.
    datapath_frames : string
        Path to CurvedTM Database.
        
    Returns:
    --------
    vertices : Pandas.DataFrame
        Table containing the vertex positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    vertex_indices_per_cell_dict : dict {int:list}
        Dictionary mapping cels to list of vertex ids.   
    cells : Pandas.DataFrame
        Table containing data for cells coloring and vectors. 
    """  
    
    #Read data from picke file convert to universal tables.
    cells    = MMTool.table_io_per_frame(datapath_frames, 'cells',    frameIdx, network_type = 'cells')
    vertices = MMTool.table_io_per_frame(datapath_frames, 'vertices', frameIdx, network_type = 'cells')
    dbonds   = MMTool.table_io_per_frame(datapath_frames, 'dbonds',   frameIdx, network_type = 'cells')
    sorted_dbonds_per_cell = MMTool.table_io_per_frame(datapath_frames, 'sorted_cell_dbonds_per_frame', frameIdx, network_type = 'cells')

    if not isinstance(cells, pd.DataFrame) or not isinstance(vertices, pd.DataFrame)\
            or not isinstance(dbonds, pd.DataFrame) or not isinstance(sorted_dbonds_per_cell, dict):
        return None, None, None

    #Convert dbond_ids to vertex indices
    vertex_indices_per_cell = [dbonds.loc[sorted_dbonds_per_cell[cell_id]]['vertex_id'].values for cell_id in cells.index]
    vertex_indices_per_cell_dict = dict(zip(cells.index, vertex_indices_per_cell))

    return vertices, vertex_indices_per_cell_dict, cells


def generate_tensor_component_names(tensor_name, tensor_eigenvalue_property):
    """ Generate systematic name of vector components for plotting.

    Parameters:
    -----------
    tensor_name : string
        Name of the tensor
    tensor_eigenvalue_property : string
        Name of eigenvalue property.
        
    Returns:
    --------
    tensor_component_names : list of 3 strings
        tensor_name + '_' + tensor_eigenvalue_property + '_' + ['x', 'y', 'z']
    """

    tensor_component_names =\
        [tensor_name + '_' + tensor_eigenvalue_property + '_x',
         tensor_name + '_' + tensor_eigenvalue_property + '_y',
         tensor_name + '_' + tensor_eigenvalue_property + '_z']

    return tensor_component_names


def make_tensor_table(frameIdx, datapath_frames, face_ids, tensor_name_eigenvalue_property_pair_list, network_type):
    """ Make table containing the components of vectors to plot.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load.
    datapath_frames : string
        Path to CurvedTM Database.
    face_ids : list
        List of face ids to plot vectors on.
    tensor_name_eigenvalue_propery_pair_list : list of pairs, [(), ...]
        List of pairs with (tensor name, eigenvalue propertie). 
        For a neamtic tensor with pair ('elongation_tensor', 'max'), the eigenvector with maximum eigenvalue is loaded and plotted.
    network_type : ['cells', 'subcellular_triangles', 'dual_lattice', 'patches_' + str(patch_size)]
        Name of the network type to plot vectors for.
        
    Returns:
    --------
    faces_tensor_tables : Pandas.DataFrame, size: 3 x len(tensor_name_eigenvalue_property_pair_list)
        Table containing vector components with column names [tensor_name + '_' + tensor_eigenvalue_property + '_x', ..., ]
    """  
        
    #Make empty table which will contain all requested tensors for each face.
    faces_tensor_tables = pd.DataFrame(index=face_ids)

    # Loop over all given table names to find the tensors and put them in faces_tensor_table
    for (tensor_name, tensor_eigenvalue_property) in tensor_name_eigenvalue_property_pair_list:
        
        #Load tensor from disk.
        faces_tensor = MMTool.table_io_per_frame(datapath_frames, tensor_name, frameIdx, network_type=network_type)

        if not isinstance(faces_tensor, np.ndarray):
            continue
            
        print('Loading tensor \'' + tensor_name + '\' (eigenvector loaded: \'' + tensor_eigenvalue_property + '\' eigenvalue) - network type:', network_type)            

        # Obtain eigenvectors and eigenvalues with required property from tensors.
        faces_tensor_eigenvalue, faces_tensor_eigenvector = \
            MMTool.calculate_tensor_eigenvalues_and_eigenvector(faces_tensor, tensor_eigenvalue_property)

        # Create DataFrame with components of unit-vector.
        column_names = generate_tensor_component_names(tensor_name, tensor_eigenvalue_property)
        faces_tensor_eigenvector_df = pd.DataFrame(faces_tensor_eigenvector, columns=column_names, index=face_ids)
        
        # Create DataFrame with eigenvalues.
        faces_tensor_eigenvalue_df  = pd.DataFrame(faces_tensor_eigenvalue, index=face_ids)
        faces_tensor_eigenvalue_df.columns = ['eigenvalue']
        
        # Set eigenvalue outliers to zero, and multiply with unit vectors.
        eigenvalue_outliers_idx = faces_tensor_eigenvalue_df[abs(faces_tensor_eigenvalue_df['eigenvalue']) > 1e6].index
        faces_tensor_eigenvalue_df.loc[eigenvalue_outliers_idx, 'eigenvalue'] = 0.0
        
        faces_tensor_eigenvector_df[column_names] = \
            faces_tensor_eigenvector_df[column_names].multiply(faces_tensor_eigenvalue_df['eigenvalue'], axis='index')

        faces_tensor_tables = pd.concat([faces_tensor_tables, faces_tensor_eigenvector_df], axis=1)

    return faces_tensor_tables


def load_cell_tensor_data(frameIdx, datapath_frames, tensor_name_eigenvalue_property_pair_list = [()]):
    """ Load vertices and vectors from CurvedTM database for plotting the cell vectors.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load.
    datapath_frames : string
        Path to CurvedTM Database.
    tensor_name_eigenvalue_propery_pair_list : list of pairs, [(), ...]
        List of pairs with (tensor name, eigenvalue propertie). 
        For a neamtic tensor with pair ('elongation_tensor', 'max'), the eigenvector with maximum eigenvalue is loaded and plotted.
        
    Returns:
    --------
    vertices : Pandas.DataFrame
        Table containing the cell center positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    faces_tensor_tables : Pandas.DataFrame, size: 3 x len(tensor_name_eigenvalue_property_pair_list)
        Table containing vector components.
    """  
    
    cells = MMTool.table_io_per_frame(datapath_frames, 'cells', frameIdx, network_type = 'cells')

    if not isinstance(cells, pd.DataFrame):
        return None, None

    # Create vertex positions for cell tensor.
    vertices = cells[['center_x', 'center_y', 'center_z']].copy()
    vertices.rename(columns={'center_x':'x_pos', 'center_y':'y_pos', 'center_z':'z_pos'}, inplace=True)

    #Calculate tensor components for each cell.
    cells_tensor_tables = make_tensor_table(frameIdx, datapath_frames, list(cells.index), tensor_name_eigenvalue_property_pair_list, 'cells')

    return vertices, cells_tensor_tables


def load_triangle_tensor_data(frameIdx, datapath_frames, tensor_name_eigenvalue_property_pair_list, network_type, triangulation_type='subcellular_triangles'):
    """ Load vertices and vectors from CurvedTM database for plotting the triangle vectors.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load.
    datapath_frames : string
        Path to CurvedTM Database.
    tensor_name_eigenvalue_propery_pair_list : list of pairs, [(), ...]
        List of pairs with (tensor name, eigenvalue propertie). 
        For a neamtic tensor with pair ('elongation_tensor', 'max'), the eigenvector with maximum eigenvalue is loaded and plotted.
    network_type : ['subcellular_triangles', 'dual_lattice', 'patches_' + str(patch_size)]
        Network type to plot
    triangulation_type : ['subcellular_triangles', 'dual_lattice']
        Triangulation type of the triangular mesh to load vertices and triangles from.
        
    Returns:
    --------
    center_vertices : Pandas.DataFrame
        Table containing the triangle centroid positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    faces_tensor_tables : Pandas.DataFrame, size: 3 x len(tensor_name_eigenvalue_property_pair_list)
        Table containing vector components.
    """  
            
    triangle_vertices = MMTool.table_io_per_frame(datapath_frames, table_name='vertices',  frameIdx=frameIdx, network_type=triangulation_type)
    triangles         = MMTool.table_io_per_frame(datapath_frames, table_name='triangles', frameIdx=frameIdx, network_type=triangulation_type)

    #Check if tables loaded correctly.
    if not isinstance(triangle_vertices, pd.DataFrame) or not isinstance(triangles, pd.DataFrame):
        return None, None

    #Get vertex positions for the 3 vertices of each triangle.
    triangles_vertex_ids = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].values.astype(dtype=int)
    triangles_vertex_ids = triangles_vertex_ids.reshape((3 * len(triangles)))
    triangles_vertices_xyz = triangle_vertices[['x_pos', 'y_pos', 'z_pos']].iloc[triangles_vertex_ids].values
    triangles_vertices_xyz = triangles_vertices_xyz.reshape((len(triangles), 3, 3))

    # Calculate the geometric center of each triangle, and make dataframe
    triangles_center_xyz = np.sum(triangles_vertices_xyz, axis=1) / 3.0
    center_vertices = dict(zip(triangles.index, triangles_center_xyz))
    center_vertices = pd.DataFrame.from_dict(data=center_vertices, orient='index')
    center_vertices.columns = ['x_pos', 'y_pos', 'z_pos']
    
    #Calculate tensor components for each triangle.
    triangles_tensor_tables = make_tensor_table(frameIdx, datapath_frames, list(triangles.index), tensor_name_eigenvalue_property_pair_list, network_type)

    return center_vertices, triangles_tensor_tables


def load_data_from_moviedata(frameIdx, datapath_frames, network_type, tensor_name_eigenvalue_propery_pair_list=[]):
    """ Load tables or tensors from MovieData database files for single frame.
    
    If len(tensor_name_eigenvalue_propery_pair_list) > 0, this function will return tensor data, else faces data.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load data from.
    datapath_frames : string
        Path to database files.
    network_type : ['cells', 'subcellular_triangles', 'dual_lattice', 'patches_' + str(patch_size)]
        Network type to load the data for.
    tensor_name_eigenvalue_propery_pair_list : list of tuples - [(string, string), ...]
        For each tensor to load, specify the name of the tensor and the eigenvector to load specified by a property of its eigenvalue.
    
    Returns:
    --------
    vertices : Pandas.DataFrame
        Table containing the vertex positions with columns ['x_pos', 'y_pos', 'z_pos'].  
    faces : Pandas.DataFrame
        Table containing data either face coloring or face vector components.
    vertex_indices_per_face : dict {int:list}
        Dictionary mapping face_id to list of vertex ids.   
    """

    vertices, faces, vertex_indices_per_face = None, None, None

    ### Load data from frames.
    if network_type == 'cells':
        if len(tensor_name_eigenvalue_propery_pair_list) == 0:
            # Cell vertices and face properties (cell face coloring).
            vertices, vertex_indices_per_face, faces = load_cell_network_data(frameIdx, datapath_frames)
        else:
            # Tensors centered on each cell face (cell centered vectors).
            vertices, faces = load_cell_tensor_data(frameIdx, datapath_frames, tensor_name_eigenvalue_propery_pair_list)
    
    elif network_type in ['dual_triangles', 'subcellular_triangles'] or 'patches' in network_type:
        if len(tensor_name_eigenvalue_propery_pair_list) == 0:
            # Triangle vertices and face properties (triangle face coloring).
            vertices, vertex_indices_per_face, faces = load_triangle_network_data(frameIdx, datapath_frames, network_type)
        else:
            # Tensors centered on each triangle face (or patches centered on each triangle).
            vertices, faces = load_triangle_tensor_data(frameIdx, datapath_frames, tensor_name_eigenvalue_propery_pair_list, network_type)
   
    return vertices, faces, vertex_indices_per_face


def load_data_from_external_table(frameIdx, external_table, faces, column_names_to_load):
    """ Load scalar data for coloring faces from external table.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load data from.
    external_table : Pandas.DataFrame
        Table with face data.
    faces : Pandas.DataFrame
        Faces table from CurvedTM.
    column_names_to_load : list
        List of column names to load from external_table.
        
    Returns:
    --------
    faces : Pandas.DataFrame
        Table containing data for face coloring.
    faces_idx_to_load : list
        List of face ids to load.    
    """
    
    external_data = external_table[external_table['frame'] == frameIdx]

    # When roi is specified, load the subset of table with that roi.
    if roi != '':
        external_data = external_data[external_data['roi'] == roi]       
        
    # Get indices of all the faces in this roi that are also specified in the tissue miner faces table.
    faces_idx_to_load = np.intersect1d(faces.index, external_data.index, assume_unique=False)
    
    if not (np.unique(faces.index) == np.unique(external_data.index)).all():
        print('Warning: Not all face ids are assigned a value in external file. Missing values set to 0.')
        print('Missing face ids:\n', np.setdiff1d(faces.index, external_data.index, assume_unique=False))
    
    for column_name in column_names_to_load:
        
        column_to_load = external_data.loc[faces_idx_to_load, column_name].copy()
        
        if column_to_load.isnull().any():
            print('Warning: Some values in column \'' + column_name + '\' of the external file are NaN! Missing values set to 0.')
            column_to_load[column_to_load.isnull()] = 0
        
        faces[column_name] = 0.0
        faces.loc[faces_idx_to_load, column_name] = column_to_load

    return faces, faces_idx_to_load


def process_single_frame_faces(frameIdx, datapath_frames, datapath_output, network_type, tables):
    """ Load data and produce VTK file of colored faces.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load data from.
    datapath_frames : string
        Path to database files.
    datapath_output : string
        Path to output of vtk files.
    network_type : ['cells', 'subcellular_triangles', 'dual_lattice']
        Network type to load the data for.
    tables : dict {table_name : table}    
        
    Returns:
    --------
    True if successfully created vtk file.
    """
    
    vertices, faces, vertex_indices_per_face = load_data_from_moviedata(frameIdx, datapath_frames, network_type)

    # If the faces table failed to load; continue to next frame.
    if not isinstance(vertices, pd.DataFrame) or not isinstance(faces, pd.DataFrame):
        print('Failed to load data for this frame. Skipping to next frame.')
        return False

    # If external table is loaded, add extra data to faces table.
    # If an roi is specified, reduce the faces to plot to this subset.
    if isinstance(tables['external_table'], pd.DataFrame):
        faces, faces_idx_to_load = load_data_from_external_table(frameIdx, tables['external_table'], faces, tables['external_scalar_names'])
        if tables['roi'] != '':
            faces = faces.loc[faces_idx_to_load]
            vertex_indices_per_face = dict((key, vertex_indices_per_face[key]) for key in faces_idx_to_load)

    # Set output filename for VTK file.
    output_filename = network_type + '_faces_' + "%04d" % frameIdx + '.vtk' 
    if tables['roi'] != '':
        output_filename = tables['roi'] + '_' + output_filename

    # Produce VTK file and return.
    filepath_output = os.path.join(datapath_output, output_filename)
    
    print('Writing', network_type, 'faces for frame', frameIdx, 'to file', output_filename)
    return create_vtk_file(vertices, faces, vertex_indices_per_face, filepath_output,
                           tables['scalar_properties_to_load'], [], 'CELL_DATA')


def process_single_frame_tensors(frameIdx, datapath_frames, datapath_output, network_type, tensor_name_eigenvalue_propery_pair_list):
    """ Load data and produce VTK file for face vectors.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to load data from.
    datapath_frames : string
        Path to database files.
    datapath_output : string
        Path to output of vtk files.
    network_type : ['cells', 'subcellular_triangles', 'dual_lattice', 'patches_' + str(patch_size)]
        Network type to load the data for.
    tensor_name_eigenvalue_propery_pair_list : list of tuples - [(string, string), ...]
        For each tensor to load, specify the name of the tensor and the eigenvector to load specified by a property of its eigenvalue.    
        
    Returns:
    --------
    True if successfully created vtk file.
    """
            
    if len(tensor_name_eigenvalue_propery_pair_list) == 0:
        return False
    
    vertices, faces, vertex_indices_per_face = load_data_from_moviedata(frameIdx, datapath_frames, network_type, tensor_name_eigenvalue_propery_pair_list)
    
    # If the faces table failed to load; continue to next frame.
    if not isinstance(vertices, pd.DataFrame) or not isinstance(faces, pd.DataFrame):
        print('Failed to load data for this frame. Skipping to next frame.')
        return False
    
    # Set output filename for VTK file and produce VTK file and return.
    output_filename = network_type + '_tensors_' + "%04d" % frameIdx + '.vtk' 
    filepath_output = os.path.join(datapath_output, output_filename)
    
    print('Writing tensors on mesh for frame', frameIdx, 'to file', output_filename)
    return create_vtk_file(vertices, faces, vertex_indices_per_face, filepath_output,
                           [], tensor_name_eigenvalue_propery_pair_list, 'POINT_DATA')


def loop_frames(frames_to_plot, datapath_frames, datapath_output, network_type, tables):
    """ Create VTK files for face coloring and face vectors for all required frames.
    
    Parameters:
    -----------
    frames_to_plot : list
        List of frame indices to create vtk files of.
    datapath_frames : string
        Path to database files.
    datapath_output : string
        Path to output of vtk files.
    network_type : ['cells', 'subcellular_triangles', 'dual_lattice', 'patches_' + str(patch_size)]
        Network type to load the data for.
    tables : dict {table_name : table}    
        
    Returns:
    --------
    None
    """
    
    for frameIdx in frames_to_plot:
                
        if 'patches' not in network_type:
            process_single_frame_faces(frameIdx, datapath_frames, datapath_output, network_type, tables)
        
        process_single_frame_tensors(frameIdx, datapath_frames, datapath_output, network_type, tables['tensor_properties_to_load'])
        

def parse_arguments(input_arguments):
    """ Argument parser for console arguments. """

    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--verbose", action="store_true", help="Run in verbose mode.")

    #choices = MMDef.valid_networks,
    parser.add_argument("-n", "--network_type", type=str, default='cells', 
                        help="Specify the network type for output. Options: " + str(MMDef.valid_networks) )

    parser.add_argument("-i", "--datapath_input", type=str, default='./' + MMDef.DATAPATH_ROOT_FRAMES + '/',
                        help="Path to the frames directory created by curved tissue miner. Default: " + './' + MMDef.DATAPATH_ROOT_FRAMES + '/')

    parser.add_argument("-o", "--datapath_output", type=str, default='./' + MMDef.DATAPATH_ROOT_FRAMES + '/vtk/',
                        help="Path to where the vtk files will be saved. Default: " + './' + MMDef.DATAPATH_ROOT_FRAMES + '/vtk/')

    parser.add_argument("-f", "--file_input", type=str, default=None,
                        help="Specify file for input data.")

    parser.add_argument("-r", "--roi_name", type=str, default='',
                        help="Specify the name of the ROI to plot.")
    
    parser.add_argument("-g", "--frames", nargs='*', type=int, default=[-1],
                        help="Specify the indices of the frames to load.")    

    parser.add_argument("-s", "--scalar_names", nargs='*', type=str, default=[],
                        help="Specify names of scalar data to plot. Multiple names separated by spaces.")

    parser.add_argument("-t", "--tensor_names", nargs='*', type=str, default=[],
                        help="Specify the names of tensor data to plot. Multiple names separated by spaces.")

    parser.add_argument("-e", "--tensor_eigenvalue_properties", nargs='*', type=str, default=[],
                        help="Specify eigenvalue properties of the tensor data. " +
                             "Each listed tensor name should have an eigenvalue property.")

    return parser.parse_args()


if __name__ == '__main__':
    """ Class for catching the execution of this script from console. """

    ### Parse input arguments from console.
    input_arguments = parse_arguments(sys.argv)

    datapath_frames       = input_arguments.datapath_input
    datapath_output       = input_arguments.datapath_output
    network_type          = input_arguments.network_type
    external_filename     = input_arguments.file_input
    external_scalar_names = input_arguments.scalar_names
    external_tensor_names = input_arguments.tensor_names
    external_tensor_eigpr = input_arguments.tensor_eigenvalue_properties
    frames_to_load        = input_arguments.frames
    roi                   = input_arguments.roi_name
    verbose_mode          = input_arguments.verbose

    if(verbose_mode):

        print('-----------------------------------')
        print('Path to tissue miner output data    :', datapath_frames)
        print('Path to outout for vtk files        :', datapath_output)
        print('Specified network_type              :', network_type)
        print('Specified external file to load     :', external_filename)
        print('Specified external scalar to load   :', external_scalar_names)
        print('Specified external tensor to load   :', external_tensor_names)
        print('Specified tensor eigenval props     :', external_tensor_eigpr)
        print('Specified indices of frames to load :', frames_to_load)
        print('Specified roi name                  :', roi)
        print('Verbosity                           :', verbose_mode)
        print('-----------------------------------')

        
    if (network_type not in MMDef.valid_networks) and ('patches' not in network_type):
        print('Error: Unknown network_type given: ' + network_type + '.\n Options:', MMDef.valid_networks)
        sys.exit()
        
    ### Create VTK output directory. Make directories if not existing yet.
    if not os.path.isdir(datapath_output):
        os.mkdir(datapath_output)

    ### If no frame indices are specified, try load load them from frames file.
    frames_to_plot = []
    if frames_to_load[0] == -1:
        movie_frames = MMTool.load_network_frames()
        #movie_frames = pd.DataFrame({'frame':[0]}) #Custome frame set.
        frames_to_plot = movie_frames['frame'].values
    else:
        frames_to_plot = frames_to_load
        # When 2 indices are given, assume range is intended.
        if len(frames_to_load) == 2:
            if frames_to_load[0] < frames_to_load[1]:
                print('Using frames', frames_to_load[0], '-', frames_to_load[1])
                frames_to_plot = list(range(frames_to_load[0], frames_to_load[1]+1))
        
    ### Make sure every externally specified tensor name is accompanied with tensor eigenval property.
    if len(external_tensor_names) > 0:
        if len(external_tensor_eigpr) == 0:
            external_tensor_eigpr = ['max'] * len(external_tensor_names)
            print('Eigenvalue properties set to max for all specified tensors.')

        assert len(external_tensor_names) == len(external_tensor_eigpr)
        for eigpr in external_tensor_eigpr:
            assert eigpr in ['max', 'min', 'abs_max', 'abs_min']

    ### Set default tensors and scalars to load.
    default_scalar_properties_to_load = ['area', 'neighbour_number', 'elongation_tensor_norm_max', 'elongation_tensor_norm', 'gaussian_curvature', 'mean_curvature']
    default_tensor_properties_to_load = [('elongation_tensor', 'max')]              
            
    ### Set default names of scalars to plot.
    scalar_properties_to_load = external_scalar_names
    for scalar_name in default_scalar_properties_to_load:
        if scalar_name not in external_scalar_names:
            scalar_properties_to_load += [scalar_name]
    
    ### Set tensor names to plot.
    tensor_properties_to_load = list(zip(external_tensor_names, external_tensor_eigpr))
    for tensor_name, tensor_eigval_prop in default_tensor_properties_to_load:
        if tensor_name not in external_tensor_names:
            tensor_properties_to_load += [(tensor_name, tensor_eigval_prop)]
                        
#    tensor_properties_to_load += [('curvature_tensor', 'non_zero_max')]
#    tensor_properties_to_load += [('curvature_tensor', 'non_zero_min')]
#    tensor_properties_to_load += [('shear_tensor', 'max')]  
    
    ### Load an external table from file if specified.
    external_table = False
    if external_filename != None:

        assert network_type in ['cells', 'subcellular_triangles']

        if not os.path.isfile(external_filename):
            print("Error: File " + external_filename + " does not exists.")
            sys.exit()

        with open(external_filename, "rb") as f:
            external_table = pickle.load(f)

        # Check if table contains roi data, and if the specified roi is in the table.
        # If no or only one roi is specified in the table, the roi contains all faces in the table.
        if 'roi' in external_table.columns:
            loaded_roi = np.unique(external_table['roi'].values)
            if len(loaded_roi) > 1:
                assert roi in loaded_roi
            else:
                roi = loaded_roi[0]
   
    ### Create tables dictionary.
    dict_keys   = ['scalar_properties_to_load', 'tensor_properties_to_load', 'external_table', 'external_scalar_names', 'roi']
    dict_tables = [scalar_properties_to_load, tensor_properties_to_load, external_table, external_scalar_names, roi]
    tables = dict(zip(dict_keys, dict_tables))
        
    ### Load data and create VTK files for all required movie frames.
    loop_frames(frames_to_plot, datapath_frames, datapath_output, network_type, tables)

              
