%\documentclass[a4paper,twoside,10pt]{revtex4-1}
%\documentclass[aps,preprint,onecolumn,a4paper,superscriptaddress,amsmath,amssymb]{revtex4}
%reprint / pre
%\documentclass[aps,preprint,superscriptaddress,amsmath,amssymb,pre,floatfix]{revtex4-1}
\documentclass[a4paper, twoside, 11pt, aps, superscriptaddress, amsmath, amsfonts, amssymb, pre,floatfix]{revtex4-1}

\usepackage[utf8]{inputenc}
\usepackage{xifthen}
\usepackage{graphicx}
\usepackage{hyphenat}
\usepackage{hhline}
\usepackage{upgreek}
\usepackage{bm}
\usepackage[table, svgnames, dvipsnames]{xcolor}

\input{my_macros.tex}

\begin{document}

\title{Implementation of the Curved Tissue Miner package in Python}
\begin{abstract}
 This document defines all quantities available in the curved Tissue Miner Python implementation.
\end{abstract}
\author{Joris \surname{Paijmans}}
\affiliation{Max Planck Institute for the Physics of Complex Systems, N\"othnitzer Str. 8, 01187 Dresden, Germany}

\maketitle
\tableofcontents
\newpage 


\section{Definition of the cellular network and its triangulations}
\subsection{Cellular network}
The cellular network consists of a set of connected polygons, which are defined by their 3D vertices $\bm{v}$ 
and the straight edges connecting them.
Each cell labeled $\alpha$ consists of $N_\alpha$ vertices, 
$\bm{v}^\alpha_i$, defining the network geometry and $N^\alpha$ directed bonds or dbonds, 
$b_e^\alpha$, defining the network topology. 
By definition, dbonds are directed in counter clockwise direction around the cell boundary, 
where we have chosen an orientation of the surface.
A dbond contains 4 topological quantities: The unique cell id it belongs to, the (right) vertex id it points outward from, 
the id of the next dbond in the cell and the conjugate dbond that lies on the other side of its edge.
Every dbond is unique to the cell but each vertex is usually shared by multiple cells.

For every cell, we define a centroid, $\bm{R}^\alpha$, an area, $A^\alpha$, and a unit normal vector, $\hat{\bm{N}}^\alpha$, as
\begin{equation}
 \bm{R}^\alpha = \frac{1}{N^\alpha}\sum_{i=1}^{N^\alpha} \bm{v}_i, \quad A^\alpha = \frac{1}{2}\sum_{i=1}^{N^\alpha} |\bm{n}^\alpha_i|,
 \quad \hat{\bm{N}}^\alpha = \frac{1}{|\sum_{i=1}^{N^\alpha} \bm{n}^\alpha_i|} \sum_{i=1}^{N^\alpha} \bm{n}^\alpha_i
 \elabel{cell_area_normal}
\end{equation}
where $\bm{n}^\alpha_i = (\bm{v}^\alpha_{i+1}-\bm{v}^\alpha_i) \times (\bm{R}^\alpha - \bm{v}^\alpha_i)$, 
is the normal vector on the triangle formed by one edge of the cell 
and the vector pointing from the cell vertex to the cell centroid. 
It has a norm equal to twice the area of the triangle.

\subsection{Dual lattice triangulation}
The dual lattice triangulation is created by connecting all the centroids, $\bm{R}_\alpha$,
of the cell network of cells that share an edge. 
This way every 3-way vertex in the cellular network has one triangle associated which geometry is defined
by the centroids on the 3 cells it is connected to.
At a vertex $v$ in the cellular network which has a vertex order, $M_v>3$, 
there is still is hole in the triangulation.
We therefore create a new centroid from the centroids of the cells that are connected to the vertex $v$
\begin{equation}
 \bm{R}^v = \frac{1}{M_v} \sum_{\alpha=1}^{M_v} \bm{R}^\alpha
 \elabel{cell_centroid}
\end{equation}
and create $N_v$ new triangles by connecting the neighboring centroids with the new one. 
This creates a complete triangulation of the cellular network with a geometry that depends only on the centroids
of the cellular network, $\{\bm{R}^\alpha, \bm{R}^\beta, \bm{R}^\gamma\}$.

\subsection{Subcellular triangulation}
The subcelluar triangulation is created by connecting the two consecutive vertices in every cell with the cell its
centroid, $\{\bm{v}_i, \bm{v}_{i+1}, \bm{R}^\alpha\}$.
This creates a complete triangulation that depends both on the vertex positions and the 
centroids of the cellular network.


\section{Definition of the triangle}
\subsection{Triangle vectors, area and orientation}
The geometry of a triangle is defined by its three vertices $\{\bm{R}_0, \bm{R}_1, \bm{R}_2\}$,
which define the triangle vectors $\bm{E}_1$, $\bm{E}_2$ and its unit normal vector $\hat{\bm{N}}$
\begin{equation}
 \bm{E}_1 = \bm{R}_1 - \bm{R}_0, \quad \bm{E}_2 = \bm{R}_2 - \bm{R}_0, \quad \hat{\bm{N}} = \frac{\bm{E}_1 \times \bm{E}_2}{|\bm{E}_1 \times \bm{E}_2|}.
 \elabel{triangle_vectors}
\end{equation}
These vectors also define the local covariant basis on the triangle. 
Using the triangle vectors, we can define the area of the triangle and the rotation angles
$\theta_x$ and $\theta_y$ that rotate a vector prallel to the z-axis to the vector normal to the plane of the triangle
\begin{equation}
 A = \frac{1}{2} \left| \bm{E}_1 \times \bm{E}_2 \right|, \quad \theta_x = -\arctan(N_y, N_z), \quad \theta_y = \arctan(N_x, 1-N_x^2).
 \elabel{triangle_area_normal}
\end{equation}
Here, $\arctan(x,y)$ is the element wised arc tangent of $x/y$ and $N_i$ is a component of the unit vector 
normal to the triangle plane.

\subsection{Triangle shape tensor and elongation tensor}
The triangle shape tensor, \statethr{}, maps an equilateral triangle with area $A_0$ lying in the xy-plane, 
defined by the vectors vectors $\bm{C}_i$, 
to an arbitrary triangle in the mesh, defined by the triangle vectors $\bm{E}_i$
\begin{equation}
 \bm{E}_i = \statethr{} \bm{C}_i.
\end{equation}
The vectors of the equilateral triangle are
\begin{equation}
 \bm{C}_1 = 
 \begin{pmatrix}
  l \\
  0 \\
  0 
 \end{pmatrix}, \quad
 \bm{C}_2 = 
 \begin{pmatrix}
  l/2 \\
  \sqrt{3}/2 l \\
  0 
 \end{pmatrix}, \quad
 \bm{C}_3 = 
 \begin{pmatrix}
  0 \\
  0 \\
  1 
 \end{pmatrix},
\end{equation}
where the side length $l=\sqrt{4A_0/\sqrt{3}}$ with $A_0=1$. 

The triangle shape tensor \statethr{} can be written in terms of the planar state tensor, \statetwo{}, as
\begin{equation}
 \statethr{} = \bm{R}_\mathrm{x}(\theta_x) \bm{R}_\mathrm{y}(\theta_y) \statetwo{},
 \elabel{triangle_state_tensor}
\end{equation}
where $\bm{R}_\mathrm{x}(\theta_x)$ and  $\bm{R}_\mathrm{y}(\theta_y)$ are rotations around the x and y axis, respectively.
The angles $\theta_x$ and $\theta_y$ are defined in \eref{triangle_area_normal}.
The planar triangle state tensor, which is represented as a 3x3 matrix with the '$z$' components zero, is defined as
\begin{equation}
 \statetwo{} = \sqrt{\frac{A}{A_0}} \bm{R}_\mathrm{z}(\phi) \exp(|\widetilde{Q}| \bm{\gamma}) \bm{R}_\mathrm{z}(-\phi) \bm{R}_\mathrm{z}(\theta_z)\,.
\end{equation}
Here, $\bm{\gamma}$ has $\{1,-1,0\}$ on the diagonal elements and zero everywhere else and
$\bm{R}_\mathrm{z}$ is the rotation matrix around the z -axis.
Furthermore, $A$ is the area of the triangle, $|\widetilde{Q}|$ the magnitude of the elongation tensor,
$\phi$ the direction of elongation in the xy-plane.
The elongation tensor in 3d, $\widetilde{\bm{Q}}$, and the elongation tensor in the xy-plane, $\widetilde{\bm{Q}}^\mathrm{planar}$, are 
\begin{equation}
 \widetilde{\bm{Q}}^\mathrm{3d} = \bm{R}_\mathrm{x}(\theta_x) \bm{R}_\mathrm{y}(\theta_y) \widetilde{\bm{Q}}^\mathrm{planar} \bm{R}_\mathrm{x}(-\theta_y) \bm{R}_\mathrm{y}(-\theta_x), \quad 
 \widetilde{\bm{Q}}^\mathrm{planar} = \bm{R}_\mathrm{z}(\phi) \exp(|\widetilde{Q}|\bm{\gamma})\bm{R}_\mathrm{x}(-\phi).
 \elabel{triangle_elongation_tensor_3D}
\end{equation}
The magnitude of elongation is defined as 
\begin{equation}
 |\widetilde{Q}| = \mathrm{arcsinh}\left( \frac{|B|}{\sqrt{|A|^2 - |B|^2}} \right).
 \elabel{triangle_elongation_norm}
\end{equation}
where $|A|$ and $|B|$ are the norms of the trace-antisymmetric and traceless-symmetric part of the 
planar triangle state tensor \statetwo{}, respectively.
The double angle of the direction of the elongation tensor is
\begin{equation}
 2\phi = \arctan2(Bxy, Bxx),
 \elabel{triangle_elongation_angle}
\end{equation}
with $B_{ij}$ the components of the nematic part of the triangle state tensor, $S$, and $\arctan2(x1, x2)$ 
the arc tangent of $x1/x2$ choosing the quadrant correctly.

\subsection{Triangle transformation and deformation tensors}
The triangle transformation tensor $\bm{M}$ maps a triangle at time $t$, 
$\{\bm{R}_0, \bm{R}_1, \bm{R}_2\}$, to the triangle at time $t + \delta t$, $\{\bm{R}'_0, \bm{R}'_1, \bm{R}'_2\}$
\begin{equation}
 \bm{E}'_i = \bm{M} \bm{E}_i.
\end{equation}
We use the transformation tensor to create a linear transformation that maps every point $\bm{x}$ 
lying on the triangle at time $t$ onto a point $\bm{x}'$ that lies on the triangle at time $t+\delta t$
\begin{equation}
 \bm{x}' = \bm{R}'_0 + \bm{M} (\bm{x} - \bm{R}_0).
\end{equation}
We can use the continuous velocity field, $\bm{v}=(\bm{x}'(\bm{x}) - \bm{x})/\delta t$,
on each triangle and calculate the velocity gradient tensor $V_{\alpha\beta}$,
\begin{equation}
  V_{\alpha\beta} = \partial_\alpha V_\beta = \left(M_{\beta\alpha} - \delta_{\alpha\beta}\right)/\delta t.
\end{equation}
We can derive the isotropic strain, the shear tensor and the anisotropic tensor
\begin{equation}
 V_{\gamma\gamma} = \mathrm{Tr}[V_{\alpha\beta}], \quad \widetilde{V}_{\alpha\beta} = \frac{1}{2}\left( V_{\alpha\beta} + V_{\beta\alpha} \right) - \frac{1}{3} V_{\gamma\gamma} \delta_{\alpha\beta}, \quad V^{*}_{\alpha\beta} = \frac{1}{2} \left( V_{\alpha\beta} - V_{\beta\alpha} \right).
\end{equation}




\section{Definition of the integrated mean and Gaussian curvatures and the curvature tensor.}
\subsection{Integrated curvature tensor on a continuous surface}
\subsubsection{curvature tensor}
For a continuous surface $\bm{X}(s_1,s_2)$, the curvature tensor 
\begin{equation}
 \bm{c} = c_{ij} \bm{e}^i\bm{e}^j, \quad c_{ij} = -\left(\partial_i\partial_j \bm{X}\right) \cdot\bm{n}. 
\end{equation}
The curvature tensor has two eigenvalues, or principle curvatures $\kappa_1$ and $\kappa_2$
and two corresponding eigenvectors or principle directions $\bm{t}_1$ and $\bm{t}_2$,
which are perpendicular, $\bm{t}_1 \cdot \bm{t}_2 = 0$.
We can find the mean and Gaussian curvatures from the curvature tensor $\bm{c}$ and the metric tensor 
$\bm{g} = g_{ij} \bm{e}^i \bm{e}^{j}$, with $g_{ij} = \bm{e}_i \cdot \bm{e}_j$,
\begin{equation}
 H = \tfrac{1}{2} \mathrm{Tr}[\bm{c}] = \tfrac{1}{2} (\kappa_1 + \kappa_2), \quad K = \frac{\mathrm{det}(\bm{c})}{\mathrm{det}(\bm{g})} = \kappa_1\kappa_2,
\end{equation}
where $\kappa_1$ and $\kappa_2$ are again the two principle curvatures
\begin{equation}
 \kappa_1 = H + \sqrt{H^2 - K}, \quad \kappa_2 =  H - \sqrt{H^2 - K}.
\end{equation}

\subsubsection{Integrated quantities}
The integrated mean and Gaussian curvatures $\bar{H}$ and $\bar{K}$, respectively, 
on a patch with area $S$ are defined as
\begin{equation}
 \bar{H} = \int_S dS' H, \quad \bar{K} = \int_S dS' K.
\end{equation}
We define the integrated curvature tensor $\bm{c}^\mathrm{int}$ as the curvature tensor, $\bm{c}$,
integrated over a surface $S$
\begin{equation}
 \bm{c}^\mathrm{int} = \frac{1}{S} \int_{S} dS' \bm{c}.
 \elabel{cont_C_int}
\end{equation}
Note that the trace of the integrated curvature tensor equals the 
integral of the trace of the normal curvature tensor, which is the integrated mean curvature $\bar{H}$,
but that this relation does not hold for the integrated Gaussian curvature $\bar{K}$.

\subsection{Integrated curvature tensor on a triangular surface}
For an oriented triangular surface, where every triangle $n$ is characterized by
two in plane triangle vectors $\bm{E}^n_{12}, \bm{E}^n_{13}$, and a unit normal vector $\bm{N}^n$,
the curvature tensor is either zero on the plane of each triangle or undefined 
on the edges and vertices of the triangle network. 
However, we can calculate the \emph{integrated} mean and Gaussian curvatures of a patch on the triangular surface
by smoothing the surface.
By smoothing we replace every edge by a small cylinder and every vertex by a spherical cap such that all discontinuous points on the surface are removed. 

\subsubsection{Surface integrated mean and Gaussian curvatures}
Contributions to the integrated mean curvature, $\bar{H}$, come from the surface edges.
Integrating over a cylindrical wedge of an edge labeled $e$ with length $L^e$, angle $\theta^e$ and radius $\epsilon$,
\begin{equation}
 \bar{H}^e = \int_L dL' \int_\theta rd\theta' H_\mathrm{cylinder} = L^e \epsilon\theta^e \frac{1}{2}\frac{1}{\epsilon} = \frac{1}{2} L^e \theta^e,
 \elabel{bond_int_mean_curvature}
\end{equation}
where $H_\mathrm{cylinder}=1/2\epsilon$ is the mean curvature on the surface of the cylinder.
The angle $\theta^e$ is the signed angle between the two normal vectors on the triangles 
that are connected to the edge $e$.
The sign of the angle is $\theta^e>0$ when the two triangles are convex and $\theta^e<0$ when the two triangles are concave on the oriented surface.

Contributions to the integrated Gaussian curvature, $\bar{K}$, come from surface vertices. 
For a patch $S$ on the surface, containing a single vertex $v$, the integrated Gaussian curvature, $\bar{K}^v$, is obtained by integrating over the patch and applying the Gauss-Bonnet theorem
\begin{equation}
 \bar{K}^v = \int_S dS' K = 2\pi - \oint_{\partial S} dl \, k_g = 2\pi - \sum_n \beta^n.
 \elabel{vertex_int_gaussian_curvature}
\end{equation}
Here $K$ is the Gaussian curvature of the surface, $\partial S$ the closed contour around the patch and $k_g$ is the geodesic curvature on the contour.
The angles $\beta^n$ are the angles of the corners of the triangles that coincide at the vertex $v$.
For $\bar{K}^v = 0$, the surface is flat, $\bar{K}^v > 0$ the surface has a peak or a valley 
and for $\bar{K}^v < 0$ the surface has a saddle point.

\subsubsection{Triangle mean and Gaussian curvatures}
Having defined the integrated mean and Gaussian curvatures on the triangular surface, we can define 
the mean and Gaussian curvature on every triangle $n$
\begin{equation}
  H^n = \frac{1}{2 A^n} \sum^3_{e=1} \bar{H}^e, \quad K^n = \sum^3_{v=1}\frac{\bar{K}^v}{B^v}
  \elabel{triangle_mean_gaussian_curvature}
\end{equation}
where $A^n$ is the area of a triangle and $B^v$ is the summed area of all the triangles connected 
to a vertex.
Note that we have some freedom in the above definitions for the curvatures on the triangle, 
but that for a patch $S$ of multiple triangles, they should always fulfill
\begin{equation}
 \bar{H} = \int_S dS' H = \sum_{n\in S} A^n H^n, \quad \bar{K} = \int_S dS' K = \sum_{n\in S} A^n K^n,
 \elabel{triangle_int_HK}
\end{equation}
with $\bar{H}$ and $\bar{G}$ as defined in \eref{bond_int_mean_curvature} and \eref{vertex_int_gaussian_curvature}, respectively.
We can find the principle curvatures on every triangle $\kappa^n_1$ and $\kappa^n_2$ as 
\begin{equation}
 \kappa^n_1 = H^n + \sqrt{(H^n)^2 - K^n} , \quad \kappa^n_2 = H^n - \sqrt{(H^n)^2 - K^n}.
 \elabel{triangle_int_k1k2}
\end{equation}
Note that contrary to the principle curvatures in the continuous case, 
the term under the square root can be negative, $(H^n)^2 - K^n < 0$.
In this cases we set both principle curvatures equal to $H$ in the implementation.

\subsubsection{Integrated curvature tensor on an edge}
We can extend our definition of the integrated curvature tensor in \eref{cont_C_int} to the 
triangular surface by again smoothing the surface such that every edge in the triangular network is a 
cylindrical wedge which has the curvature tensor of the cylinder. 
For an edge parallel to the z-axis, with one triangle in the x-z plane and the other triangle 
rotated an angle $\theta$ towards the y-z plane,
with length $L$, radius $\epsilon$, and applying \eref{cont_C_int}
\begin{equation}
 \bm{c}^\mathrm{int} = \int_{l'=0}^{L} dl' \int_{\theta'=0}^{\theta} d\theta' \bm{c}^\mathrm{cylinder} = \frac{L}{2} 
  \begin{pmatrix}
    2\theta - \mathrm{sin}(2\theta) & -\mathrm{sin}(\theta)^2 & 0 \\
    -\mathrm{sin}(\theta)^2 & 2\theta + \mathrm{sin}(2\theta) & 0 \\
    0 & 0 & 0
  \end{pmatrix}.
  \elabel{triangle_C_int_axis_z}
\end{equation}
Note that the integrated curvature tensor does not depend on the cylinder radius and that 
$\mathrm{Tr}\left[\bm{C}^\mathrm{int}\right] = 2\bar{H}^e$. 
Also, we did not normalize the tensor by the integrated area $L|\theta|\epsilon$,
because this area tends to zero and \eref{triangle_C_int_axis_z} would not be well defined.

To find $\bm{c}^\mathrm{int}$ for a bond with arbitrary orientation, we first write $\bm{c}^\mathrm{int}$
in the basis of its eigenvectors, denoted $c^\mathrm{int}_{uw}$, 
and then transforming the tensor to the global frame using its eigenvectors, $\bm{v}_u$.
For each bond labeled $e$, the eigenvalues of $\bm{c}^\mathrm{int}$ are
\begin{equation}
 w_1 = \tfrac{1}{2}L^e\left[\sin(\theta^e) + \theta^e \right], \quad 
 w_2 = \tfrac{1}{2}L^e\left[-\sin(\theta^e) + \theta^e \right], \quad
 w_3 = 0,
 \elabel{triangle_C_int_eigenvalues}
\end{equation}
and the corresponding eigenvectors are
\begin{equation}
 \bm{v}_1 = \hat{\bm{N}}^e \times \hat{\bm{L}}^e / |\hat{\bm{N}}^e \times \hat{\bm{L}}^e|, \quad
 \bm{v}_2 = \hat{\bm{N}}^e, \quad 
 \bm{v}_3 = \hat{\bm{L}}^e.
\end{equation}
Here $\hat{\bm{N}}^e$ is a unit vector perpendicular to the surface of the cylinder with a direction 
equal to the sum of the two unit normal vectors on the triangles connected to the bond
and $\hat{\bm{L}}^e$ is the unit vector in the direction of the bond.

Given the eigenvectors and eigenvalues, we can construct $\bm{c}^\mathrm{int}$ on each bond via
\begin{equation}
 c^\mathrm{int}_{\alpha\beta} = \left(c^\mathrm{int}_{uw} \bm{v}^u \bm{v}^{w} \right)_{\alpha\beta},
 \elabel{triangle_int_curv_tensor_edge}
\end{equation}
where $C^\mathrm{int}_{uw}$ is a diagonal matrix with the eigenvalues in \eref{triangle_C_int_eigenvalues} on the diagonal.

\subsubsection{Integrated curvature tensor on a patch (Implementation I)}
We extend the integrated curvature tensor on a single edge in \eref{triangle_int_curv_tensor_edge} 
to a patch on the triangular surface labeled $p$, with area $A_p$,
which consists of a set of triangles that lie in the patch.
The integrated curvature tensor on the patch is the sum over the integrated curvature tensors 
of each edge $e_p$ that lies in the patch, normalized by the area of the patch
\begin{equation}
 \bm{c}_{p}^{\mathrm{int}} = \frac{1}{A_p} \sum_{\{e_p\}} \bm{c}^\mathrm{int}_e.
 \elabel{triangle_int_curv_tensor_patchI}
\end{equation}
This included edges that lie on the boundary of the patch.

\subsubsection{Integrated curvature tensor on a patch (Implementation II)}
An alternative definition of the curvature tensor on a patch $p$, 
is to calculate the integrated curvature tensor as defined in \eref{triangle_int_curv_tensor_patchI} 
and obtain its three eigenvectors $\{\bm{v}_i\}_{i=1}^3$, 
sorted by the norms of their corresponding eigenvalues, $|\nu_1| > |\nu_2| > |\nu_3|$. 
The eigenvalue with the smallest norm, $\nu_3$, 
will be close to zero and corresponds the the eigenvector that has a direction 
parallel to the normal vector on the patch.
We then construct the curvature tensor on the patch, $c^p_{uv}$, 
which has eigenvalues equal to the principle curvatures of the patch, $\kappa_+^p$ and $\kappa_-^p$,
such that $|\kappa_+^p| > |\kappa_+^p|$,
$c^p_{11} = \kappa_+^p, c^p_{22} = \kappa_-^p, c^p_{33} = 0$ 
and eigenvectors equal to the eigenvectors of the integrated curvature tensor $\bm{v}_i$.

The principle curvatures $\kappa^p_+$ and $\kappa^p_-$ of the patch are calculated using 
the mean and Gaussian curvatures of the patch, which in turn we calculate from the 
integrated mean and Gaussian curvatures $\bar{H}_p$ and $\bar{G}_p$ 
defined in \eref{triangle_int_HK} 
\begin{equation}
 H_p = \frac{\bar{H}}{A_p}, \quad G_p = \frac{\bar{G}}{A_p}.
\end{equation}
We use these to calculate the principle curvatures on the patch,
$\kappa_1^p$ and $\kappa_2^p$, via \eref{triangle_int_k1k2} and sort them according to their absolute value
to obtain $\kappa_+^p$ and $\kappa_-^p$.
We can then use the principle curvatures and the corresponding sorted eigenvectors $\{\bm{v}_i\}_1^3$ 
to construct the curvature tensor on the patch
\begin{equation}
 \bm{c}^p = c_{uv}^p \bm{v}^u \otimes \bm{v}^v.
\end{equation}

\section{Definition of cell tensors}
All tensors defined on the cells are calculated by taking the area weighted average of the tensors defined on the triangles which overlap with the cell. 
In the subcellular triangulation used in this implementation, every triangle only overlaps with one cell.
A tensor (or scalar) $\bm{t}^\alpha$ on a cell $\alpha$ is defined as
\begin{equation}
 \bm{t}^\alpha = \frac{1}{A^\alpha} \sum_{n\in\mathrm{cell}} a^t \bm{t}^n
 \elabel{cell_tensor}
\end{equation}
where $A^\alpha$ is the area of the cell, $a^t$ the area of a triangle that overlaps with the cell 
and $\bm{t}^n$ the tensor defined on that triangle.

To plot a rank 2 tensors, like the cell elongation tensor, 
we take the largest eigenvalue of $\bm{t}^\alpha$ as the norm of elongation, and the corresponding eigenvector 
as the direction of elongation which we can plot to the surface.
Note that for cells / patches that are reasonably flat, the eigenvector with the eigenvalue 
closest to zero, is (almost) parallel to the normal vector on the patch.


\section{Smooth tensors and detect nematic defects on a curved triangular surface}
Here we give definitions for smoothing or coarse graining tensors that are defined on the triangles that form the surface. 
Because the surface is curved, we want a method of averaging the tensors that does not depend on the coordinate system used.
We thus need a coordinate free representation of the surface.
To this end, we create an undirected graph $G$ from the triangular surface, in which every node of the graph is a triangle and every edge of the graph stems from two triangles that share an edge on the surface.
The order of each node in the graph can therefore be at most three.
We use the Python package \emph{igraph} for constructing the graph and performing the neighborhood searches.

\subsection{Smoothing tensor field using a coordinate free method}
We smooth a certain field of tensors defined on every triangle $m$ on the surface, $\bm{q}^m$, by calculating for every triangle the area weighted average over its closed neighborhood with order $k(n)$. 
The closed neighborhood of $m$ with order $k(n)$, $N_k(m)[m]$, is defined as the set of nodes (triangles) in the graph $G$, including $m$, that can be reached with $k(m)$ hops starting from $m$.
Note that the order of the neighborhood can be different for every triangle in our implementation.
Taken together, the smooth field on the surface, $\bm{Q}^m_{k(m)}$, is defined as
\begin{equation}
 \bm{Q}^m_{k(m)} = \frac{1}{A^m} \sum_{r\in N_k(m)[m]} a^r \bm{q}^r \, .
\end{equation}
Here $a^r$ is the area of a triangle and $A^m$ is the total area of all the triangles that lie in the neighborhood of $m$. 

Since we want the area of every neighborhood to be similar for every triangle, we choose the order of each neighborhood $k(m)$ such that its area $A_k^m$ is closest to a certain chosen coarse graining area $A_\mathrm{coarse}$
\begin{equation}
 k\,|\,\mathrm{min}\left( |A_k^m - A_\mathrm{coarse} | \right) \, .
\end{equation}
The level of coarse graining can be set through the preferred area $A_\mathrm{coarse}$.

\subsection{Defect detection using contour integration}
We want to find the defects in the smooth field of nematic tensors, $\bm{Q}^m_{k(m)}$, obtained above. 
To this end, we calculate the total rotation angle of the nematic tensors as one traverses a closed contour of connected triangles on the surface.
Therefore, for every triangle $m$ with a neighborhood of order $l(m)$, we obtain the closed contour on the surface, $C_{l(m)}^m$, which goes around the border of the neighborhood.
The contour $C_{l(m)}^m$ is a set of triangles that lie within the neighborhood of $m$ and that share at least one vertex of the triangle network with the vertices that lie on the boundary of the neighborhood.
Furthermore, it is a sub-graph of $G$ and has the topology of a cycle graph where every node has degree two. 
The neighborhoods used for contours are obtained as outlined in the section above, but with a different target area, $A_\mathrm{contour}$.
This allows for a different area of smoothing than for defect detection.
Therefore, the neighborhoods used for obtaining the contour have in general a different order, $l(m)$, than the neighborhood orders used for smoothing the surface, $k(m)$. 

Given a contour of ordered triangles centered around triangle $m$, we calculate the smallest angle $\theta^m_{i, i+1}$ between the nematic tensors on all pairs of consecutive triangles $i$ and $i+1$ going around the contour,
\begin{equation}
 \theta^m_{i,i+1} = \min\left[\arccos(\mathbf{Q}_i\cdot\mathbf{Q}_{i+1}),\,\pi - \arccos(\mathbf{Q}_i\cdot\mathbf{Q}_{i+1})\right]\in [0,\pi/2].
 \elabel{angle_consec_tensors}
\end{equation}

Next to the angle, we also require the direction of rotation, $s_i$, of rotating $\mathbf{Q}_i$ onto $\mathbf{Q}_{i+1}$. 
Given the unit normal on triangle $i$, $\mathbf{n}_i$, the direction of rotation is defined as
\begin{equation}
 s_i = \mathrm{sign}[(\mathbf{Q}_i\times\mathbf{Q}_{i+1})\cdot\mathbf{n}_i] \in \{-1,1\},
\end{equation}
where $s_i=1$ means counterclockwise rotation and $s_i=-1$ means clockwise rotation w.r.t. the oriented surface. 

Given the angle and direction of rotation, we obtain the total rotation angle of the smooth nematic field, $\Theta_n$, traversing the contour centered around triangle $n$ in counterclockwise direction, as
\begin{equation}
 \Theta_n = \sum_i \theta_{i,i+1}\, s_i.
\end{equation}
In this implementation, the total rotation angles are normalized by $2\pi$: 
\begin{equation}
 \bar{\Theta}_n = \frac{\Theta_n}{2\pi}\,.
\end{equation}

Note that, in general, our surface is curved, and the tensors $\mathbf{Q}^t$ do not all lie in the same plane.
Therefore, the angle between consecutive tensors on the contour calculated in \eref{angle_consec_tensors} originates from 1) a difference in direction between the triangle elongation tensors and 2) a change in orientation of the surface normal vectors between the positions the tensors are located.
To remove the contribution stemming from the curvature of the surface, we (optionally) rotate all the tensors $\mathbf{Q}^t$ into the same plane.
Specifically, we rotate $\mathbf{Q}^t$ such that it lies in the xy-plane of the coordinate system
\begin{equation}
 \mathbf{Q}^t_\mathrm{in-plane} = \bm{R}_{\bm{u}^t}(\psi^t) \mathbf{Q}^t \bm{R}_{\bm{u}^t}(-\psi^t)
\end{equation}
where the angle and direction of the rotation $\bm{R}$, $\psi^t$ and $\bm{u}^t$ respectively, are defined as
\begin{equation}
 \psi^t = \arccos(\hat{\bm{m}}^t\cdot\hat{\bm{z}})\in[0,\pi]\,, \quad \bm{u}^t = \frac{\hat{\bm{m}}\times\hat{\bm{z}}}{|\bm{m}\times\hat{\bm{z}}|}\,. 
\end{equation}
Here, $\hat{\bm{z}}$ is the unit base vector in the z-direction and $\hat{\bm{m}}^t$ is the unit vector that is normal to the plane of the nematic tensor (this is the eigenvector of $\mathbf{Q}^t$ with the eigenvalue closest to zero) and pointing into the space set by the orientation of the surface.


\newpage
\section{curvedtm2vtk: Convert CurvedTM tables to VTK format for Paraview plotting}
\begin{verbatim}
usage: curvedtm2vtk.py [-h] [-v] [-n NETWORK_TYPE] [-i DATAPATH_INPUT]
                       [-o DATAPATH_OUTPUT] [-f FILE_INPUT] [-r ROI_NAME]
                       [-g [FRAMES [FRAMES ...]]]
                       [-s [SCALAR_NAMES [SCALAR_NAMES ...]]]
                       [-t [TENSOR_NAMES [TENSOR_NAMES ...]]]
                       [-e [TENSOR_EIGENVALUE_PROPERTIES [TENSOR_EIGENVALUE_PROPERTIES ...]]]

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Run in verbose mode.
  -n NETWORK_TYPE, --network_type NETWORK_TYPE
                        Specify the network type for output. Options:
                        ['cells', 'dual_lattice_triangles',
                        'subcellular_triangles', 'patches']
  -i DATAPATH_INPUT, --datapath_input DATAPATH_INPUT
                        Path to the frames directory created by curved tissue
                        miner. Default: ./frames/
  -o DATAPATH_OUTPUT, --datapath_output DATAPATH_OUTPUT
                        Path to where the vtk files will be saved. Default:
                        ./frames/vtk/
  -f FILE_INPUT, --file_input FILE_INPUT
                        Specify file for input data.
  -r ROI_NAME, --roi_name ROI_NAME
                        Specify the name of the ROI to plot.
  -g [FRAMES [FRAMES ...]], --frames [FRAMES [FRAMES ...]]
                        Specify the indices of the frames to load.
  -s [SCALAR_NAMES [SCALAR_NAMES ...]], --scalar_names [SCALAR_NAMES [SCALAR_NAMES ...]]
                        Specify names of scalar data to plot. Multiple names
                        separated by spaces.
  -t [TENSOR_NAMES [TENSOR_NAMES ...]], --tensor_names [TENSOR_NAMES [TENSOR_NAMES ...]]
                        Specify the names of tensor data to plot. Multiple
                        names separated by spaces.
  -e [TENSOR_EIGENVALUE_PROPERTIES [TENSOR_EIGENVALUE_PROPERTIES ...]], 
                        Specify eigenvalue properties of the tensor data. Each
                        listed tensor name should have an eigenvalue property.
\end{verbatim}


\newpage
\section{Explanation of the Curved Tissue Miner tables}
List of all the tables and their columns created by the curved tissue miner implementation.

% VERTICES TABLE
\begin{table}[ht]
 \centering
 \begin{tabular} {l|l}
 \hline
 \rowcolor{blue!20}
 \multicolumn{2}{c}{vertices table}\\
 \hline
  \textbf{column name} & \textbf{description} \\
  \hline \hline 
  \rowcolor{gray!10}
  \texttt{index} & ID of the vertex. Only unique within frame, not shared between frames. \\
  
  \texttt{x/y/z\_pos} & Components of the vertex position vector. \\ 
  \rowcolor{gray!10}
  \texttt{angular\_defect} & Defect angle of the vertex. Only defined for triangle network. (\eref{vertex_int_gaussian_curvature}) \\
  \hline
 \end{tabular}
 \caption{Definition of the vertices table for both the cell and the triangle network.}
\end{table}


% DCEL TABLE
\begin{table}[ht]
 \centering
 \begin{tabular} {l|l}
 \hline
 \rowcolor{blue!20}
 \multicolumn{2}{c}{doubly connected edge list table (DCEL)}\\
 \hline
  \textbf{column name} & \textbf{description} \\
  \hline \hline 
  \rowcolor{gray!10}
  \texttt{index} & ID of the directed bond. Only unique within frame, not shared between frames. \\
  
  \texttt{cell\_id} & ID of cell the dbond belongs to. \\ 
  \rowcolor{gray!10}
  \texttt{conj\_dbond\_id} & ID of the dbond on the other side of the same bond of the network. \\
  
  \texttt{bond\_id} & ID of the bond. This dbond and its conjugate dbond share the same bond id.\\
  \rowcolor{gray!10}
  \texttt{vertex\_id} & ID of the vertex this dbond points outward from. \\
  
  \texttt{left\_dbond\_id} & ID of the dbond this dbond points to. It lies within the same face (cell or triangle). \\
  \rowcolor{gray!10}
  \hline
 \end{tabular}
 \caption{Definition of the double connected edge list (DCEL) table. Every row defines a directed bond (dbond) in  the cell network or the triangle network.}
\end{table}


% Triangle faces table
\begin{table}[ht]
 \centering
 \begin{tabular} {l|l|c}
 \hline
 \rowcolor{blue!20}
 \multicolumn{3}{c}{triangles table}\\
 \hline
  \textbf{column name} & \textbf{description} & Definition \\
  \hline \hline 
  \rowcolor{gray!10}
  \texttt{index} & ID of the triangle. Only unique within frame. & [0,\,N-1] \\
  
  \texttt{triangle\_id} & Unique ID of the triangle. Shared between frames. & pairing function\\ 
  \rowcolor{gray!10}
  \texttt{vertex\_id\_1/2/3} & IDs of vertices connected to the triangle. & \\
  
  \texttt{dr\_12\_x/y/z} & Components of the triangle vector $\bm{E}_{12}$. & \eref{triangle_vectors} \\
  \rowcolor{gray!10}
  \texttt{dr\_13\_x/y/z} & Components of the triangle vector $\bm{E}_{13}$. & \eref{triangle_vectors} \\
  
  \texttt{area} & Area of the triangle. & \eref{triangle_area_normal} \\
  \rowcolor{gray!10}
  \texttt{normal\_x/y/z} & Components of the unit normal vector on the triangle. & \eref{triangle_area_normal} \\
  
  \texttt{rotation\_angle\_x/y/z} & Angles definition orientation of the triangle.  & \eref{triangle_state_tensor}\\
  \rowcolor{gray!10}
  \texttt{elongation\_tensor\_norm} & Norm of the triangle elongation tensor. & \eref{triangle_elongation_norm} \\
  
  \texttt{elongation\_tensor\_twophi} & Double angle of the triangle elongation tensor. & \eref{triangle_elongation_angle} \\
  \rowcolor{gray!10}
  \texttt{mean\_curvature} & Mean curvature of the triangle. & \eref{triangle_mean_gaussian_curvature} \\
  
  \texttt{gaussian\_curvature} & Gaussian curvature of the triangle. & \eref{triangle_mean_gaussian_curvature} \\
  \hline
 \end{tabular}
 \caption{Definition of the triangles table. Every row defines a triangle or a tensor defined on that triangle.}
\end{table}


% Triangle faces table
\begin{table}[ht]
 \centering
 \begin{tabular} {l|l|c}
 \hline
 \rowcolor{blue!20}
 \multicolumn{3}{c}{cells table}\\
 \hline
  \textbf{column name} & \textbf{description} & Definition \\
  \hline \hline 
  \rowcolor{gray!10}
  \texttt{cell\_id} & Unique ID of the cell. Shared between frames. & [10001,\ldots] \\
  
  \texttt{center\_x/y/z} & Comnponents of the cell centroid. & \eref{cell_centroid} \\
  \rowcolor{gray!10}
  \texttt{area} & Area of the cell. & \eref{cell_area_normal} \\
  
  \texttt{normal\_x/y/z} & Components of the unit normal vector on the cell. & \eref{cell_area_normal} \\
  \rowcolor{gray!10}
  \texttt{neighbour\_number} & Number of neighbors of the cell.  & \\
  
  \texttt{elongation\_tensor\_norm\_max} & Magnitude of the cell elongation tensor. & \eref{cell_tensor} \\
  \rowcolor{gray!10}
  \texttt{elongation\_tensor\_twophi} & Double angle of the cell elongation tensor. & \eref{cell_tensor} \\
  
  \texttt{mean\_curvature} & Mean curvature of the cell. & \eref{cell_tensor} \\
  \rowcolor{gray!10}
  \texttt{gaussian\_curvature} & Gaussian curvature of the cell. & \eref{cell_tensor} \\
  \hline
 \end{tabular}
 \caption{Definition of the cells table. Every row defines a cell or a tensor defined on that cell.}
\end{table}


% dbond geometry table
\begin{table}[ht]
 \centering
 \begin{tabular} {l|l|c}
 \hline
 \rowcolor{blue!20}
 \multicolumn{3}{c}{dbond geometry table}\\
 \hline
  \textbf{column name} & \textbf{description} & Definition \\
  \hline \hline 
  \rowcolor{gray!10}
  \texttt{index} & ID of the dbond. Not unique between frames. & [0, M-1] \\
  
  \texttt{signed\_angle} & Signed angle between vectors normal to the triangles connected to this bond & \eref{bond_int_mean_curvature} \\
  \rowcolor{gray!10}
  \texttt{length} & Length of the dbond. &  \\
  
  \texttt{int\_mean\_curvature} & Integrated mean curvature of the dbond & \eref{bond_int_mean_curvature} \\
  \hline
 \end{tabular}
 \caption{Definition of the dbond geometry table. Every row gives the geometry of a dbond in the triangle network. A dbond and its conjugate share the same geometry.}
\end{table}

\end{document}

