{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Coordinate free cell elongation alignment at distance $k$ \n",
    "\n",
    "### Required software\n",
    "\n",
    "- Python >= 3.6\n",
    "- Numpy\n",
    "- Pandas\n",
    "- [igraph for Python](https://anaconda.org/conda-forge/python-igraph) \n",
    "- netCDF4\n",
    "\n",
    "Make sure you have the latest version of igraph: v0.8.2! The installer might take an older version by default. To force the latets version:\n",
    "\n",
    "`conda install -c conda-forge python-igraph=0.82`\n",
    "\n",
    "You also need netCDF4 that can be downloaded and installed using \n",
    "\n",
    "`pip install netCDF4`\n",
    "\n",
    "### Method outline\n",
    "\n",
    "Given the cellular network of the tissue and the elongation tensor, $\\mathbf{q}^\\alpha$, on every cell $\\alpha$, defined as\n",
    "\n",
    "$$ \\mathbf{q}^\\alpha = \\frac{1}{A^\\alpha} \\sum_m a^m \\mathbf{q}^m $$\n",
    "\n",
    "where $A^\\alpha$ is the area of the cell, $a^m$ the area, $\\mathbf{q}^m$ the triangle elongation tensor lying in the plane of a subcellular triangle contained in the cell polygon $m$. Then\n",
    "\n",
    "- Define the graph $g$ of the cell network, where every cell is a node and the neighbors of each cell define the edges of the graph.\n",
    "- Then, for every cell $\\alpha$ in the network, find the set of cells that are on a ring $k$ steps away from $\\alpha$, $R_k(\\alpha)$. This set is defined as those cells that are in the neighborhood of order $k$ of cell $\\alpha$, $N_k(\\alpha)$, but are not in the neighborhood with order $k-1$\n",
    "\n",
    "$$ R_k(\\alpha) = N_k(\\alpha) - N_{k-1}(\\alpha) $$\n",
    "\n",
    "- We find the mean alignment of the cell elongation tensors of cell $\\alpha$ with the cells $k$ hops away as:\n",
    "\n",
    "\n",
    "$$ \\Omega^\\alpha_k = \\frac{1}{M^\\alpha_k}\\sum_{\\beta\\in R_k(\\alpha)} \\langle \\hat{\\mathbf{q}}^\\alpha, \\hat{\\mathbf{q}}^\\beta \\rangle \\,.$$\n",
    "\n",
    "Here, $M^\\alpha_k$ is the number of cells that are $k$ hops away from cell $\\alpha$, $\\langle \\mathbf{A}, \\mathbf{B} \\rangle$ is the matrix scalar product defined below and $\\hat{\\mathbf{q}}$ is the cell elongation tensor $\\mathbf{q}$ divided by its norm $|\\mathbf{q}|=\\sqrt{\\langle \\mathbf{q}, \\mathbf{q} \\rangle}$.\n",
    "\n",
    "- We find the mean alignment against the distance $k$, averaged over all cells as\n",
    "\n",
    "$$ \\Omega_k = \\sum_\\alpha \\Omega^\\alpha_k \\,.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scalar product and norm of a matrix:\n",
    "\n",
    "We define the scalar product between two matrices $\\mathbf{A}$ and $\\mathbf{B}$ as:\n",
    "$$\\langle \\mathbf{A}, \\mathbf{B} \\rangle =\\frac{1}{2}\\mathrm{Tr}[\\mathbf{A}^T\\cdot\\mathbf{B}]=\\frac{1}{2}\\sum_{i,j} A_{ij} B_{ij},$$\n",
    "and the norm of a matrix $\\mathbf{M}$ reads:\n",
    "$$|\\mathbf{M}|=\\sqrt{\\mathrm{Tr}[\\mathbf{M}^T\\cdot\\mathbf{M}]/2}.$$\n",
    "\n",
    "Note that the $1/2$ differs from the usual definitions of the Frobenius scalar product and norm. We add it such that a symmetric traceless matrix $\\mathbf{q}$ in dimension two has a norm:\n",
    "$$|q|^2 = q_{xx}^2+q_{xy}^2 \\, .$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import os\n",
    "import pwd\n",
    "import sys\n",
    "import multiprocessing as mp\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.linalg as lin\n",
    "\n",
    "import igraph as ig\n",
    "\n",
    "# Load Curved Tissue Miner packages\n",
    "import curvedtm\n",
    "from curvedtm import MMCoarse\n",
    "from curvedtm import MMTool"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Set path to directory containing the Tissue Miner movies and the name of specific name.\n",
    "username = 'duclut'\n",
    "\n",
    "datapath_tissue_miner = '/data/biophys/' + username + '/data_wing/' \n",
    "\n",
    "movie_name = '20161110_116hrAEL_400nM20E'\n",
    "\n",
    "datapath_movie_name   = os.path.join(datapath_tissue_miner, movie_name)\n",
    "datapath_movie_frames = os.path.join(datapath_movie_name, 'frames/')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Warning: Output directory already exists. Executing this might overwrite existing tables!\n",
      "Output directory: /data/biophys/duclut/data_wing/20161110_116hrAEL_400nM20E/frames\n",
      "Warning: One or more pickle files are missing. refresh_DB_tables set to True.\n",
      "Tissue Miner DB Loader for movie 20161110_116hrAEL_400nM20E inititalized.\n",
      "Triangulation of movie 20161110_116hrAEL_400nM20E for network type subcellular_triangles inititalized.\n",
      "Execute 'run()','run_parallel()' or 'run_frame(frameIdx)' to start the Curved Tissue Miner.\n"
     ]
    }
   ],
   "source": [
    "curved_surface = curvedtm.CurvedTM(datapath_movie_name, \n",
    "                                   network_type = 'subcellular_triangles',  \n",
    "                                   frames_to_process = [-1], \n",
    "                                   refresh_DB_tables = False, \n",
    "                                   refresh_fundaments = True, \n",
    "                                   refresh_geometry = True,\n",
    "                                   refresh_triangle_state = True,\n",
    "                                   refresh_cell_state = True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "frameIdx = 72"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create graph of cellular network\n",
    "\n",
    "Create a graph $g$ in which every cell is a node and every neighbor relation is an edge in the graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load data\n",
    "cells_dbonds_id_flat, cells_nn = curved_surface.load_data_tables(frameIdx, ['/cells/sorted_cell_dbonds_per_frame_dbonds_id', '/cells/sorted_cell_dbonds_per_frame_nn'], 'list')\n",
    "cells = curvedtm.load_table(datapath_movie_frames, table_name = 'cells', frameIdx = frameIdx, network_type = 'cells')\n",
    "cell_dbonds = curvedtm.load_table(datapath_movie_frames, 'dbonds', frameIdx)\n",
    "\n",
    "# Create cell_id -> cell_idx dict.\n",
    "cells_id = np.array(list(cells.index))\n",
    "cell_id_to_cell_idx_dict = dict(zip(cells_id, range(len(cells_id))))\n",
    "\n",
    "# Convert cell -> dbonds to cell -> neighbor cell ids.\n",
    "cells_conj_dbonds_id_flat = cell_dbonds['conj_dbond_id'].iloc[cells_dbonds_id_flat].values\n",
    "cells_neighbors_id_flat   = cell_dbonds['cell_id'].iloc[cells_conj_dbonds_id_flat].values\n",
    "\n",
    "# Convert cell ids to cell idx, ignore boundary cells.\n",
    "edges_start_cidx = [cell_idx for cell_idx, nn in enumerate(cells_nn) for i in range(nn)]\n",
    "\n",
    "edges_end_cidx_filt   = [cell_id_to_cell_idx_dict[cid] for cid in cells_neighbors_id_flat if cid != 10000]\n",
    "edges_start_cidx_filt = [start_cidx for (start_cidx, neighbor_idx) in zip(edges_start_cidx, cells_neighbors_id_flat) if neighbor_idx != 10000]\n",
    "\n",
    "assert (np.unique(edges_start_cidx_filt) == np.unique(edges_end_cidx_filt) ).any()\n",
    "N_cells   = len(np.unique(edges_start_cidx_filt))\n",
    "cells_idx = list(range(N_cells))\n",
    "\n",
    "# Create graph egdes (cell_idx -> neighbor_idx)\n",
    "graph_edges = list(zip(edges_start_cidx_filt, edges_end_cidx_filt))\n",
    "\n",
    "# Create igraph object\n",
    "g = ig.Graph(n=N_cells, edges=list(graph_edges), directed=False)\n",
    "g.vs['id'] = cells_id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Find cells at distance $k$ in graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Obtaining rings of order: 24/25\n",
      "Done\n"
     ]
    }
   ],
   "source": [
    "### Max order used for finding the rings.\n",
    "max_order = 25\n",
    "\n",
    "### Construct rings of cell ids.\n",
    "# Cell neighborhood of order k-1\n",
    "cells_neighborhood_k_1 = list(range(N_cells))\n",
    "# Cell neighborhood of order k.\n",
    "cells_neighborhood_k = []\n",
    "# Store cell rings\n",
    "cells_ring_k_list = []\n",
    "\n",
    "for neighborhood_order in range(1, max_order):\n",
    "    print('\\rObtaining rings of order:', str(neighborhood_order) + '/' + str(max_order), end='')\n",
    "    \n",
    "    cells_neighborhood_k = g.neighborhood(cells_idx, order=neighborhood_order)\n",
    "    \n",
    "    cells_ring_k = [np.setdiff1d(cell_nb_k, cell_nb_k_1) for cell_nb_k, cell_nb_k_1 in zip(cells_neighborhood_k, cells_neighborhood_k_1)] \n",
    "    cells_ring_k_list.append(cells_ring_k)\n",
    "            \n",
    "    cells_neighborhood_k_1 = cells_neighborhood_k\n",
    "print('\\nDone')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calculate the cell-cell elongation alignment at every distance $k$.\n",
    "\n",
    "We calculate the alignment between the elongation of any 2 cells $\\alpha$ and $\\beta$\n",
    "$$ \\Omega^{\\alpha\\beta} =  \\langle \\hat{\\mathbf{q}}^\\alpha, \\hat{\\mathbf{q}}^\\beta \\rangle\\,.$$\n",
    "\n",
    "To remove contributions to the angle $\\Omega^{\\alpha\\beta}$ stemming from the curvature of the surface, we (optionally) rotate each tensor $\\mathbf{q}^\\beta$ to align its normal $\\mathbf{n}^\\beta$ with the normal $\\mathbf{n}^\\alpha$ of the tensor $\\mathbf{q}^\\alpha$. Note that the normal vector $\\mathbf{n}^\\alpha$ is the unit vector that is normal to the plane of the nematic tensor (this is the eigenvector of $\\mathbf{q}^{\\alpha}$ with the eigenvalue closest to zero) and pointing into the space set by the orientation of the surface.\n",
    "\n",
    "The rotation is made around the axis orthogonal to both normals, such that the rotated tensor $\\mathbf{q'}^\\beta$ reads:\n",
    "$$ \\mathbf{q'}^\\beta = \\mathbf{R}_{\\mathbf{u}}(\\psi) \\mathbf{q}^\\beta \\mathbf{R}_{\\mathbf{u}}(-\\psi)$$\n",
    "where the angle and direction of the rotation $\\mathbf{R}$, $\\psi$ and $\\mathbf{u}$ respectively, are defined as\n",
    "$$ \\psi^t = \\arcsin(|\\mathbf{n}^\\alpha\\times\\mathbf{n}^\\beta|), \\quad \\mathbf{u}=\\frac{\\mathbf{n}^\\alpha\\times\\mathbf{n}^\\beta}{|\\mathbf{n}^\\alpha\\times\\mathbf{n}^\\beta|} \\,. $$\n",
    "\n",
    "\n",
    "There are three options below:\n",
    "- rotate_matrices_spherical_geodesics: [True, False]\n",
    " * True: Rotate each elongation tensor of cell $\\beta$ into the plane of cell $\\alpha$ before calculating alignment. Note that using this option make the whole computation much slower (~30minutes if True versus ~30seconds if False).\n",
    " * False: Keep elongation tensors in the plane of the cell.\n",
    "- weight_by_norm: [True, False] \n",
    " * True: Weight the average by $|q^\\alpha| |q^\\beta|$. The mean alignement in each ring is then normalized by $\\sum_m |q^\\alpha| |q^\\beta|$.\n",
    " * False: The normalization of the average over each ring of distance $k$ is the number of cells in that ring, $M^\\alpha_k$.\n",
    "- use_inplane_tensors: [True, False]\n",
    " * True: Rotate all elongation tensors into xy-plane before calculating alignment.\n",
    " * False: Keep elongation tensors in the plane of the cell.\n",
    "\n",
    "This last option is implemented in the following way:\n",
    "Specifically, we rotate $\\mathbf{Q}^\\alpha$ such that it lies in the xy-plane of the coordinate system\n",
    "$$ \\mathbf{Q}^t_\\mathrm{in-plane} = \\mathbf{R}_{\\mathbf{u}^t}(\\psi^t) \\mathbf{Q}^t \\mathbf{R}_{\\mathbf{u}^t}(-\\psi^t)$$\n",
    "where the angle and direction of the rotation $\\mathbf{R}$, $\\psi^t$ and $\\mathbf{u}^t$ respectively, are defined as\n",
    "$$ \\psi^t = \\arccos(\\hat{\\mathbf{m}}^t\\cdot\\hat{\\mathbf{z}})\\in[0,\\pi]\\,, \\quad \\mathbf{u}^t = \\frac{\\hat{\\mathbf{m}}\\times\\hat{\\mathbf{z}}}{|\\mathbf{m}\\times\\hat{\\mathbf{z}}|}\\,. $$\n",
    "Here, $\\hat{\\mathbf{z}}$ is the unit base vector in the z-direction and $\\hat{\\mathbf{m}}^t$ is the unit vector that is normal to the plane of the nematic tensor (this is the eigenvector of $\\mathbf{Q}^t$ with the eigenvalue closest to zero) and pointing into the space set by the orientation of the surface. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Averaging over rings of order: 24\n",
      "Done\n"
     ]
    }
   ],
   "source": [
    "weight_by_norm      = False\n",
    "rotate_matrices_spherical_geodesics = False\n",
    "use_inplane_tensors = False\n",
    "\n",
    "# Load cell elongation tensors.\n",
    "cells_q2_full = curvedtm.load_table(datapath_movie_frames, table_name = 'elongation_tensor', frameIdx = frameIdx, network_type = 'cells')\n",
    "# Shuffle tensors to simulate case with random alignment.\n",
    "#cells_q2_full = cells_q2_full[np.random.permutation(N_cells)]\n",
    "\n",
    "# Calculate norm of tensors (assumed to be symmetric)\n",
    "cells_q2_norm = np.array([np.sqrt(0.5*np.sum(mat*mat)) + 1e-12 for mat in cells_q2_full])\n",
    "\n",
    "\n",
    "# Rotate all tensors in xy-plane.\n",
    "if use_inplane_tensors:\n",
    "    cells_normal  = cells[['normal_x', 'normal_y', 'normal_z']].values\n",
    "    cells_q2_full = MMCoarse.rotate_tensors_in_plane(cells_q2_full, cells_normal, return_rank2=True)\n",
    "    \n",
    "if rotate_matrices_spherical_geodesics:\n",
    "    # some cells are made of 2 vertices and thus have a vanishing normal vector: we add the 1e-12 to compute their rotation.\n",
    "    # in the end those cells have a vanishing elongation tensors so they won't contribute\n",
    "    cells_normal  = cells[['normal_x', 'normal_y', 'normal_z']].values + 1e-12\n",
    "        \n",
    "if weight_by_norm:\n",
    "    cells_q2 = cells_q2_full\n",
    "else:\n",
    "    cells_q2 = cells_q2_full/cells_q2_norm.reshape(-1,1,1)\n",
    "    \n",
    "cells_alignment_k_list = []\n",
    "for k, cells_R_k in enumerate(cells_ring_k_list):\n",
    "    print('\\rAveraging over rings of order:', k+1, end='')\n",
    "    \n",
    "    # Find cells without neighbors. The ring list of such cells is set to be the cell id of that isolated cell.\n",
    "    cells_R_k_order        = np.array([len(cell_R) for cell_R in cells_R_k])\n",
    "    cells_idx_no_neighbors = np.where(cells_R_k_order == 0)[0]\n",
    "    for cell_idx_nn in cells_idx_no_neighbors:\n",
    "        cells_R_k[cell_idx_nn] = [cell_idx_nn] \n",
    "    \n",
    "    if rotate_matrices_spherical_geodesics:\n",
    "        cells_alignment_k = np.zeros(N_cells)\n",
    "    \n",
    "        for (cell_alpha, cell_beta_list) in zip(range(N_cells), cells_R_k):\n",
    "            if cell_beta_list[0]!=cell_alpha:\n",
    "                ncrossn = np.cross(cells_normal[cell_alpha],cells_normal[cell_beta_list])\n",
    "                normn = np.linalg.norm(ncrossn,axis=1)\n",
    "                theta = np.arcsin(normn)\n",
    "                u = ncrossn/normn.reshape(-1,1)\n",
    "                # here we rotate each cell beta to align its normal vector to that of cell alpha\n",
    "                # this step is especially slow since every single cell beta has to be rotated\n",
    "                cells_q2_beta = MMTool.rotation(cells_q2[cell_beta_list], -theta, u)\n",
    "    \n",
    "            cells_alignment_k[cell_alpha] = 0.5*np.sum(cells_q2[cell_alpha]*cells_q2_beta)\n",
    "        \n",
    "    else:\n",
    "        cells_alignment_k = [0.5*np.sum(np.array([cells_q2[cell_alpha]]*len(cell_beta_list)) * cells_q2[cell_beta_list])\n",
    "                             for (cell_alpha, cell_beta_list) in zip(range(N_cells), cells_R_k)]\n",
    "        \n",
    "        cells_alignment_k = np.array(cells_alignment_k)       \n",
    "    \n",
    "    \n",
    "    # Set alignment of cells without neighbor to zero.\n",
    "    cells_alignment_k[cells_idx_no_neighbors] = 0.\n",
    "    \n",
    "    # Normalize by number of cells in each ring.\n",
    "    if weight_by_norm:\n",
    "        cells_alignment_k_norm = [np.sum(cells_q2_norm[cell_alpha] * cells_q2_norm[cell_beta_list])\n",
    "         for (cell_alpha, cell_beta_list) in zip(range(N_cells), cells_R_k)]\n",
    "    else:\n",
    "        cells_alignment_k_norm = [len(R_k) for R_k in cells_R_k]\n",
    "    \n",
    "    # Save normalized alignment for each cell at order k.\n",
    "    cells_alignment_k_list.append(cells_alignment_k / np.array(cells_alignment_k_norm))\n",
    "    \n",
    "    \n",
    "print('\\nDone')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mean cell elongation alignment against distance\n",
    "\n",
    "For each distance $k$, take the unweighted mean over all cells of the cell elongation alignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAY8AAAEICAYAAACnL3iHAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+WH4yJAAAgAElEQVR4nO3dd3iV9f3/8ec7ixBGIJCArLBRNhjZKo4qDtS6FbXqV22dVbtbvz/tt7bWtlrFVUcdKGrrFuvGAQoyRTaEPWSDAcLIev/+OCc1IoFzQk7u5JzX47rOFc7NOcmL+zrw4h6fz8fcHRERkWgkBR1ARETqHpWHiIhETeUhIiJRU3mIiEjUVB4iIhK1lKADxELz5s29ffv2QccQEakzZsyYsdndsyN9fVyWR/v27Zk+fXrQMURE6gwzWxnN6+PqtJWZjTSzxwoKCoKOIiIS1+KqPNx9nLtfk5mZGXQUEZG4FlflISIiNUPlISIiUVN5iIhI1FQeIiISNZWHiIhETeURVlbmjJ2yknfmrAs6iohIrReXgwSrwgxenLqavSWljOjZEjMLOpKISK2lI48wM+PSQbks3rCTKcu3Bh1HRKRWU3lUMLJPKzLrp/Ls5KhG6YuIJByVRwX105I5P68N781bz4bte4KOIyJSa6k89nHJoFxK3Xl+yqqgo4iI1Foqj33kNmvAsV2zeWHqKopLy4KOIyJSK6k89uOywbls3LGX9+atDzqKiEitpPLYj2O75tA2qz5jdOFcRGS/VB77kZxkXDIwl6nLt7Jw/fag44iI1Doqj0qcl9eWtJQknvtCRx8iIvtSeVQiq0EaI3u34rWZa9mxpzjoOCIitYrK4wAuG5xLYVEpr85cG3QUEZFaReVxAH3aNqFPm0ye/WIl7h50HBGRWkPlcRCXDm7Pko07mbx0S9BRRERqDZXHQZze+zCaZqTqtl0RkQriqjzMbKSZPVZQUFBt3zM9NZnzj2rLBws2sK5gd7V9XxGRuiyuysPdx7n7NZmZmdX6fS8ZmEuZ5rsSEfmvuCqPWGmblcFx3XJ4Yepqiko035WIiMojQpcOzmXzzr28M1fL1IqIqDwidGyXbHKbZWjEuYgIKo+IJYXnu5q2YhsL1mm+KxFJbCqPKJyX14Z6KUm6bVdEEp7KIwpNMtI4s28rXv9yLQW7Nd+ViCQulUeULhvcnt3FpbwyY03QUUREAqPyiFLP1pn0bduE575YSVmZ5rsSkcSk8qiCywbnsmxzIZ8v3Rx0FBGRQKg8quDUXoeR1SBNF85FJGGpPKogPTWZC45qy/gFG1j7jea7EpHEo/KoolED2wEwVoMGRSQBqTyqqE3TDI4/vAX/mraavSWlQccREalRKo9DcNngXLYUFvHOnPVBRxERqVEqj0MwrHNz2jfL4LEJy9hTrKMPEUkcBy0PMxsfybZElJRk/HLE4SxYv51rnp2hAhGRhFFpeZhZupllAc3NrKmZZYUf7YFWNRWwtju112H8+exeTFi8ievGztR6HyKSEA505PFjYAZwePhr+eMN4KHYR6s7LjiqHXee1ZOPFm7khudnUlyqAhGR+FZpebj7/e7eAfi5u3d09w7hRx93f7AGM9YJlwzK5Y6R3Xl//gZufnEWJSoQEYljKQd7gbs/YGZDgPYVX+/uY2KYq066fGgHSsqcO/+zgOQk4+8X9CU5yYKOJSJS7Q5aHmb2LNAJmAWUXxF2QOWxH1cd3ZHiUufudxeSkmz89dw+KhARiTsHLQ8gD+ju7ppCNkLXDu9EcWkZ936wmNSkJO46uxdJKhARiSORlMdcoCWwLsZZ4spNJ3ShpLSM0R8tISXZuPOsnpipQEQkPkRSHs2B+WY2FdhbvtHdz4hZqjhxyw+6UlTq/OPTpaQmJ3H7yO4qEBGJC5GUxx2xDhGvzIxfjehGSWkZT3y2nJQk43enHaECEZE6L5K7rT41s1ygi7t/aGYZQHLso8UHs1BhlJR5qECSk/jViG4qEBGp0yK52+pq4Bogi9BdV62BfwAnxDZa/DAzbh/ZneLSMv7x6VLSko1bT+oWdCwRkSqL5LTV9cAAYAqAu+ebWU5MU8UhM+MPZ/akpNQZ/dESzIybTuii23hFpE6KpDz2untR+WkWM0shNM5DopSUZNx1di+Ky8q4f3w+L89Yw8UD23HhUW1p1rBe0PFERCIWyZTsn5rZb4H6ZvYD4CVgXGxjxa+kJONv5/bhkVH9aZeVwV/fW8Tguz7iln/NYsbKbWg4jYjUBXawf6zMLAn4H+AkwID3gCdq86DBvLw8nz59etAxIrJk4w6enbySV2auZefeEnq0asxlg3M5o09r6qfpvgQRqRlmNsPd8yJ+fS3ugCqrS+VRrnBvCa99uZZnJ69k0YYdNE5P4by8tlwyKJcOzRsEHU9E4ly1l4eZnQ78AcgldI3EAHf3xocSNJbqYnmUc3emrdjGmMkreHfuekrKnKO7NOeywe05/vAcXWAXkZiIRXksAc4G5tTmU1UV1eXyqGjj9j28OG01z09Zxfrte2jdpD4n92jJ8G7ZDOiQRXqqTmuJSPWIRXl8DJzg7oEsUGFmHYHfAZnufm4k74mX8ihXXFrG+AUbeHHaaiYt3UJRSRn1U5MZ3KkZw7tlM7xrDu2aZQQdU0TqsFiUx1GETlt9ynfntro3gjBPAqcDG929Z4XtI4D7CY1Uf8Ld/xzB93o5Ucujot1FpXyxbAufLNrIJ4s3sXLLLgA6Nm/Asd2yGd4th4E6KhGRKEVbHpGM8/gjsBNIB9KizPM08CAV1v4ws2RCy9j+AFgDTDOzNwkVyV37vP9Kd98Y5c+Ma/XTkjnu8ByOOzw0TnP55sJQkSzaxNgpq3jq8xWkpyYxuGMzhnfLYXi3bHKb6YK7iFSvSMojy91Pqso3d/cJZtZ+n80DgCXuvgzAzF4EznT3uwgdpUgUOjRvQIfmHbhiaIfvHZV8vGgeAL3bZHL5kPac1vsw6qXoiEREDl0k5fGhmZ3k7u9X089sDayu8HwNMLCyF5tZM0JHP/3M7Dfhktnf664hNAcX7dq1q6aodcu+RyUrNhfy4YINvDB1Fbf++yv+9PYCLh7QjlGDcmnROD3gtCJSl0VyzWMH0IDQ9Y5iorxVN3zk8Vb5NQ8zOw842d2vCj+/FBjg7jdW8c/wPfF8zaMq3J3Plmzm6c9X8NGijSSbcUqvw7h8SHv6t2uiGX5FpPqvebh7o0OL9D1rgLYVnrcBvq7mnyEVmBlHd8nm6C7ZrNxSyJjJK/n39NWM++prerUOndI6vY9OaYlI5CIaYW5mrfl2kCAQup4R0Q/4/pFHCrCY0JTua4FpwMXuPi/K7JXSkcfBFe4t4dUv1/LMpBUs2biTZg3SuHhgO0YNzKVlpk5piSSaWNyqezdwATAfKA1v9kiWoTWzF4DhhJay3QDc7u7/NLNTgfsI3WH1pLv/MdLAkVB5RM7d+XzJFp6etJzxC0OntEb0bMmPj+lErzaZQccTkRoSi/JYBPR2970HfGEtovKomlVbdjFm8gr+NX01hXtLuOaYTtzygy46nSWSAKItj0imZF8GpFY9Us0xs5Fm9lhBQUHQUeqkds0yuO307kz69fGcn9eWf3y6lDMe+Jy5a7U/ReS7IjnyeAXoA4znuyPMb4pttKrTkUf1+HjhRn71ymy2FhZx0wlduG54J1KSI/n/hojUNbEYYf5m+CEJ5rjDc3j/lmP4f2/M494PFjN+wQbuOb8PnXOq+wY8EalrtJ6HROQ/s9dx2+tzKCwq5Zcnd+PKoR1I0vTwInGj2o88zGwO31+zvACYDtzp7luiiyh10Wm9D+OoDk357atzuPM/C3h//gbuOa8PbbM0m69IIorkBPY7wH+AUeHHOGAisJ7QxIeSIHIapfP4ZXn89dzeLPh6OyffN4Hnp6zSuusiCSiSC+afu/vQ/W0zsznu3iumCaNgZiOBkZ07d746Pz8/6Dhxbe03u/nly1/x+ZItHNs1m7vP6a3BhSJ1WCxu1W1oZv+duNDMBgANw09LoswXU+4+zt2vyczU4LZYa92kPs9eOZD/O7MHU5Zv4aS/f8obs9YGHUtEakgk5XEV8ISZLTezFcATwNVm1oDvr78hCSQpybhscHve+ekxdM5pyE9fnMWvX5nNnuLSg79ZROq0iO+2MrPM8Ou/iW2kQ6e7rWpeaZlz7weLeOjjpfRo1ZhHRh2ppXFF6pBqm57EzC5x9+fM7Nb9/X4ky9AGReURnA/nb+DWf88C4N7z+3Ji9xYBJxKRSFTnNY/ytUsbVfIQ+Z4Tu7fgrRuPpm1WBleNmc5f3l1ISWlZ0LFEpJppkKDExJ7iUu54cx4vTlvN4I7NGH1RP7Ib1Qs6lohUotoGCZrZ6AO9sTbPbSXBS09N5s/n9ObI3Kbc9vpcTn9gIg9d3J+89llBRxORanCgEeYzaixFNakwziPoKBJ2Xl5berTK5NqxM7jwsS/49SmH8z/DOmjpW5E6TqetpEYU7C7mFy99xfvzN3Bqr5bcfU5vGqXXiZn+RRJCtQ8SNLNsM/ubmb1tZh+VPw4tpiSazPqpPHrpkfzmlMN5b94Gznzwcxat3xF0LBGpokgGCY4FFgAdgN8DKwitOy4SFTPjx8d2YuxVA9m+p4SzHvqc175cE3QsEamCSMqjmbv/Eyh290/d/UpgUIxzSRwb1LEZb980jF6tM7nlX19x97sLNbmiSB0TSXkUh7+uM7PTzKwf0CaGmSQB5DRO5/mrB3LRgHY88slSfv7SbIo1HkSkzohkJcE7w1OT/Ax4AGgM3BLTVJIQUpKT+NMPe9KicT3u+zCfrYV7eWhUfzLSIvlYikiQDvq31N3fCv+yADgutnEk0ZgZN5/YlexG9fjf1+dy0eNTeOryo8hqkBZ0NBE5gEhOW4nE3KiBuTxyyZEsWLedcx+ZxOqtu4KOJCIHEFflYWYjzeyxgoKCoKNIFZzcoyVjrxrI5p17OfuRScz/envQkUSkEnFVHloMqu47qn0WL187hJQk44JHJzNp6eagI4nIfkQySLCemV1sZr81s/9X/qiJcJKYurZoxCvXDqFlZjqXPzmN/8xeF3QkEdlHJEcebwBnElpytrDCQyRmWjWpz0s/GUzvNpnc8MJMnv58edCRRKSCSO6JbOPuI2KeRGQfTTLSeO6qgdz4wpfcMW4+G3fs5Rcnd9OkiiK1QCRHHpPMrFfMk4jsR3pqMo+M6s9FA9rx8CdL+cXLGkwoUhtEcuQxDLjczJYDewED3N17xzSZSFj5YMKcRvW4f3w+W3ZqMKFI0CL523dKzFOIHISZccsPupLTODSY8IcPTeKhUf3pnNMw6GgiCemgp63cfSXQBBgZfjQJbxOpcaMG5vLUFQPYtHMvZzz4Ga/O1Ky8IkGI5FbdnxKalj0n/HjOzG6MdTCRyhzbNZu3bzqanq0zufXfX/HLl79id1Fp0LFEEspBVxI0s9nAYHcvDD9vAEyujdc8KixDe3V+fn7QcSTGSkrLuH98Pg9+vIQuOQ156OL+dGnRKOhYInVSta8kSOgCecX/1pWGt9U6GmGeWFKSk/jZSd145ooBbNlZxBkPfs4rM3QaS6QmRFIeTwFTzOwOM7sD+AL4Z0xTiUThmK7ZvP3To+ndJpOfvfQVv3hJp7FEYi2SC+b3AlcAW4FtwBXufl+sg4lEo0XjdMZeNZCbju/MyzPXcOZDn5G/QWuki8RKpeVhZo3DX7MIrVv+HPAssDK8TaRWSUlO4taTuvHslQPZWhg6jfWyTmOJxMSBjjyeD3+dAUyv8Ch/LlIrDevSnLdvOpo+bTP5+Utf8fOXvmJXUUnQsUTiSqWDBN399PDXDjUXR6R65DROZ+xVgxg9Pp/RH+Xz1epvuP/CfnRv1TjoaCJxIZJxHuMj2SZS2yQnhUalP/c/A9m2q5hTR0/kqmemMW3F1qCjidR5B7rmkR6+ttHczJqaWVb40R5oVVMBRQ7V0M7N+eCWY7j5xC7MWLmN8/4xmXMemcT789ZTVnbgcU4isn+VDhIMjyy/mVBRrOXbsR3bgcfd/cEaSVgFeXl5Pn26LsvI9+0uKuWlGat5bMIy1mzbTcfsBvz4mI6c1a819VKSg44nEphoBwlGMsL8Rnd/4JCT1SCVhxxMSWkZb89dz6OfLmXe19vJaVSPK4Z2YNSgdjROTw06nkiNq/byCH/TnkB3IL18m7uPqVLCGqDykEi5O58v2cKjE5YyMX8zDeulMGpgO64Y2oGWmekH/wYicSIWRx63A8MJlcfbhKZo/8zdzz2EnDGl8pCqmLu2gEcnLOM/s78mOck4q29rLhzQjpxG9WicnkrD9BSSk2rlzDwihywW5TEH6AN86e59zKwF8IS7jzy0qNVPEyNKdVi9dRdPTFzGv6avZk/xd1ctbFgvhcbpKTRKT6Vx/fDXfZ63aFyP03q1Ii0lktl/RGqHWJTHVHcfYGYzgOOAHcBcd+9xaFFjR0ceUh22FhYxdflWtu8pZvvuYnbsKWH7nvDXfZ+Hv5aG797q364Jj1xyJC0a69SX1A3RlkckKwlON7MmwOOERpfvBKZWMZ9InZHVII0RPVtG/Hp3Z1dRKeMXbuTXr8zmtNGf8fCo/gzooNl8JP5EdMH8vy8OjfFo7O6zYxWoOujIQ4K2eMMOfvLsDFZt3cXvTjuCy4e0x0zXS6T2qvb1PMysf/kDyAJSzKyTmUVy1CKSkLq2aMTrNwzluMNz+P24+dz8r1maX0viSiQF8DDQH5hNaKBgz/Cvm5nZT9z9/RjmE6mzGqen8uglR/LIp0v52/uLWLR+B49eeiS5zRoEHU3kkEVyO8gKoJ+757n7kUA/YC5wIvCXGGYTqfOSkozrj+vM01cMYP32PYx84DM+Xrgx6FgihyyS8jjc3eeVP3H3+YTKZFnsYonEl2O7ZjPuhmG0aZrBlc9M474PF2teLanTIimPRWb2iJkdG348DCw2s3pAcYzzicSNtlkZvHrdEM7u14b7PsznqjHTKdilv0JSN0VSHpcDSwhNkngLsCy8rZjQuA8RiVB6ajJ/O683fzirJxPzN3HGQ5+xYN32oGOJRC2qW3XrCt2qK3XBjJVbufa5mWzfU8zd5/TmzL6tg44kCSwWI8yHAncAuVS4O8vdO1YxY8ypPKSu2LhjDzeM/ZKpK7bSp00mx3bLYXi3bPq0aaJ5tKRGxaI8FhI6XTUDKC3f7u5bqhoy1lQeUpcUl5bx5GfLeW/eemat/oYyhyYZqRzdJZvhXbM5pms22Y3qBR1T4lwsymOKuw885GQ1SOUhddW2wiImLtnMJ4s2MmHxJjbvLAKgV+tMju2azfBu2fRt24SUZE26KNUrFuXxZyAZeBXYW77d3WdWNWSsqTwkHpSVOfPXbeeTRRv5ZNEmZq7aRplD4/QUju6SzbHdsjm5e0syM7R4lRy6WJTHx/vZ7O5+fLThaorKQ+JRwa5iJi7ZxKeLNvHp4k1s3LGX1k3q88yVA+ic0zDoeFLHxWQlwbpC63lIonB3pizfyg3Pz6SkzPnnj/I4Mlez90rVxWJixEwzu9fMpocf95hZ5qHFjA13H+fu12Rm1sp4ItXGzBjUsRmvXjuUphlpXPz4FN6btz7oWJJAIrnq9iShBaDODz+2A0/FMpSIRKZdswxe/slgDj+sMdc+N4PnvlgZdCRJEJGURyd3v93dl4Ufvwdq7RgPkUTTrGE9Xrh6IMd1y+G21+fyt/cWEU+no6V2iqQ8dpvZsPIn4UGDu2MXSUSilZGWwqOXHsmFR7XlwY+X8IuXZ1NcWnbwN4pUUSTreVwLPBO+zmHAVkJzW4lILZKSnMRdZ/eiZWY6932Yz6Yde3l4VH8a1NO6bVL9Dnrk4e6z3L0P0Bvo5e793P2r2EcTkWiZGTef2JW7zu7FxPxNXPT4F2zeuffgbxSJUqX/JTGzWyvZDoC73xujTCJyiC4a0I6cRvW4/vmZnPPIJJ65YgDtm2sFQ6k+BzryaHSQh4jUYicc0YIXrh7E9t3FnPPIJGat/iboSBJH4mqQYDmNMBf51rJNO/nRU1PZvKOIh0f157jDc4KOJLVQLAYJdjWz8WY2N/y8t5nddighRaTmdMxuyCvXDqFTTgOuGjOdJyYuY2thUdCxpI6LZG6rT4FfAI+6e7/wtrnu3rMG8lWJjjxEvm/n3hKuGzuTCYs3YQZ92jRheLdshnfLoVfrTK0fkuCiPfKI5B6+DHefWn6hPKwk6mQiEqiG9VJ4+vKjmL224L8z9d4/Pp/7Pswnq0EaR3dpzvBu2RzTJZtmDbV+iBxYJOWx2cw6AQ5gZucC62KaSkRiIinJ6Nu2CX3bNuHmE7uyrbCICfnfztT7xqyvMYPerbWqoRxYJKetOgKPAUOAbcByYJS719pJdHTaSiR6ZWXO3K8L+GTRJj5ZtPE7qxoe2zWbU3oexvBu2aSnJgcdVWIgZlOym1kDIMndd1Q1XE1ReYgcum92FTExfzOfLNrEx4s2srWwiIb1UjipewtG9mnF0M7NSUvRiobxIqHX8yin8hCpXiWlZUxetoVxX33Nu3PXs31PCU0yUhnRoyUj+7RiYIcsLY1bx6k8UHmIxFJRSRkT8zfx1ux1vD9vPYVFpTRvmMapvQ7j9N6tyMttSpKukdQ5Kg9UHiI1ZU9xKR8v3Mhbs9cxfuEG9hSX0bJxOqf1PoyRfVrRp00m+9ypKbVUTMrDzIYA7alwd5a7j6lKwJqg8hCpeYV7S/hwwQbGfbWOCYs3UVRaxuEtG3Hp4FzO6ttas/vWctVeHmb2LNAJmAWUhje7u99U5ZQxpvIQCVbB7mLembOOMZNXMn/ddhqlp3DekW25dHAuHTRBY60Ui/JYAHT3OnR+S+UhUju4OzNXbWPM5JW8PWcdxaXOMV2z+dHgXIZ3y9H4kVokFuXxEnCTu9eZgYEqD5HaZ+OOPbw4dTVjp6xkw/a9tGlan0sH5XJ+XluaNkgLOl7Ci0V5fAz0BaYC/11Vxt3PqGrIWDGzkcDIzp07X52fnx90HBHZj+LSMj6Yv4FnJq1gyvKt1EtJ4sy+rbhscHt6ts4MOl7CikV5HLu/7e7+aZTZaoyOPETqhkXrdzBm8gpenbmW3cWl9G/XhFt/0I1hXZoHHS3h6FZdVB4idU3B7mJembGGpyetYNXWXVx/XCduObGrBh7WoFis5zHIzKaZ2U4zKzKzUjPbfmgxRUS+lVk/lSuHdeC9m4/hgry2PPTxUkY9MYUN2/cEHU0qEUmtPwhcBOQD9YGrwttERKpV/bRk7j63N/ec14fZawo4bfREPsvfHHQs2Y+IjgndfQmQ7O6l7v4UMDymqUQkoZ1zZBvevGEoTTPSuPTJKdz7wWJKy+LvFHtdFkl57DKzNGCWmf3FzG4BNMpHRGKqS4tGvHHDUM7u14bR4/O59J9T2LhDp7Fqi0jK49Lw624ACoG2wDmxDCUiApCRlsI95/fhL+f2ZuaqbZw2+jMmLdVprNrgoOURXvTJgMPc/ffufmv4NJaISI04P68tb1w/jEbpKVzyxBRGj8+nTKexAhXJ3VYjCc1r9W74eV8zezPWwUREKurWshHjbhjGGX1ace8Hi/nRU1PZvHPvwd8oMRHJaas7gAHANwDuPovQDLsiIjWqQb0U/n5BX+46uxdTlm/ltNETmbJsS9CxElIk5VHi7gUxTyIiEgEz46IB7Xj9uqFkpKVw8RNTeOjjJTqNVcMiKY+5ZnYxkGxmXczsAWBSjHOJiBxQ91aNefOGoZzSsyV/fW8RVzw9ja2FRUHHShiRlMeNQA9CkyK+AGwHbo5lKBGRSDRKT+WBi/px51k9mbxsC6feP5FpK7YGHSshaG4rEYkLc9cWcP3zM1mzbTc/P6kbPz6mo9ZSj0Is5rbKM7NXzWymmc0ufxxaTBGR6tWzdSZv3TiMET1acve7C7nyGZ3GiqVIpmRfBPwCmAOUlW8Pj/+olXTkIZK43J3npqziD+Pmk9UgjQcv7kde+6ygY9V61X7kAWxy9zfdfbm7ryx/HEJGEZGYMTMuHZTLq9cNoV5qEhc89gX/+HSp7saqZpGUx+1m9oSZXWRmZ5c/Yp5MROQQ9Gydybgbh3Fyjxb8+Z2FXDVmOtt0GqvaRFIeVxBahnYEMDL8OD2WoUREqkPj9FQeurg//3dmDz7L38ypoycyY6XuxqoOKRG8po+794p5EhGRGDAzLhvcnn5tm3L98zM5/9Ev+OXJ3bj6aN2NdSgiOfL4wsy6xzyJiEgM9WqTyVs3DeOk7i24652FXPPsdIpLyw7+RtmvSMpjGKG1PBaFb9Odo1t1RaQuapyeysOj+nPbaUfw4YKNPDZhWdCR6qxITluNiHkKEZEaYmZcdXRHZqzcxv3j8zm112F0aK717aIV0Xoe+3vURDgRkVj5/Rk9qJeSxG9fnUM8zrQRaxGtYS4iEm9yGqfzm1OOYPKyLbw0fU3QceoclYeIJKwLj2rLgPZZ/PHtBWzaoYWloqHyEJGElZRk/OnsXuwuKuX34+YFHadOUXmISELrnNOQG47vzFuz1/HRwg1Bx6kzVB4ikvB+cmwnuuQ05H9fn0fh3pKg49QJKg8RSXhpKUn8+ZxefF2wm7+9vyjoOHWCykNEBDgyN4tLBuby9KQVzFr9TdBxaj2Vh4hI2C9HdKNFo3R+/cpsTV1yECoPEZGwRump/N+ZPVi4foemLjkIlYeISAUn9WjJKT1bcv/4fJZvLgw6Tq2l8hAR2YemLjm4Wl8eZnaWmT1uZm+Y2UlB5xGR+KepSw4upuVhZk+a2UYzm7vP9hHhKd6XmNmvD/Q93P11d78auBy4IIZxRUT+S1OXHFisjzyeZp8p3c0sGXgIOAXoDlxkZt3NrJeZvbXPI6fCW28Lv09EJOY0dcmBxbQ83H0CsO+CwQOAJe6+zN2LgBeBM919jrufvs9jo4XcDbzj7jMr+1lmdo2ZTTez6Zs2bYrdH0pEEkbnnIZcf5ymLtmfIK55tAZWV3i+JrytMjcCJwLnmtlPKnuRuz/m7nnunpednV09SUUk4V07PDR1yW2vzWWnpi75r0hWEqxu+1txvtLbGdx9NDA6dnFERCpXPnXJuShewIcAAAg2SURBVP+YzD3vL+L2kT0oK3N27Clh664itu0qYlthEVsLi/hmVzFbdxXxza7Q8227itm+u5iTerTkpuM7k5Jc6+9RilgQ5bEGaFvheRvg6wByiIhEpOLUJW/O+pptu4ooq+S/vClJRpOMNLIapNIkI43G9VMZPT6fqcu3MPrCfuQ0Tq/Z8DESRHlMA7qYWQdgLXAhcHEAOUREIvbLEd1IS0lid3EpWRlpNMlIJatBGk0z0mjaII2mGak0bZBGo3opmH33BMvLM9Zw2+tzOHX0Z4y+sC9DOjcP6E9RfSyWA2DM7AVgONAc2ADc7u7/NLNTgfuAZOBJd/9jdf7cvLw8nz59enV+SxGRQ7J4ww6uGzuTpZt2cvMJXbnh+M4kJ+3vLH4wzGyGu+dF/Pp4Gj1pZiOBkZ07d746Pz8/6DgiIt9RuLeE3702h9dnfc3RXZrz9wv60rxhvaBjAdGXR/xcvQHcfZy7X5OZmRl0FBGR72lQL4W/X9CXu87uxZTlWzlt9ESmLt93NEPdEFflISJS25kZFw1ox2vXDaF+ajIXPf4Fj3yylLLKrsBHaH3BHj6cX3NjUYK4YC4ikvB6tMrkzRuH8etXZnP3uwuZtmIr95zXh6YN0iJ6/ze7ipi8dAufL93MpKVbWLapEDOY9b8nkZmRGuP0cXbNo5wumItIXeHujJm8kjv/M5+cRuk8eHE/+rVr+r3XFe4tYeqKraHCWLKZ+eu24w4N0pIZ0CGLoZ2bM7hTM45o2ZikKlyIT+gL5uVUHiJS13y1+huuGzuTjTv28JtTjmDUoHbMWvUNny/dwuSlm/ly1TeUlDlpyUn0z23CkE7NGdq5Gb3bNCG1GgYfqjxQeYhI3VSwq5ifvTSLDxdsJDXZKC51kgx6tc5kSOfmDOnUjLzcLOqnJVf7z462POLqmkeFW3WDjiIiErXMjFQevyyP56euYunGQgZ1zGJgx2Zk1o/9NYxo6chDREQSe5yHiIjUDJWHiIhETeUhIiJRU3mIiEjUVB4iIhK1uCoPMxtpZo8VFBQEHUVEJK7FVXloVl0RkZoRV+UhIiI1Iy4HCZrZJmAloRUMNwccpzbQfgjRfgjRfgjRfvhWc6CBu2dH+oa4LI9yZjY9mhGT8Ur7IUT7IUT7IUT74VtV2Rc6bSUiIlFTeYiISNTivTweCzpALaH9EKL9EKL9EKL98K2o90VcX/MQEZHYiPcjDxERiQGVh4iIRC0uy8PMRpjZIjNbYma/DjpPUMxshZnNMbNZZpZQq2OZ2ZNmttHM5lbYlmVmH5hZfvhr0yAz1oRK9sMdZrY2/LmYZWanBpmxJphZWzP72MwWmNk8M/tpeHtCfSYOsB+i/kzE3TUPM0sGFgM/ANYA04CL3H1+oMECYGYrgDx3T7iBUGZ2DLATGOPuPcPb/gJsdfc/h/9T0dTdfxVkzlirZD/cAex0978Fma0mmdlhwGHuPtPMGgEzgLOAy0mgz8QB9sP5RPmZiMcjjwHAEndf5u5FwIvAmQFnkhrm7hOArftsPhN4JvzrZwj9pYlrleyHhOPu69x9ZvjXO4AFQGsS7DNxgP0QtXgsj9bA6grP11DFnRMHHHjfzGaY2TVBh6kFWrj7Ogj9JQJyAs4TpBvMbHb4tFZcn6rZl5m1B/oBU0jgz8Q++wGi/EzEY3nYfrbF17m5yA119/7AKcD14VMYIo8AnYC+wDrgnmDj1Bwzawi8Atzs7tuDzhOU/eyHqD8T8Vgea4C2FZ63Ab4OKEug3P3r8NeNwGuETuklsg3hc77l5343BpwnEO6+wd1L3b0MeJwE+VyYWSqhfzDHuvur4c0J95nY336oymciHstjGtDFzDqYWRpwIfBmwJlqnJk1CF8Qw8waACcBcw/8rrj3JvCj8K9/BLwRYJbAlP9jGfZDEuBzYWYG/BNY4O73VvithPpMVLYfqvKZiLu7rQDCt5ndByQDT7r7HwOOVOPMrCOhow2AFOD5RNoPZvYCMJzQVNMbgNuB14F/A+2AVcB57h7XF5Mr2Q/DCZ2ecGAF8OPy8/7xysyGAROBOUBZePNvCZ3vT5jPxAH2w0VE+ZmIy/IQEZHYisfTViIiEmMqDxERiZrKQ0REoqbyEBGRqKk8REQkaioPERGJmspDRESipvIQqWbhtRF+HqPvfaKZPRuL7y0SDZWHyCGwkCr/ParC+/sAX1b154lUF5WHyAGY2a1mNjf8uDm8rX14JbaHgZlAWzP7XXj1yg+BbhXef4mZTQ2vzvaomSXv7/1RROoDfGlm9czsaTP7U3i+IpEapfIQqYSZHQlcAQwEBgFXm1m/8G93I7Q6Xz9C80ZdSGhthLOBo8LvPwK4gNDU+H2BUmDUvu9395VRxOpDaObX94AP3f23rjmGJAApQQcQqcWGAa+5eyGAmb0KHE1oJtaV7v5F+HVHh1+3K/y68lmcTwCOBKaFDw7qE/qHf8I+749IeCrt9sALhCaum1z1P5rIoVF5iFTuQKeDCvd5vr///RvwjLv/5jsbQyu47fv+ir9/PXB1+Omp5euyAN0JLTmQRegoRiQwOm0lUrkJwFlmlhFeE+WHhKaz3t/rfmhm9cNrqIwMbx8PnGtmOQBmlmVmuQf7oe7+kLv3DT8qLmTWB5hE6BTZU2bWoup/NJFDo/IQqYS7zwSeBqYSWvfhCXf/3p1O4df9C5hFaIW2ieHt84HbCK0jPxv4ADhs3/dHoQ8w190XA78C/h0+lSVS47Seh4iIRE1HHiIiEjWVh4iIRE3lISIiUVN5iIhI1FQeIiISNZWHiIhETeUhIiJR+/9AAk1J12jyTwAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "mean_alignment_k = [np.mean(cells_alignment_k) for cells_alignment_k in cells_alignment_k_list]\n",
    "\n",
    "ax = plt.subplot(xlabel='order - $k$', ylabel = 'mean elongation alignment')\n",
    "ax.plot(range(1,len(mean_alignment_k)+1), mean_alignment_k)\n",
    "ax.set_yscale('log')\n",
    "plt.savefig('cell_alignment.png',dpi=600)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save alignment data to table\n",
    "\n",
    "Save cell elongation alignment for every cell up to order 3:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "max_save_order = 3\n",
    "\n",
    "# Put cell alignment value in cells table and save to DB.\n",
    "for k in range(max_save_order):\n",
    "    cells['elongation_alignment_order_' + str(k+1)] = cells_alignment_k_list[k]\n",
    "    \n",
    "MMTool.table_io_per_frame(datapath_movie_frames, table_name = 'cells', frameIdx = frameIdx, network_type = 'cells', action = 'save', table = cells)\n",
    "print('done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create VTK files to plot cells colored by alignment value\n",
    "To plot cells colored with their alignment value at orders $0, \\ldots,k$, goto the movie root dir and type\n",
    "\n",
    "```bash\n",
    "    python /path/to/cureved_tm_dir/curvedtm2vtk -n cells -s 'elongation_alignment_order_0' 'elongation_alignment_order_1' ...\n",
    "```\n",
    "\n",
    "or execute from console. Make sure to set path of python interperter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Save current working dir\n",
    "curwd = os.getcwd()\n",
    "curvedtm2vtk_path = os.path.join(curwd, 'curvedtm2vtk.py')\n",
    "\n",
    "python_interp = ''\n",
    "\n",
    "os.chdir(datapath_movie_name)\n",
    "!$python_interp $curvedtm2vtk_path -g $frameIdx -n cells -s 'elongation_alignment_order_1' 'elongation_alignment_order_2' 'elongation_alignment_order_3'\n",
    "\n",
    "#Return to this dir.\n",
    "os.chdir(curwd)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
