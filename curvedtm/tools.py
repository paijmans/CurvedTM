#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thursday Oct 18 2018

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore
import scipy.linalg as lin
import pickle

from curvedtm import MMDef

def angle_difference(angle1, angle2):
    """ Calculates the difference between two angles angle2 and angle1.
    
    Returns:
    --------
        angle: angle2 - angle1    
    """
    return np.angle( np.exp(1j*(angle2 - angle1)) )


def rotation(tensor_array, angle_array, unit_axis_array):
    """ Rotate a rank 1 or 2 tensor around a certain axis and angle.
    
    (From Dr. Duclut:) The computation goes as follow:
    let 'u' be the unit vector along 'axis', i.e. u = axis/norm(axis)
    and A = I × u be the skew-symmetric matrix associated to 'u', i.e.
    the cross product of the identity matrix with 'u'
    then rot = exp(θ A) is the 3d rotation matrix.    
    
    Parameters:
    -----------
    tensor_array : list like, size N x rank x dim
        Array of tensors with rank 1 or 2 and dimension dim.
    angle_array : list like, size N
        Array of angles in radians.
    unit_axis_array : list lie, size N x dim
        Array of unit vectors of dimension dim defining the rotation axis.
        
    Returns:
    --------
    rotated_tensor_array : Numpy.array, size N x rank x dim
        Array of rotated tensors.
    """

    # Determine dimension and rank of input tensor.
    dim  = tensor_array.shape[-1]
    rank = len(tensor_array.shape) - 1

    assert rank == 1 or rank == 2

    rot_array = \
        np.array([lin.expm(np.cross(np.eye(3), axis * angle)) for axis, angle in zip(unit_axis_array, angle_array)])

    rotated_tensor_array = np.array([])
    if rank == 1:
        rotated_tensor_array = np.array([np.dot(rot, tensor) for rot, tensor in zip(rot_array, tensor_array)])
    elif rank == 2:
        rotated_tensor_array = np.array([np.dot(rot, tensor).dot(rot.T) for rot, tensor in zip(rot_array, tensor_array)])

    return rotated_tensor_array


def unit_vector(vectors):
    """ Normalize array of vectors
    
    Parameters
    ----------
    vectors : numpy.array / list N x d
        Array or list with M d-dimensional vectors.
        
    Results
    -------
    unit_vectors : numpy.array
        M normalized vectors
    """
    
    vector_length = np.sqrt(np.sum(vectors**2, axis=1))
    vector_length[vector_length < MMDef.EPSILON] += MMDef.EPSILON
    
    unit_vectors = [vec/l for vec, l in zip(vectors, vector_length)]
    
    return np.array(unit_vectors)


def check_dbonds_consistency(dbonds, strict = True):
    """ Check if DCEL (bonds) table topology is consistent."""

    next_dbond_vertex_id = dbonds.loc[dbonds['left_dbond_id']]['vertex_id'].values
    conj_dbond_vertex_id = dbonds.loc[dbonds['conj_dbond_id']]['vertex_id'].values
    
    dbonds_table_consistency = sum(next_dbond_vertex_id == conj_dbond_vertex_id) == len(dbonds)
    
    if dbonds_table_consistency:
        print( 'dbond table consistency: ' + MMDef.txtcolors.OKGREEN + 'PASSED' + MMDef.txtcolors.ENDC + ', Consistent dbonds: ' + MMDef.txtcolors.BOLD\
              + str(sum(next_dbond_vertex_id == conj_dbond_vertex_id)) + MMDef.txtcolors.ENDC + ', total dbonds: ' + MMDef.txtcolors.BOLD + str(len(dbonds))\
              + MMDef.txtcolors.ENDC )
    else:
        print('dbond table consistency: ' + MMDef.txtcolors.FAIL + 'FAILED' + MMDef.txtcolors.ENDC + ', Consistent dbonds: ' + MMDef.txtcolors.BOLD\
              + str(sum(next_dbond_vertex_id == conj_dbond_vertex_id)) + MMDef.txtcolors.ENDC + ', total dbonds: ' + MMDef.txtcolors.BOLD + str(len(dbonds))\
              + MMDef.txtcolors.ENDC)
        
    if strict:
        assert dbonds_table_consistency


# 
def sortVertexCellIds_cc(vertex_id, dbonds):
    """ Given vertex_id, return ids of connected cells in counter clockwise direction, starting with lowest cell id.
    
        Parameters:
        -----------
        vertex_id : int
            Vertex id
        dbonds : Pandas.DataFrame
            DCEL table
            
        Returns:
        --------
        sorted_cell_ids : list
    """

    selected_dbonds = dbonds['vertex_id'] == vertex_id
    connected_cell_ids, connected_conj_dbond_ids =\
    dbonds[selected_dbonds][['cell_id', 'conj_dbond_id']].values.T

    conj_dbonds_cell_ids = dbonds.iloc[connected_conj_dbond_ids]['cell_id'].values

    if sum(conj_dbonds_cell_ids == MMDef.BOUNDARY_CELL_ID) > 1:
        return conj_dbonds_cell_ids

    sorted_cell_ids = [connected_cell_ids.min()]
    while(len(sorted_cell_ids) < len(connected_cell_ids)):
        sorted_cell_ids.append(connected_cell_ids[np.where(conj_dbonds_cell_ids==sorted_cell_ids[-1])[0]][0])

    return sorted_cell_ids


def get_vertex_connected_cell_ids_cc(dbonds):
    """ For each vertex, find connected cell ids in counter-clockwise direction.
        Return seperate dictionaries for 2, 3 and higher order vertices. 
        
        Parameters:
        -----------
        dbonds : Pandas.DataFrame
            DCEL table
            
        Returns:
        --------
        sorted_cell_ids_per_2_vertex_id, sorted_cell_ids_per_3_vertex_id, sorted_cell_ids_per_higher_order_vertex_id
            Three dictionaries vertex_id --> sorted cell ids.
        
        """

    sorted_cell_ids_per_2_vertex_id = {}
    sorted_cell_ids_per_3_vertex_id = {}
    sorted_cell_ids_per_higher_order_vertex_id = {}

    print('Sort cell_ids in counter-clockwise direction around each vertex.')
    
    vertex_ids = np.unique(dbonds['vertex_id'].values)
    for vertex_index in vertex_ids:

        if(vertex_index%1000 == 0):
            print(vertex_index, ":", len(vertex_ids))

        sorted_cell_ids =  sortVertexCellIds_cc(vertex_index, dbonds)

        if len(sorted_cell_ids) == 2:
            sorted_cell_ids_per_2_vertex_id[vertex_index] = sorted_cell_ids
        elif len(sorted_cell_ids) == 3:
            sorted_cell_ids_per_3_vertex_id[vertex_index] = sorted_cell_ids
        elif len(sorted_cell_ids) > 3:
            sorted_cell_ids_per_higher_order_vertex_id[vertex_index] = sorted_cell_ids

    return sorted_cell_ids_per_2_vertex_id, sorted_cell_ids_per_3_vertex_id, sorted_cell_ids_per_higher_order_vertex_id


def make_triangle_DCEL_table(triangles, cell_network_DCEL_table):
    """ Make a doubly connected edge list (DCEL) for the subcellular triangle network.

        Parameters:
        -----------
        cell_network_DCEL_table : Pandas.DataFrame
            DCEL table of the cell network.

        Returns:
        --------
        dbonds : Pandas.DataFrame
            DCEL Table with columns: ['triangle_id', 'left_dbond_id', 'vertex_id', 'conj_dbond_id', 'bond_id'],
            indexed by dbond id.
    """

    print('Making doubly connected edge list for the triangle network...')

    ### Create all the dbonds in the triangle mesh: 
    # - Internal dbonds: Inside the triangles
    # - External dbonds: From the boundary dbonds from cell_network_DCEL_table table.

    #Find the boundary dbonds from the cell_dbonds table.
    boundary_cell_dbonds  = cell_network_DCEL_table[cell_network_DCEL_table['cell_id'] == MMDef.BOUNDARY_CELL_ID]

    #Determine the total number of dbonds in the triangle network.
    N_triangles       = len(triangles)
    N_triangle_dbonds = 3 * N_triangles
    N_boundary_dbonds = len(boundary_cell_dbonds)

    #Create empty dataframe for the triangle and tissue boundary dbonds tables.
    triangles_dbonds = pd.DataFrame(MMDef.BOUNDARY_TRIANGLE_ID, index=range(N_triangle_dbonds),\
                                    columns=['triangle_id', 'conj_dbond_id', 'left_dbond_id', 'vertex_id']).astype(dtype=int)
    boundary_dbonds  = pd.DataFrame(MMDef.BOUNDARY_TRIANGLE_ID, index=range(N_triangle_dbonds, N_triangle_dbonds + N_boundary_dbonds),\
                                    columns=['triangle_id', 'conj_dbond_id', 'left_dbond_id', 'vertex_id']).astype(dtype=int) 

    #For dbonds belonging to triangle, for each pair of three dbonds, set triangle_id, left_dbond_id and vertex_id.
    triangles_dbonds.loc[0::3, 'triangle_id'] = triangles.index
    triangles_dbonds.loc[1::3, 'triangle_id'] = triangles.index
    triangles_dbonds.loc[2::3, 'triangle_id'] = triangles.index

    triangles_dbonds.loc[0::3, 'left_dbond_id'] = triangles_dbonds.loc[1::3].index
    triangles_dbonds.loc[1::3, 'left_dbond_id'] = triangles_dbonds.loc[2::3].index
    triangles_dbonds.loc[2::3, 'left_dbond_id'] = triangles_dbonds.loc[0::3].index

    triangles_dbonds.loc[0::3, 'vertex_id'] = triangles['vertex_id_1'].values
    triangles_dbonds.loc[1::3, 'vertex_id'] = triangles['vertex_id_2'].values
    triangles_dbonds.loc[2::3, 'vertex_id'] = triangles['vertex_id_3'].values

    #For dbonds belonging to tissue boundary, find right and left vertex ids.
    boundary_dbonds['vertex_id']   = boundary_cell_dbonds['vertex_id'].values
    boundary_dbonds_left_vertex_id = cell_network_DCEL_table.loc[ boundary_cell_dbonds['conj_dbond_id'] ]['vertex_id']

    #For each boundary dbond, find the boundary dbond that has the right_vertex_id equal this dbond left_vertex_id 
    boundary_dbonds_left_dbond_index = [np.where(boundary_dbonds['vertex_id'] == lvid)[0] for lvid in boundary_dbonds_left_vertex_id]
    boundary_dbonds_left_dbond_id    = np.array([boundary_dbonds.iloc[idx].index[0] for idx in boundary_dbonds_left_dbond_index])

    boundary_dbonds.loc[ boundary_dbonds.index, 'left_dbond_id'] = boundary_dbonds_left_dbond_id

    #Merge the triangle and boundary dbonds tables.
    dbonds = pd.concat((triangles_dbonds, boundary_dbonds))
    dbonds = dbonds.astype(dtype=int)


    ### Assign the conj_dbond_id to each dbond in the table.        
    #For each dbond, find left and right vertex id, make pairs, and assign unique id to each pair.
    #Note that the swapped pair will give the pair_id of the conjugate bond.
    right_vertex_ids = dbonds['vertex_id'].values
    left_vertex_ids  = dbonds.loc[ dbonds['left_dbond_id'] ]['vertex_id'].values

    dbond_vertex_id_pairs      = np.array([right_vertex_ids, left_vertex_ids]).T
    conj_dbond_vertex_id_pairs = np.array([left_vertex_ids, right_vertex_ids]).T

    dbond_vertex_pair_ids      = np.array([ pairing_function(list(vid_pair)) for vid_pair in dbond_vertex_id_pairs])
    conj_dbond_vertex_pair_ids = np.array([ pairing_function(list(vid_pair)) for vid_pair in conj_dbond_vertex_id_pairs])        

    #For each dbond, find the index of its conjugate.
    conj_dbond_idx = np.array([np.where(conj_dbond_vertex_pair_ids == vertex_pair_id)[0] for vertex_pair_id in dbond_vertex_pair_ids])

    # There are two possibilities for each dbond_idx:
    # 1. 1  conj_dbond found -> assign conj_dbond to dbond.
    # 2. >1 conj_dbond found -> dbond and conj_dbond are part of a 2-vertex cell, such that two vertices are connected by more than 2 dbonds.
    conj_dbond_order    = np.array([len(dbond_idx) for dbond_idx in conj_dbond_idx])
    normal_dbonds_idx   = np.where(conj_dbond_order == 1)[0]
    multi_dbonds_idx    = np.where(conj_dbond_order > 1)[0]        

    #1. Set al the dbonds with only one conj_dbond.
    normal_conj_dbonds_idx = np.array([ conj_dbond_idx[dbond_idx][0] for dbond_idx in normal_dbonds_idx])
    dbonds.loc[normal_dbonds_idx, 'conj_dbond_id'] = dbonds.loc[normal_conj_dbonds_idx].index        

    # 2. dbonds with multiple possible conj_dbonds: choose that dbond, which 
    # has the shortest distance between the cell center positions of the cell
    # belonging to this dbond and its potential conj_dbond.
    for multi_dbond_idx in multi_dbonds_idx:

        #Find the cell_id of this dbond.
        dbond_triangle_id = dbonds.loc[multi_dbond_idx, 'triangle_id']
        if dbond_triangle_id != -1:
            dbond_cell_id = triangles.loc[dbond_triangle_id, 'cell_id']
        else:
            dbond_cell_id = MMDef.BOUNDARY_CELL_ID

        #Find cell_ids of the possible conj_dbonds.
        conj_dbonds_idx         = conj_dbond_idx[multi_dbond_idx]  
        conj_dbonds_triangle_id = dbonds.loc[conj_dbonds_idx, 'triangle_id'].values.astype(dtype=int)
        boundary_conj_dbonds_idx = np.where(conj_dbonds_triangle_id == -1)[0]
        internal_conj_dbonds_idx = np.where(conj_dbonds_triangle_id != -1)[0]

        conj_dbonds_cell_id = np.array([MMDef.BOUNDARY_CELL_ID]*len(conj_dbonds_idx))
        conj_dbonds_cell_id[internal_conj_dbonds_idx] = triangles.loc[conj_dbonds_triangle_id[internal_conj_dbonds_idx], 'cell_id'].values.astype(dtype=int)

        #Find counter-clockwise ordered cell_ids around right vertex of this dbond.
        right_vertex_sorted_cell_ids =  sortVertexCellIds_cc(dbonds.loc[multi_dbond_idx, 'vertex_id'], cell_network_DCEL_table)
        right_vertex_sorted_cell_ids = np.array(right_vertex_sorted_cell_ids)
        
        #The conj_dbond of this dbond is the conj_dbond with the cell_id that is right of the cell_id of this dbond.
        dbond_cell_idx   = np.where(right_vertex_sorted_cell_ids == dbond_cell_id)[0]        
        dbond_cell_idx = int(dbond_cell_idx[0])
        
        selected_cell_id = right_vertex_sorted_cell_ids[dbond_cell_idx - 1]
        conj_dbond_with_neigbor_cell_id = np.where(conj_dbonds_cell_id == selected_cell_id)[0]

        #Assign selected conj_dbond to this dbond.
        dbonds.at[multi_dbond_idx, 'conj_dbond_id'] = conj_dbonds_idx[ conj_dbond_with_neigbor_cell_id[0] ]

    #Add bond id to each dbonb. The bond id is the same for each dbond and its conjugate dbond.
    dbonds['bond_id'] = dbonds.index
    conjugate_edges = dbonds[dbonds.index > dbonds['conj_dbond_id']]['conj_dbond_id'].copy()
    dbonds.loc[conjugate_edges.index, 'bond_id'] = conjugate_edges.values        

    return dbonds

def create_hdf5_filename(frameIdx):
    """ Create filename for hdf5 database file for each frame labeled with frameIdx.
    """
    
    return 'data_frame_' + "%04d" % (frameIdx,) + '.hdf5'

def check_hdf5_db_files(frames_to_check, path_hdf5_DB):
    """ Check whether requested hdf5 database files exists.
    
    Parameters:
    -----------
    frames_to_check : list like
        Indices of frames to check whether they exist in 'path_hdf5_DB'.
    path_hdf5_DB : string
        Path to database directory.
    
    Returns:
    --------
    db_file_exists, list of booleans with len(db_file_exists) = len(frames_to_check)
        Whether the requested database file was present for the frame.
    """        
        
    db_file_exists = np.array([False]*len(frames_to_check))
    for idx, frameIdx in enumerate(frames_to_check):
        filepath_hdf5 = os.path.join(path_hdf5_DB, create_hdf5_filename(frameIdx))
        db_file_exists[idx] = os.path.isfile(filepath_hdf5)
            
    return db_file_exists


def table_io_per_frame(path_outputdir, table_name, frameIdx, network_type = 'cells', action = 'load', table = [], verbose=False):
    """ Load and save tables from the curved Tissue Miner HDF5 file.
    
        Parameters:
        -----------
        path_outputdir : string
            Path to curved tissue miner output directory.
        table_name : string
            Name of table to load/save.
        frameIdx : int
            Index of frame to load/save.
        network_type : ['cells', 'dual_triangles', 'subcellular_triangles', 'cell_tensor', 'triangle_tensor', 'tissue_average']
            Network type to load.
        action : ['load', 'save']
            Whether to load or save a table from/to the database.
        table : pd.DataFrame / cells_sorted_dbonds dict
            Table / dict to save to the database.
        verbose : [False, True]
            Verbose output.            
        
        Returns:
        --------
        table : Pandas.DataFrame / dict
            Required table.
        
    """
    
    def convert_to_matrix(table):
        """ Convert Pandas.DataFrame to Numpy.ndarray in matrix form: (...,3,3), (...,2,2) or (...,1)
        """
        
        if isinstance(table, pd.Series):
            return table.values
               
        N_tensor_elements = len(table.iloc[0])
        
        if N_tensor_elements == 9:     
            return table.values.reshape((-1,3,3))
        if N_tensor_elements == 4:
            return table.values.reshape((-1,2,2))
        if N_tensor_elements == 3:
            return table.values.reshape((-1,3))
        if N_tensor_elements == 2:
            return table.values.reshape((-1,2))               

    def cells_sorted_dbonds_io(file_io, hdf5_DB_path, action, cells_dbonds_dict=[]):
        """ Convert the dictionary cell_id --> 'counter clockwise sorted dbonds' to:
             - Pandas.Series: cells neighbour number indexed by cell_id.
             - Pandas.Series: cells sorted dbond_id indexed (0....'Number of dbonds')
        """
        
        hdf5_DB_path_nn = hdf5_DB_path + '_nn'
        hdf5_DB_path_db = hdf5_DB_path + '_dbonds_id'
        
        if action == 'load':
            
            #Load data from DB.
            cells_nn        = file_io.get(hdf5_DB_path_nn)
            cells_dbonds_id = file_io.get(hdf5_DB_path_db)
            
            # Convert data to dict (cell_id, [cell_sorted_dbonds])
            cells_sorted_dbonds = [[]]*len(cells_nn)
            cells_id =[-1]*len(cells_nn)
            idx = 0
            for cell_idx, (cell_id, nn) in enumerate(cells_nn.iteritems()):
                cells_sorted_dbonds[cell_idx] = np.array(cells_dbonds_id[idx:idx+nn])
                cells_id[cell_idx] = cell_id
                idx += nn
                
            return dict(zip(cells_id, cells_sorted_dbonds))
            
        if action == 'save':
            
            cells_nn = [len(a) for a in cells_dbonds_dict.values()]
            total_dbonds = sum(cells_nn)
            
            cells_dbonds_ids = [-1]*total_dbonds
            idx = 0
            for nn, cell_dbonds_id in zip(cells_nn, cells_dbonds_dict.values()):
                cells_dbonds_ids[idx:idx+nn] = cell_dbonds_id[:]
                idx += nn
           
            # Save data to DB.
            file_io.put(hdf5_DB_path_nn, pd.Series(cells_nn, index=cells_dbonds_dict.keys()))
            file_io.put(hdf5_DB_path_db, pd.Series(cells_dbonds_ids))
            
            return cells_dbonds_dict
    
    ### PERFROM CHECKS ON PARAMETERS
    # Check whether output path exists.
    assert os.path.isdir(path_outputdir), 'Curved tissue miner output directory does not exist. Given: '+path_outputdir
    # Check correct network type.
    assert network_type in MMDef.valid_networks or 'patches' in network_type, "Wrong network type given: " + str(network_type)+". Allowed types: "+str(MMDef.valid_networks)
    # Check correct action.
    assert action in MMDef.valid_file_actions, "Wrong file action given:" + str(action)+". Allowed actions: "+str(MMDef.valid_file_actions)
    
    ### CREATE HDF5 FILE POINTER to required frame.
    filepath_hdf5 = os.path.join(path_outputdir,  create_hdf5_filename(frameIdx))
    with HDFStore(filepath_hdf5) as hdf5_file_obj:
            
        ### CREATE TABLE NAME for hdf5 database.
        if network_type == 'cells':
            assert table_name in MMDef.valid_cell_table_names, "Wrong table name for cells network given. Allowed: "+str(MMDef.valid_cell_table_names)
        elif network_type == 'dual_triangles' or network_type == 'subcellular_triangles' or network_type == 'tissue_average' or 'patches' in network_type:
            assert table_name in MMDef.valid_triangle_table_names, "Wrong table name for triangles network given. Allowed: "+str(MMDef.valid_triangle_table_names)        
        else:
            print('Requested network type', network_type, 'not supported.')
            sys.exit()
        hdf5_table_name = '/' + network_type + '/' + table_name

        ### LOAD data from database and RETURN table.
        if action == 'load':
            if verbose:
                print('Loading table', hdf5_table_name)

            try:
                if table_name == 'sorted_cell_dbonds_per_frame':
                    table = cells_sorted_dbonds_io(hdf5_file_obj, hdf5_table_name, action)
                else:
                    table = hdf5_file_obj.get(hdf5_table_name)
            except:
                hdf5_file_obj.close()
                print('Requested hdf5 table name', hdf5_table_name, 'does not exist.')
                table = None

            # Close file channel.
            hdf5_file_obj.close()    
                
            # Return tensor as Numpy.ndarray of matrices.
            if ('tensor' in table_name) and (type(table) == pd.DataFrame or type(table) == pd.Series):
                return convert_to_matrix(table)
            else:
                return table

        ### SAVE data to database and RETURN table.
        is_pandas_obj = type(table) == pd.DataFrame or type(table) == pd.Series
        is_sorted_dbonds_dict = table_name == 'sorted_cell_dbonds_per_frame' and type(table) == dict

        if action == 'save' and (is_pandas_obj or is_sorted_dbonds_dict):        
            if verbose:
                print('Saving table', hdf5_table_name)

            if table_name == 'sorted_cell_dbonds_per_frame':
                table = cells_sorted_dbonds_io(hdf5_file_obj, hdf5_table_name, action, table)
            else:
                hdf5_file_obj.put(hdf5_table_name, table)

            # Close file channel.
            hdf5_file_obj.close()
            return table

        else:
            print('ERROR: Can only save Pandas DataFrame or Series objects. Given:', type(table))
            print('Type check:', (type(table) == pd.DataFrame or type(table) == pd.Series))
            hdf5_file_obj.close()
            sys.exit()

            
            
        
def calculate_tensor_eigenvalues_and_eigenvector(tensors, return_eigen_property = 'max'):
    """ Get tensor eigenvalues and eigenvectors with specified property.

        Parameters
        ----------
        tensors : Numpy.ndarray, shape: M x 3 x 3
            Array of 3x3 matrices defining the tensor.
        return_eigen_property : {'max', 'min', 'abs_max', 'abs_min', 'sorted_basis'}
            Specify which eigenvalues/ eigenvectors to return. Options are: 
                - max (default): return eigenvalue/vec with highest value
                - min          : return eigenvalue/vec with lowest value
                - abs_max      : return eigenvalue/vec with highest norm
                - abs_min      : return eigenvalue/vec with smallest norm
                - snd_abs_max  : return eigenvalue/vec with second largest norm.
                - non_zero_max : returns largest eigenvalue that is not closest to zero.
                - non_zero_min : returns smallest eigenvalue that is not closest to zero.
                - sorted_basis : return base vectors and values sorted by the magnitude of the eigenvector.

        Returns
        -------
        tensors_spec_eigenvalue : Numpy.array, shape: M x 1 (3)
            Array of eigenvalues

        tensors_spec_eigenvector : Numpy.array, shape: M x 3 (x 3)
            tensor_id -> requested eigenvector
    """

    # For each tensor find real component of eigenvector and eigenvalue.
    tensors_eigenvalues, tensors_eigenvecs = np.linalg.eig(tensors)
    
    real_eigenvalues  = np.real(tensors_eigenvalues)
    real_eigenvectors = np.real(tensors_eigenvecs)

    # Get index of required eigenvalue(s) v1, v2, v3:
    specified_eigenvalue_index = np.array([])
    if   return_eigen_property == 'abs_max': # First largest magnitude
        specified_eigenvalue_index = np.argmax(abs(real_eigenvalues), axis=1)
    elif return_eigen_property == 'snd_abs_max': # Second largest magnitude.
        specified_eigenvalue_index = np.argsort(-abs(real_eigenvalues), axis=1).T[1]     
    elif return_eigen_property == 'abs_min': #Closest to zero.
        specified_eigenvalue_index = np.argmin(abs(real_eigenvalues), axis=1)
    elif return_eigen_property == 'min': # Smallest
        specified_eigenvalue_index = np.argmin(real_eigenvalues, axis=1)
    elif return_eigen_property == 'non_zero_max': # Largest not closest to zero.
        max_idx, mid_idx, min_idx = np.argsort(-abs(real_eigenvalues), axis=1).T
        non_zero_max_idx = [mx if v1v2v3[mx] > v1v2v3[md] else md for mx, md, v1v2v3 in zip(max_idx, mid_idx, real_eigenvalues)]        
        specified_eigenvalue_index = np.array(non_zero_max_idx)
    elif return_eigen_property == 'non_zero_min': # Smallest not closest to zero.
        max_idx, mid_idx, min_idx = np.argsort(-abs(real_eigenvalues), axis=1).T
        non_zero_min_idx = [md if v1v2v3[mx] > v1v2v3[md] else mx for mx, md, v1v2v3 in zip(max_idx, mid_idx, real_eigenvalues)]
        specified_eigenvalue_index = np.array(non_zero_min_idx)    
    elif return_eigen_property == 'sorted_basis': #Sorted by eigenvalues: |v1| > |v2| > |v3|
        specified_eigenvalue_index = np.argsort(-abs(real_eigenvalues), axis=1)
    else: #return_eigen_property == 'max': #Largest
        specified_eigenvalue_index = np.argmax(real_eigenvalues, axis=1)

    # Get required eigenvalue and corresponding eigenvectors.
    tensors_spec_eigenvalue = [tensor_eigvals[spec_idx] for spec_idx, tensor_eigvals in zip(specified_eigenvalue_index, real_eigenvalues)]
    
    if return_eigen_property == 'sorted_basis':
        tensors_spec_eigenvector = [[eigen_vecs[:, spec_idx] for spec_idx in spec_indices] 
                                    for spec_indices, eigen_vecs in zip(specified_eigenvalue_index, real_eigenvectors)]
    else:
        tensors_spec_eigenvector = [eigen_vecs[:, spec_idx] 
                                    for spec_idx, eigen_vecs in zip(specified_eigenvalue_index, real_eigenvectors)]
        
    return np.array(tensors_spec_eigenvalue), np.array(tensors_spec_eigenvector)


def calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles, triangles_tensor, patches_triangles_id_dict):
    """ Calculate area weighted average of the tensors defined on each triangle over patches of triangles.
    
        Parameters:
        -----------
        triangles : Pandas.DataFrame, size M
            Table with triangle state properties: ['area'].
        triangles_tensor : Numpy.ndarray, size M x 3 x 3
            Matrices defining the tensor on each triangle, indexing must be equal to triangles table. Can be 0-order tensor (scalar)
        patches_triangles_id_dict : dict, size N
            Dictionary defining a patch by the indices of a set of triangles. (patch_id, [triangles_idx])
        
        Returns:
        --------
        averaged_tensors : Numpy.ndarray, shape N x 3 x 3
            Tensor defined on each patch, indexed as given in patch_triangles_id_dict.
    
    """

    assert len(triangles) == len(triangles_tensor), 'ERROR: Number of triangles and tensors does not equal.'
            
    # For all groups of triangles, find their tensors and areas.
    triangles_area = triangles['area'].values 
    
    patches_triangles_tensor = [triangles_tensor[triangles_index] for patch_id, triangles_index in patches_triangles_id_dict.items()]
    patches_triangles_area   = [triangles_area[triangles_index] for patch_id, triangles_index in patches_triangles_id_dict.items()]
    
    # Calculate area weighted averages of the grouped tensors.
    patches_area             = [np.sum(triangles_area) for triangles_area in patches_triangles_area]
    patches_averaged_tensors = np.array([(patch_triangles_area *  patch_triangles_tensor.T).T.sum(axis=0)/(patch_area + MMDef.EPSILON)
                                         for patch_triangles_area, patch_triangles_tensor, patch_area in
                                         zip(patches_triangles_area, patches_triangles_tensor, patches_area)])
    #Old slower method for loops
#     patches_averaged_tensors = np.array([sum([area * tensor for area, tensor in zip(triangles_area, triangles_tensor)]) / (patch_area + MMDef.EPSILON)
#                                          for triangles_area, triangles_tensor, patch_area in
#                                          zip(patches_triangles_area, patches_triangles_tensor, patches_area)])

    return patches_averaged_tensors


# Given cells and triangle tables and a dictionary with triangle tensors,
# calculate area weighted mean of these tensors for every cell and return as dictionary.
def calculate_cell_tensor_from_triangle_tensors(cells, triangles, triangles_tensor):
    """ Calculate the area weighted average of a set of triangle tensors over a cell. 
        Cell specific wrapper of calculate_area_weighted_mean_tensor_from_triangle_tensors()
    """

    #Group triangle index by cell_id:
    cells_triangles_idx = np.array([triangles[triangles['cell_id'] == cell_id].index.values for cell_id in cells.index])
    cells_triangles_index_dict = dict(zip(cells.index, cells_triangles_idx))

    #Calculate area weighted mean tensor on each cell.
    cells_mean_tensor =  calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles, triangles_tensor, cells_triangles_index_dict)

    return cells_mean_tensor


#Given cell, triangle and triangle scalar tables, calculate the area weighted mean a scalar defined on the triangles.
def update_cell_table_with_tensors_from_triangle_tensors(cells, triangles, triangles_tensor, triangles_tensor_name, eigenvalue_property='max'):
    """ Update cell table with area weighted triangle tensors.
    
    Parameters:
    -----------
    ...
    
    Returns:
    --------
    Cells : Pandas.DataFrame, size N
        Cells table, updated with: []
    cells_mean_tensor : Numpy.ndarray, shape N x (tensor shape)
        Area weighted average of triangle tensor on each cell.
    """

    print('Update cells table with the area weighted', triangles_tensor_name, 'from the triangles.')

    #Calculate cell tensor from the area weighted mean of the triangle tensors.
    cells_mean_tensor =  calculate_cell_tensor_from_triangle_tensors(cells, triangles, triangles_tensor)

    # If given tensors are scalars
    if len(cells_mean_tensor.shape) == 1:
        cells[triangles_tensor_name] = cells_mean_tensor
    else:
        #Find eigenvalue and eigenvecor with property eigenvalue_property from averaged tensors.
        cells_tensor_eigenvalue, cells_tensor_eigenvector =  calculate_tensor_eigenvalues_and_eigenvector(cells_mean_tensor, eigenvalue_property)
        cells[triangles_tensor_name + '_norm_' + eigenvalue_property] = cells_tensor_eigenvalue

    return cells, cells_mean_tensor


#Given cells and triangle tables, calculate area weighted mean and gaussian curvature of each cell and update cells table.
def update_cell_table_with_curvature_from_triangle_curvatures(cells, triangles):
    """ Update cells table with area weighted average of the triangle mean and gaussian curvatures.
    
        Parameters:
        -----------
        cells : Pandas.DataFrame
            Cells table
        triangles : Pandas DataFrame
            Triangles table with columns ['area', 'cell_id', 'mean_curvature', 'gaussian_curvature'].
            
        Returns
        -------
        cells : Pandas.DataFrame
            Cells table updated with columns ['mean_curvature', 'gaussian_curvature'].
    
    """

    cells, cells_mean_curv =  update_cell_table_with_tensors_from_triangle_tensors(cells, triangles, triangles['mean_curvature'].values, 'mean_curvature')
    cells, cells_gaus_curv =  update_cell_table_with_tensors_from_triangle_tensors(cells, triangles, triangles['gaussian_curvature'].values, 'gaussian_curvature')
    
    return cells


#Generate unique triangle ids using the cell_ids, given dbonds and triangles. Triangle table is updated.
#Here, we use a pairingfunction on the cell_id of the triangle and the cell_id of its neighbour.
#These two numbers are unique if all the cells are convex.
#TODO: triangle ids are not unique due to zero-area cells and boundary cells.
def generate_triangle_ids(triangle_dbonds, triangles):
    """ Depricated """
    
    #Get all dbonds that live on the cell boundaries and are not part of the tissue boundary.
    #The triangle_id of the conjugate dbond of these dbond is the neighbour triangle id of each triangle.
    triangle_inner_dbonds   = triangle_dbonds[triangle_dbonds['triangle_id'] != MMDef.BOUNDARY_TRIANGLE_ID]
    triangles_neighbour_tid = triangle_dbonds.iloc[ triangle_inner_dbonds['conj_dbond_id'][0::3].values ]['triangle_id'].values

    #Add tissue exterior triangle (id = -1), and find the cell_id of all neighbouring triangles.
    triangles.loc[-1] = np.array([-1] * len(triangles.columns)).astype(dtype=int)
    triangles.loc[-1, 'cell_id'] = MMDef.BOUNDARY_CELL_ID

    triangles_neighbour_cell_id = triangles.loc[ triangles_neighbour_tid ]['cell_id'].values

    #Remove tissue exterior triangle.
    triangles.drop(index=-1, inplace=True)

    #Find cell_id of each triangle, and create unique triangle id from its cell_id and neighbour_cell_id.
    triangles_cell_id = triangles['cell_id'].values
    triangles_id      = np.array([  pairing_function([cell_id, neighbour_cell_id]) for cell_id, neighbour_cell_id in zip(triangles_cell_id, triangles_neighbour_cell_id)])

    print( 'Total triangles:', len(triangles_id), ', triangles with unique id:', len(set(triangles_id)) )
    
    #Add the triangle ids to the triangles table.
    triangles['triangle_id'] = triangles_id

    return triangles


#Generate unique triangle id string for each triangle,
#given a set of unique ids for each triangle.
def generate_triangle_id_strings(triangles_unique_ids, sort_ids = True):
    """ Depricated """

    N_triangles = len(triangles_unique_ids)

    #For every triangle, shift the given ids such that the smallest is in front.
    if sort_ids:
        triangles_shifted_ids = [np.roll(unique_ids, -np.argmin(unique_ids)) for unique_ids in triangles_unique_ids]
    else:
        triangles_shifted_ids = triangles_unique_ids

    #Convert every integer to a string.
    triangles_shifted_strings = [list(map(str, ids)) for ids in triangles_shifted_ids]
    #Merge the strings for every triangle to one: this is the unique id string for each triangle.
    triangles_id = ['_']*N_triangles
    for triangle_idx, strings in enumerate(triangles_shifted_strings):
        triangles_id[triangle_idx] = triangles_id[triangle_idx].join(strings)

    return triangles_id
    
    
def pairing_function(id_list):
    """ Cantor pairing function generates unique numbers from sets of numbers.
    Parameters
    ----------
    id_list : List (int)
        List containing integers.

    Return
    ------
    cantor_number: int
    """
    y = id_list.pop()
    x = id_list[-1]

    if (len(id_list) > 1):
        x = pairing_function(id_list)

    return int(0.5 * (x + y) * (x + y + 1) + y)


# Load frames file to find frame indices from movie.
def load_network_frames(path='./', frames_filename = 'frames.pkl'):
    """ Load network frame indices from file 'frames.pkl' located in the rootfolder of the movie.
    
        Parameters:
        -----------
        path : string
            Directory containing the file 'frames.pkl'
        frames_filename : string
            Filename of file containg table with frame indices.
            
        Returns:
        --------
        frames : Pandas.DataFrame
            DataFrame with column ['frame'] containg the frame indices in the movie.
    """
    
    filepath = os.path.join(path, frames_filename)
    
    if not os.path.isfile(filepath):
        print('Error: File', frames_filename, 'not found in', filepath)
        sys.exit()

    with open(filepath, "rb") as f:
        frames = pickle.load(f)

    return frames


#Return correctly formatted path (beginning and endin with '/')
def check_path_existence( input_path ):
    """ Check existence of gives path."""
    
    #Remove leading and trailing white spaces.
    input_path = os.path.normpath( input_path )
    
    #Check if path exists.
    input_path_existence = os.path.exists(input_path)
    
    if not input_path_existence:
        print('The given path of the movie data is invalid. Please check again. Path given:', input_path)
        sys.exit()
        
    #Make sure path ends with a '/'.
    if not input_path[-1] == '/':
        input_path = input_path + '/'
        
    return input_path


def get_directories(path, contains = '', sort_by_property = '', list_directories = False):
    """
    Returns a sorted list of all the subdirectories in path.
    """
    if not os.path.isdir(path):
        print('WARNING: Directory not found.', path)
        return None
    
    # get all subdirectories
    path_subdirs = [name for name in os.listdir(path)
                    if os.path.isdir(os.path.join(path, name))]
    
    # Perform pattern matching to get specified subset.
    if contains != '': 
        path_subdirs = [dir_name for dir_name in path_subdirs if fnmatch.fnmatch(dir_name, contains)]
    
    # Sort arrays
    if sort_by_property != '':
        path_subdirs = TL.sort_model_names_by_property_name(path_subdirs, sort_by_property)
    else:
        path_subdirs.sort()
    
    if list_directories:
        for idx, sim in enumerate(path_subdirs):
            print("%2d"%idx, sim)
    
    return list(path_subdirs)


def merge_tables(path_outputdir, frames, table_name = 'cells', network_type='cells'):
    """ Merge output tables over different frames
    
        Parameters:
        -----------
        path_outputdir : string
            Path to curver TM output directory.
        frames : list
            Frame indices to load.
        table_name : ['cells', 'vertices', 'dbonds', 'sorted_cell_dbonds_per_frame', 'shear_tensor', 'elongation_tensor', 'deformation_rate', 'curvature_tensor']
            Name of the table to load and merge.
        network_type : ['cells', 'dual_lattice_triangles', 'subcellular_triangles', 'cell_tensor', 'triangle_tensor', 'tissue_average']
            Network type the required table is defined on.
            
        Returns:
        --------
        df : Pandas.DataFrame
            Table with tables concatenated over frames. Contains extra column 'frame'.
    """
        
    table_frames = []
        
    for frameIdx in frames:
            
        #print('\rLoading frame', frameIdx, 'of table', table_name, end='')
            
        table_fr = table_io_per_frame(path_outputdir, table_name, frameIdx, network_type)
        assert type(table_fr) == pd.DataFrame
                        
        if 'frame' not in table_fr.columns:
            table_fr['frame'] = frameIdx
            
        if table_name == 'cells':
            table_fr.reset_index(inplace=True)
            table_fr.rename(columns={'index':'cell_id'}, inplace=True)
            
        table_frames.append(table_fr)
        
    # Concatonate tables and return.
    return pd.concat(table_frames, ignore_index=True)


def load_roi(filepath_roi, roi_set_name = '', cells = []):
    """ Load roi file and add to cells table.
    
        Parameters:
        -----------
        filepath_roi : string
            Path to RData file which contains the roi.
        roi_set_name : string, default: ''
            Name of the roi set to load. If set to empty string, return list of available roi sets.
        cells : Pandas.DataFrame
            Cells table. For every cell_id in the roi table, add its roi to the cells table. 
            Else add 'unspecified' in roi column.
            Cells in the roi table that are not in the cells table are ignored.
            If no cells table is given, return the loaded roi table.            
                
        Returns:
        --------
        cells : Pandas.DataFrame
            Cells table with added column 'roi' which gives the cells roi.
    """
    ### Import package 'pyreadr' to read RData files
    try:
        import pyreadr 
    except:
        print('Error: Can not load package \'pyreadr\' to load RData files.  Install package first.')
        return None
    
    ### Load RData ROI file.
    roi_dict = []
    if os.path.isfile(filepath_roi):
        roi_dict = pyreadr.read_r(filepath_roi)
    else:
        print('Error: File', filepath_roi, 'does not exists.')
        return None
        
    ### Load ROI set from file. If no name is specified, print available sets and return.
    df_roi = []
    if roi_set_name == '':
        return roi_dict.keys()
    else:
        if roi_set_name in roi_dict.keys():
            df_roi = roi_dict[roi_set_name]
        else:
            print('Error: Requested roi set', roi_set_name, 'not found in file.')
    
    df_roi['cell_id'] = df_roi['cell_id'].astype(np.int)
    
    ### If cells table is given, add roi to cell table.
    if type(cells) == pd.DataFrame:
       
        assert 'roi' in df_roi.columns and 'cell_id' in df_roi.columns
        
        cells_roi = cells.copy()
        
        # Make sure cells table is not indexed by cell_id, else reset_index.
        if cells_roi.index.name == 'cell_id' or 'cell_id' not in cells_roi.columns:
            cells_roi = cells_roi.reset_index()
            if 'index' in cells_roi.columns:
                cells_roi.rename(columns={'index':'cell_id'}, inplace=True)
        
        df_roi.set_index("cell_id", inplace=True)
        cells_roi = cells_roi.join(df_roi, on=['cell_id'])
        cells_roi.loc[cells_roi['roi'].isnull(), 'roi'] = 'unspecified'
        
        return cells_roi
    else:
        return df_roi