#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thursday March 19 2020

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

### MOVIE DATA GLOBALLY DEFINED CONSTANTS

#Cell Id of area outside the tissue boundary.
BOUNDARY_CELL_ID           = 10000
BOUNDARY_TRIANGLE_ID       = -1
#Cells with more than this number of vertices will be removed from network.
#Assumming this is a hole or fld in the tissue.
CELL_MAXIMUM_VERTEX_NUMBER = 20

#Directory name of directory to place all the frames.
DATAPATH_ROOT_FRAMES = 'frames'

#Small number to prevent division by zero etc.
EPSILON = 1e-12

#Valid strings for saving and loading different network types.
valid_networks             = ['cells', 'dual_lattice_triangles', 'subcellular_triangles', 'patches']

valid_file_actions         = ['load', 'save']

valid_cell_table_names     = ['cells', 'vertices', 'dbonds', 'sorted_cell_dbonds_per_frame', 'shear_tensor',
                              'elongation_tensor', 'deformation_rate', 'curvature_tensor']

valid_triangle_table_names = ['triangles', 'vertices', 'dbonds', 'deformation_rate', 'shear_tensor', 'dbond_geometry',
                              'dbond_int_curvature_tensor', 'elongation_tensor', 'curvature_tensor']

### Define table columns
#For tissue_deformation_rate_tensor
tissue_deformation_rate_tensor_columns = ['time', 'dt', 'delta_area', 'shear_xx', 'shear_xy', 'delta_psi',
                                          'shear_corotational_xx', 'shear_corotational_xy', \
                                          'covariance_elongation_xx', 'covariance_elongation_xy',
                                          'covariance_corotation_xx', 'covariance_corotation_xy', \
                                          'shear_rot_corr_xx', 'shear_rot_corr_xy', 'shear_area_corr_xx',
                                          'shear_area_corr_xy']
#For triangles_deformation_rate_tensor
triangles_deformation_rate_tensor_columns = ['triangle_id', 'delta_area', 'shear_xx', 'shear_xy', 'delta_psi',
                                             'shear_corotational_xx', 'shear_corotational_xy',
                                             'delta_elongation_xx', 'delta_elongation_xy',
                                             'delta_elongation_twophi']

#For text coloring.
class txtcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'