#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 2018

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore
import matplotlib.pyplot as plt

# Import database libraries.
import pickle
import sqlite3
import pandas.io.sql as psql

# Import image libraries.
from skimage import io as tiff_io
#import cv2

from curvedtm import MMDef, MMTool, MMGeom, MMCurv


class TissueMinerDBLoader:
    """
    Loads and extends the cell segmentation data of the Tissue Miner SQLite output file.
    
    Extends the 2d vertex position from the Tissue Miner database to 3d by using a heightmap image.
    
    1. Class loads the following tables from the Tisue Miner SQLite file:
         - vertices : x,y positions of the cell vertices.
         - directed_bonds : doubly connected edge list (DCEL) defining cell network topology.
         - cells : the cell ids that are tracked throughout the movie.
    2. Class loads the heightmap images corresponding to the segmented cells data, located in 'datapath_root/heightmaps'.
       The heightmap gives the height h at every pixel coordinate (x,y); h(x,y).
       We use is to extend the (x,y) coordinated in the vertices table with a z-position.
       The pixel coordinates in the vertices table and from the heightmap (x,y,z) can be converted to microns
       using the micron/pixel scaling defined in the file 'datapath_root/vertex_scaling.txt'
    3. The vertices, dbonds and cells tables are split by frame.
    4. Remove cells that have 0 or 1 vertices or cells that have more than MMDef.CELL_MAXIMUM_VERTEX_NUMBER vertices. 
       The latter likely stem from holes in the tissue or error in data aquiring.
    5. Cells table is extended with centroid position, area and unit normal vector using 3d vertex positions.
    
    Parameters:
    -----------
    datapath_root : string
        Path to the Tissue Miner root directory of the movie to load.
    refresh_DB_tables : [True, False]
        Whether to (re)load the data from the Tissue Miner SQLite database. Has to be true for first load.
    frames_to_process : list like, default: [-1]
        List with indices of movie frames to load. For [-1] it loads all available frames.
        
    Methods:
    --------
    run() : 
        Create and updated cell network from sqlite table in sequence.
    run_frame(frameIdx) : 
        Update cell network for specific frame.
    run_parallel(processes_number = 4) : 
        Create and update all frames in parallel.
    """  
    
    def __init__(self, datapath_root, refresh_DB_tables = False, frames_to_process = [-1]):
       
        #print('Initializing Movie database for ' + movie_name + '...')        
              
        #Set data paths for Tissue Miner data structure.
        self.movie_name          = os.path.basename(os.path.normpath(datapath_root))
        self.datapath_root       = os.path.normpath(datapath_root)
        self.datapath_heightmap  = os.path.join(self.datapath_root, 'heightmaps/')
        self.datapath_frames     = os.path.join(self.datapath_root, MMDef.DATAPATH_ROOT_FRAMES + '/')
        self.refresh_DB_tables   = refresh_DB_tables
        
        #Test given paths.
        MMTool.check_path_existence(self.datapath_root)        
        
        #Set name and create directory for output hdf files.
        self.path_hdf5_DB = os.path.join(self.datapath_root, MMDef.DATAPATH_ROOT_FRAMES)
        if not os.path.isdir(self.path_hdf5_DB):
            os.mkdir(self.path_hdf5_DB)
        else:
            print('Warning: Output directory already exists. Executing this might overwrite existing tables!')
            print('Output directory:', self.path_hdf5_DB)
                                           
        #Set of loaded table_names.
        self.loaded   = set()
        self.DB_table = {}

        #Set frames to load from DB. Default is all available frames.
        self.frames_to_process = self.load_movie_frames_indices(frames_to_process)

        # Check whether all required files exist.
        if self.refresh_DB_tables == False:
            
            # Check whether the TM sqlite DB file is already unpacked into pickle files in a previous run. 
            file_exist = []
            for filename in ['cells.pkl', 'directed_bonds.pkl', 'vertices.pkl', 'frames.pkl']:
                file_exist.append(os.path.isfile( os.path.join(self.datapath_root, filename) ))
            
            if not np.array(file_exist).all():
                print('Warning: One or more pickle files are missing. refresh_DB_tables set to True.')
                self.refresh_DB_tables = True
            else:
                # Check whether hdf5 DB files are present from previous run.
                if not MMTool.check_hdf5_db_files(self.frames_to_process, self.path_hdf5_DB).all():
                    print('Warning: At least one of the hdf5 DB files of the requested frames is missing. refresh_DB_tables set to True.')
                    self.refresh_DB_tables = True
                        
        #Get heightmap filenames and place in frame_idx -> filename dictionary.
        #Determine the use of heightmaps if the directory exists. Can be set to False after initialization
        #to prevent loading z-positions from heightmaps.
        self.use_heightmaps = os.path.exists(self.datapath_heightmap)

        self.heightmap_filenames = {}
        if self.use_heightmaps:
            self.heightmap_filenames = self.load_movie_heightmap_filenames()
        else:
            print('No heightmap directory found. self.use_heightmaps set to False.')

        print('Tissue Miner DB Loader for movie', self.movie_name, 'inititalized.')

        
    def run(self):
        """ Create and update cell network from sqlite table, 
            processing all requesed frames in sequence. """
        
        self.update_DB_tables_and_split_by_frame()

        for frameIdx in self.frames_to_process:
            self.run_frame_nocheck(frameIdx)

        print('Done.')
              
            
    def run_frame(self, frameIdx):
        """ Update cell network for specific frame.
        Run 'update_DB_tables_and_split_by_frame()' to create all frames first!
        
        Parameters:
        -----------
        frameIdx : int
            Index of frame to process.
        """
        self.update_DB_tables_and_split_by_frame()
        self.run_frame_nocheck(frameIdx)        
            
            
    def run_frame_nocheck(self, frameIdx):
        """ Update cell network for specific frame, used in run_parallel function.
            Function does not check wheter 'update_DB_tables_and_split_by_frame()' was already executed.
                
        Parameters:
        -----------
        frameIdx : int
            Index of frame to process.
        """
        try:
            self.update_cell_network_and_cell_properties_per_frame(frameIdx)
            print('Done with frame', frameIdx)
        except Exception as e: 
            print('ERROR: Something went wrong in frame', frameIdx, 'while creating the cell network.')
            print(e)        


    def run_parallel(self, processes_number = 4):
        """ Create and update all frames in parallel.
        
        Parameters:
        -----------
        processes_number : int, default: 4
            Numer of processes to run in parallel. Should not be larger than available cores.
        """
        
        self.update_DB_tables_and_split_by_frame()
        
        #Processes all frames in paralell.
        pool = mp.Pool(processes = processes_number)
        pool.map(self.run_frame_nocheck, self.frames_to_process)
        pool.close()

        print('Done running all frames.')


    def update_DB_tables_and_split_by_frame(self):
        """ Update Tissue Miner tables and split data by frame.
            #1. Reset indices for vertices and dbonds.
            #2. Add z-coordinate to vertices, scale from pixel -> microns and change coordinate system to right-handed.
            #3. Remove outdateted columns from cells table.
            #4. Split table into different files per frame in ./frames/.        
        """

        # If no DB refresh is required, and all files exist, skip this function.
        if not self.refresh_DB_tables:
            if MMTool.check_hdf5_db_files(self.frames_to_process, self.path_hdf5_DB).all():
                return True
        
        # Load tables from SQLite database.
        self.load_DB_table('cells',          self.refresh_DB_tables)
        self.load_DB_table('directed_bonds', self.refresh_DB_tables)
        self.load_DB_table('vertices',       self.refresh_DB_tables)

        #Group tables by frame index for future use.
        self.grouped_cells    = self.DB_table['cells'].groupby('frame')
        self.grouped_dbonds   = self.DB_table['directed_bonds'].groupby('frame')
        self.grouped_vertices = self.DB_table['vertices'].groupby('frame')
        
        #Append z-positions to vertices table; using data from heightmaps. Update pickle file.
        self.append_z_to_vertices_table()

        #Map all y vertex coordinates: y -> figsize_y - y
        if self.use_heightmaps:
            self.flip_coordinate_system_lefthanded_to_righthanded()

        #Scale vertex positions by pixel-to-micrometer.
        self.apply_vertex_scaling()

        #Reset dbond and vertex ids in tables to indexes.
        #Save new tables.
        print('Resetting directed bond and vertex tables indices...')
        self.reset_dbond_and_vertex_ID_to_Index()

        file_pickle = os.path.join(self.datapath_root, 'vertices.pkl')
        print('Writing updated vertices data to file ', file_pickle)
        self.DB_table['vertices'].to_pickle(file_pickle)

        file_pickle = os.path.join(self.datapath_root, 'directed_bonds.pkl')
        print('Writing updated DCEL data to file ', file_pickle)
        self.DB_table['directed_bonds'].to_pickle(file_pickle)

        #Save split tables with reset indices.
        self.save_DB_table_per_frame('cells')
        self.save_DB_table_per_frame('dbonds')
        self.save_DB_table_per_frame('vertices')

        #Remove old tables from memory, close connection to sql database.
        print('Clear old data tables from memory...')
        del [self.DB_table, self.grouped_cells, self.grouped_vertices, self.grouped_dbonds]
        self.DB_table = {}
        self.loaded = set()

        print('Done updating DB tables.')
        
        return True


    def update_cell_network_and_cell_properties_per_frame(self, frameIdx):
        """ Update cell network further and calculate new geometric properties area, cell center and normal
            #1. Remove all cells with zero area: less than 3 vertices.
            #2. Sort dbonds in counter-clockwise direction per cell.
            #3. Update cells table with area, normal vector and centroid.
        """
        
        #Remove all cells which consist of 0, 1 or more than MMDef.CELL_MAXIMUM_VERTEX_NUMBER vertices.
        self.remove_zero_area_cells_and_update_tables(frameIdx)

        #Append counter clockwise ordered bond ids to cell table and save to file.
        self.sort_dbonds_cc_per_cell(frameIdx)

        #Calculate the center of mass, area and unit normal vector for all the cells.
        self.update_cell_properties(frameIdx)


    def remove_zero_area_cells_and_update_tables(self, frameIdx):
        """ Remove cells from tissue with only 1 vertex or more than MMDef.CELL_MAXIMUM_VERTEX_NUMBER vertices.
            Such a large cell is probably a hole in the tissue.        
        """

        cells  = self.table_io_per_frame('cells', frameIdx)
        dbonds = self.table_io_per_frame('dbonds', frameIdx)

        changed_tables = False

        # Cells between 2 to MMDef.CELL_MAXIMUM_VERTEX_NUMBER vertices are not removed.
        # Cells with more than MMDef.CELL_MAXIMUM_VERTEX_NUMBER vertices are assumed holes in the tissue.
        for cell_id in cells.index:

            # Find all dbonds belonging to cell with id cell_id.
            selected_dbonds = dbonds['cell_id'] == cell_id
            cell_dbond_number = sum(selected_dbonds)

            if cell_dbond_number < 2 or cell_dbond_number > MMDef.CELL_MAXIMUM_VERTEX_NUMBER:
                cells.drop(index=cell_id, inplace=True)
                dbonds.loc[selected_dbonds, 'cell_id'] = MMDef.BOUNDARY_CELL_ID
                print('rem. cid:', cell_id, ', (Number of vertices:', cell_dbond_number,')')
                changed_tables = True

        if changed_tables:
            self.table_io_per_frame('cells',  frameIdx, 'save', cells)
            self.table_io_per_frame('dbonds', frameIdx, 'save', dbonds)

            
    def update_cell_properties(self, frameIdx):
        """ Update cell table with cell center, cell normal and cell area.        
        
        Extra columns: ['area', 'center_x', 'center_y', 'center_z', normal_x', 'normal_y', 'normal_z']
        """

        print('Calculating cell area, unit normal and neighbour number for frame ' + str(frameIdx) + '...')

        #Load cell network for this frame.
        self.cells                     = self.table_io_per_frame('cells', frameIdx)
        self.dbonds                    = self.table_io_per_frame('dbonds', frameIdx)
        self.vertices                  = self.table_io_per_frame('vertices', frameIdx)
        self.sorted_dbonds_per_cell_id = self.table_io_per_frame('sorted_cell_dbonds_per_frame', frameIdx)

        #Calculate cells centers
        cell_centers = MMGeom.calculate_cell_centers(self.dbonds, self.vertices, self.sorted_dbonds_per_cell_id)

        #Update cells table with centers.
        self.cells[['center_x', 'center_y', 'center_z']] = cell_centers[['x_pos', 'y_pos', 'z_pos']]

        #Calculate the area and unit normal vector for each cell.
        cells_area, cells_normal_vector = MMGeom.calculate_cell_area_and_unit_normal_vector(self.vertices, self.cells, self.dbonds, self.sorted_dbonds_per_cell_id)

        #Store data in the tables.
        self.cells['area']     = cells_area
        self.cells['normal_x'] = cells_normal_vector.T[0]
        self.cells['normal_y'] = cells_normal_vector.T[1]
        self.cells['normal_z'] = cells_normal_vector.T[2]

        #Store cell neighbour number in cells tabel.
        if not (self.cells.columns == 'neighbour_number').any():
            cells_neighbour_number = MMGeom.calculate_cell_neighbour_number(self.sorted_dbonds_per_cell_id)
            self.cells = pd.concat([self.cells, cells_neighbour_number], axis=1)

        #Save updated cells table.
        self.table_io_per_frame('cells', frameIdx, 'save', self.cells)


    def reset_dbond_and_vertex_ID_to_Index(self):
        """ Reset all dbond and vertex ids to range(0, len(dbonds)/len(vertices)) for each frame.
        
        This means vertex and dbond ids are not unique between frames.
        """
        
        def apply_inplace(df, field, fun):
            return df[field].apply(func=fun)
        
        for frameIdx in self.frames_to_process:
        
            dbonds_frame   = self.grouped_dbonds.get_group(frameIdx)
            vertices_frame = self.grouped_vertices.get_group(frameIdx)

            #Make mapping from dbond id and vertex id to index ranging 0:N-1
            dbonds_Id_To_Idx   = dict(zip(dbonds_frame['dbond_id'], range(len(dbonds_frame))))
            vertices_Id_To_Idx = dict(zip(vertices_frame['vertex_id'], range(len(vertices_frame))))

            #Reset all dbond ids to 0:N-1.
            selected_dbonds = self.DB_table['directed_bonds']['frame'] == frameIdx
            for field in ['conj_dbond_id', 'left_dbond_id', 'dbond_id']:

                self.DB_table['directed_bonds'].loc[selected_dbonds, field] = apply_inplace( dbonds_frame, field, fun = lambda dbond_id: dbonds_Id_To_Idx[dbond_id] )

            self.DB_table['directed_bonds'].loc[selected_dbonds, 'vertex_id'] =\
                apply_inplace( dbonds_frame, 'vertex_id', fun = lambda vertex_id: vertices_Id_To_Idx[vertex_id] )

            #Reset vertex ids to 0:N-1.
            selected_vertices = self.DB_table['vertices']['frame'] == frameIdx
            self.DB_table['vertices'].loc[selected_vertices, 'vertex_id'] =\
                apply_inplace( vertices_frame, 'vertex_id', fun = lambda vertex_id: vertices_Id_To_Idx[vertex_id] )          
        
        
    def sort_dbonds_cc_per_cell(self, frameIdx):
        """ Create a dict mapping each cell_id to a list of directed bond ids, 
        sorted in counterclockwise direction, starting with the lowest dbond_id.
        
        Save the dictionary to the table 'sorted_cell_dbonds_per_frame'.
        """

        #Given the table of the left bond for each bond, find all the bonds related to a cell.
        def sortCellDbondIds_cc(unsorded_dbonds, sorted_dbonds):

            dbonds_id, ldbonds_id = unsorded_dbonds
            Ndbonds = len(dbonds_id)        

            sorted_dbonds[0] = dbonds_id[0]
            sorted_dbonds[1] = ldbonds_id[0]

            for idx in range(1, Ndbonds-1):
                next_dbond_idx = np.where(dbonds_id == sorted_dbonds[idx])[0]
                sorted_dbonds[idx+1] = ldbonds_id[next_dbond_idx]

        cells  = self.table_io_per_frame('cells', frameIdx)
        dbonds = self.table_io_per_frame('dbonds', frameIdx)

        print('Calculating cell bond topology for frame', frameIdx)

        # Group dbonds by there cell ids and create lists.
        gg = dbonds.groupby('cell_id')['left_dbond_id']
        cells_dbonds_id_to_next_dbond_id = [[gg.get_group(cid).index.values, gg.get_group(cid).values] for cid in cells.index]
        sorted_dbonds_list = [np.array([-1]*len(gg.get_group(cid))) for cid in cells.index]

        # Sort dbond ids in counter clockwise direction in each cell.
        for unsorted_db, sorted_db in zip(cells_dbonds_id_to_next_dbond_id, sorted_dbonds_list):
            sortCellDbondIds_cc(unsorted_db, sorted_db) 

        # Create dictionary cell_id -> cc dbonds id and save.
        self.sorted_dbonds_per_cell = dict(zip(cells.index, sorted_dbonds_list)) 
        self.table_io_per_frame('sorted_cell_dbonds_per_frame', frameIdx, 'save', self.sorted_dbonds_per_cell)
        
        
    def append_z_to_vertices_table(self):
        """ Append z-positions to vertices table; using data from heightmaps. 
        
        Given the heightmap h(x,y), extend vertex table (x,y) -> (x,y,z-h(x,y))
        
        If no heightmaps were found, set z=0.
        """
        print('Adding z-coordinates to vertices table...')
        
        #Add z position column to vertices dataframe.
        self.DB_table['vertices']['z_pos'] = 0

        #If heightmaps are not used, skip function.
        if not self.use_heightmaps:
            return None
        
        for frameIdx in self.frames_to_process:
            
            print('\rLoading heightmap for frame', frameIdx, end='')
                        
            filename_heightmap = self.heightmap_filenames[frameIdx]

            if os.path.isfile(self.datapath_heightmap + filename_heightmap):
                #Load hightmap from file.
                heightmap = tiff_io.imread(self.datapath_heightmap + filename_heightmap)

                #Map vertex x,y-positions to z position from heightmap.
                #Since the z position in the heightmap in inverted, we apply -1.
                selected_vertices = self.DB_table['vertices']['frame'] == frameIdx

                #vertices_xy = self.grouped_vertices.get_group(frameIdx)[['x_pos','y_pos']].values
                vertices_xy = self.DB_table['vertices'].loc[selected_vertices, ['x_pos', 'y_pos']].values
                vertices_z  = -1 * np.array([heightmap[int(y), int(x)] for x,y in vertices_xy])

                #Save z positions in vertices DataFrame.
                self.DB_table['vertices'].loc[selected_vertices, 'z_pos'] = vertices_z
            else:
                print('Can not find heightmap file ', filename_heightmap, ' for frame ', frameIdx, '.')
        
        print('\nDone loading heightmaps.')
    
    def flip_coordinate_system_lefthanded_to_righthanded(self):
        """ Transform vertex coordinates to right-handed coordinate system.
        
        Flip coordinate system from right-handed to left-handed by mapping all y-coordinates in the vertices table:
        y -> image_size_y_dim - y
        """
                
        #Find image size to flip
        try:
            first_frameIdx     = self.frames_to_process[0]
            filepath_heightmap = os.path.join(self.datapath_heightmap, self.heightmap_filenames[first_frameIdx])
            heightmap_shape    = np.shape( tiff_io.imread(filepath_heightmap) )
        except:
            print('Error: Could not determine shape of heightmap image.')
            return None        
        
        heightmap_y_max = heightmap_shape[0] - 1
               
        for frameIdx in self.frames_to_process:        
            selected_vertices = self.DB_table['vertices']['frame'] == frameIdx
            vertices_y_pos = self.DB_table['vertices'].loc[selected_vertices, 'y_pos'].values
            self.DB_table['vertices'].loc[selected_vertices, 'y_pos'] = heightmap_y_max - vertices_y_pos

            
    def load_vertex_scaling(self):
        """ Load vertex scaling pixel/micrometer from file.
        
        Scaling should be in file 'datapath_root/vertex_scaling.txt'
             
        Returns: 
        --------
        vertex_scaling : Pandas.DataFrame
            Pixels to microns scaling with columns ['x_scaling', 'y_scaling', 'z_scaling']
        """
        
        vertex_scaling_filename = 'vertex_scaling.txt'

        scaling_filepath = os.path.join(self.datapath_root, vertex_scaling_filename)
        
        if not os.path.isfile(scaling_filepath):
            print('File', vertex_scaling_filename, 'not found in path', scaling_filepath,
                  '. Continue without scaling vertex positions.')
            vertex_scaling = pd.DataFrame({'x_scaling': [1.0], 'y_scaling': [1.0], 'z_scaling': [1.0]})
        else:
            print('Found vertex scaling file.')
            vertex_scaling = pd.read_csv(scaling_filepath, sep=' ', header=0)

        return vertex_scaling            
            

    def apply_vertex_scaling(self):
        """ Apply vertex scaling pixel-to-micrometer to each vertex position. """

        #Scale vertex positions by list scaling = (scale_x, scale_y, scale_z)
        scaling_vertices = self.load_vertex_scaling()
        
        self.DB_table['vertices'].loc[:, 'x_pos'] *= scaling_vertices.iloc[0]['x_scaling']
        self.DB_table['vertices'].loc[:, 'y_pos'] *= scaling_vertices.iloc[0]['y_scaling']
        self.DB_table['vertices'].loc[:, 'z_pos'] *= scaling_vertices.iloc[0]['z_scaling'] 
            
                
    def plot_heightmap_and_vertices(self, frameIdx, filename_heightmap = '', no_vertices = False):
        """ Plot heightmap and vertex positions. """

        if int(frameIdx) in range(len(self.heightmap_filenames)):

            #Load heightmap from file.
            heightmap = tiff_io.imread(self.datapath_heightmap +  self.heightmap_filenames[frameIdx])
        
            fig, ax = plt.subplots( nrows=1, ncols=1, figsize = (15,15))
            ax.set_aspect('equal')

            #img = plt.imshow(np.flipud(heightmap))
            #img = plt.imshow(np.fliplr(heightmap))
            img = plt.imshow(heightmap)

            if not 'vertices' in self.DB_table.keys():
                self.load_DB_table('vertices')

            if not no_vertices:
                #self.flip_coordinate_system_lefthanded_to_righthanded(refresh=True)
                selected_vertices = self.DB_table['vertices']['frame'] == frameIdx
                vertices_xy = self.DB_table['vertices'].loc[selected_vertices, ['x_pos','y_pos']].values
                plt.scatter(vertices_xy.T[0], vertices_xy.T[1], s = 2, c = 'red')

            plt.show()


    def apply_gaussian_blur_to_heightmaps(self, blur_std):
        """ Apply gaussian blur to heightmaps.
        
        In case heightmaps are too rough, apply gaussian filter on images.
        
        Parameters:
        -----------
        blur_std : float
            Sets standard deviation of Gaussian kernel in pixels.
        """

        #Set filter size to 3*std; make sure it is an odd number.
        blur_std = int(blur_std)
        filter_size = 3 * blur_std
        if filter_size%2 == 0:
            filter_size += 1

        # Construct output directory for blurred heightmaps.
        dir_name_save = 'blurred_' + str(blur_std) + '/'
        if not os.path.isdir(self.datapath_heightmap + dir_name_save):
            os.mkdir(self.datapath_heightmap + dir_name_save)

        for frameIdx, filename in self.heightmap_filenames.items():

            img = tiff_io.imread(self.datapath_heightmap + filename)
            img = img.astype(np.float32)

            #blur = cv2.GaussianBlur(img, (filter_size, filter_size), blur_std)

            tiff_io.imsave(self.datapath_heightmap + dir_name_save + filename, blur)
            
    
    #Load single table names table_name from SQLite table. Write to pickle file if not done yet.
    def load_DB_table(self, table_name, refresh = False):
        """ Loads a table from the Tissue Miner SQL database.
        
        Will create a pickle file on disk for each requested table for faster loading in the future.
        If pickle format of the table exists and is not odler than the sqlite database file,
        it will load the table from pickle file if refresh=False.
        Also checks whether the table is already loaded.
                
        Parameters:
        -----------
        table_name : string, ['vertices', 'bonds', 'frames', 'directed_bonds', 'cells']
            Name of the table to load. 
        refresh : [False, True]
            Wheter to always reload from SQLite DB or use the pickle file created earlier instead (faster)
            
        Functions adds requested table to the dict self.DB_table[table_name]
        """
        
        table_names = ['vertices', 'bonds', 'frames', 'directed_bonds', 'cells']
        
        if not table_name in self.loaded:
            file_pickle = os.path.join(self.datapath_root, table_name + '.pkl')
            file_sql    = os.path.join(self.datapath_root, self.movie_name + '.sqlite')
            refresh_pickle_file = (not os.path.isfile(file_pickle)) or (os.path.getmtime(file_sql) > os.path.getmtime(file_pickle)) or (refresh)
            if refresh_pickle_file:
                try:
                    if os.path.isfile(file_sql):
                        # Connect to SQLite database created by Tissue Miner.
                        con = sqlite3.connect(file_sql)
                    else:
                        print("Error: SQLite database does not exist.", file_sql)
                        sys.exit()
                except:
                    print('Unable to open SQLite file in ', file_sql)
                    sys.exit()

                print('Loading table ' + table_name + ' from database ' + self.movie_name + '...')
                if table_name in table_names:
                    self.DB_table[table_name] = psql.read_sql('SELECT * FROM ' + table_name + ';', con)
                else:
                    print('Table named ', table_name, ' not found in ', self.movie_name + '.sqlite. Skipping.')

                # Close sqlite db connection.
                con.close()

                #Drop unused columns in cells.
                if table_name == 'cells':
                    self.DB_table['cells'] = self.DB_table['cells'][['frame', 'cell_id']]
                    #Remove boundary cell from table.
                    boundary_cell_idx = self.DB_table['cells'][self.DB_table['cells']['cell_id'] == MMDef.BOUNDARY_CELL_ID].index
                    self.DB_table['cells'].drop(index=boundary_cell_idx, inplace=True)
                
                #Only overwrite tables when self.refresh_DB_tables = True.
                if refresh_pickle_file:
                    print('Writing table ' + table_name + ' to pickle file ' + self.movie_name)
                    self.DB_table[table_name].to_pickle(file_pickle)
            else:
                print('Loading table ' + table_name + ' from pickle file...') 
                self.DB_table[table_name]= pd.read_pickle(file_pickle)
                           
            self.loaded.add(table_name)


    def save_DB_table_per_frame(self, table_name):
        """ Split Tissue Miner database in a seperate file for each frame.
        
        Files daved in diectory 'datapath_root/frames/'
        
        Parameters:
        -----------
        table_name : string, ['cells', 'vertices', 'dbonds', 'sorted_cell_dbonds_per_frame']
            Name of table to save.
        """

        if not table_name in MMDef.valid_cell_table_names:
            print('ERROR: Unknown table name for data output ', table_name)
            sys.exit()

        print('Split data from DB table', table_name, 'for frames', self.frames_to_process.min(), 'to', self.frames_to_process.max(), '.')

        for frameIdx in self.frames_to_process:
                                               
            if table_name == 'cells':
                cells_frame = self.grouped_cells.get_group(frameIdx)
                cells_frame = cells_frame.reset_index(drop=True)
                cells_frame = cells_frame.set_index('cell_id')
                self.table_io_per_frame('cells', frameIdx, 'save', cells_frame)
                                               
            if table_name == 'dbonds':
                dbonds_frame = self.grouped_dbonds.get_group(frameIdx)
                dbonds_frame.set_index(keys='dbond_id', inplace=True, drop=True)
                self.table_io_per_frame('dbonds', frameIdx, 'save', dbonds_frame)

            if table_name == 'vertices':
                vertices_frame = self.grouped_vertices.get_group(frameIdx)
                vertices_frame.set_index(keys='vertex_id', inplace=True, drop=True)
                self.table_io_per_frame('vertices', frameIdx, 'save', vertices_frame)
                                               
            if table_name == 'sorted_cell_dbonds_per_frame':
                self.table_io_per_frame('sorted_cell_dbonds_per_frame', frameIdx, 'save', self.sorted_cell_dbonds_per_frame[frameIdx])
                   
                    
    def database_tables(self):
        """ Returns a list of tables of the SQL database as DataFrame. """
        return psql.read_sql("SELECT name FROM sqlite_master WHERE type='table';", self.con)


    def load_movie_frames_indices(self, frames_to_process):
        """ Load all the movie frame indices and times from the sqplite table. """

        if not(type(frames_to_process) is list or type(frames_to_process) is np.array):
            frames_to_process = np.array([frames_to_process])
        else:
            frames_to_process = np.array(frames_to_process)
                    
        #Try to load list of movie frame indices from file.
        filepath_frames_pickle = os.path.join(self.datapath_root, 'frames.pkl')
        if os.path.isfile(filepath_frames_pickle) and not self.refresh_DB_tables:
            with open(filepath_frames_pickle, "rb") as f:
                frames_in_movie = pickle.load(f)['frame'].values
        else:
            #First check if tables file already exitsts in movie root as pickle file.
            self.load_DB_table('frames', self.refresh_DB_tables)
            # Set frames to process.
            frames_in_movie = self.DB_table['frames']['frame'].values

        #Check if requested frames are in the movie, or load all frame indices if no specific frames are given.
        if frames_to_process[0] != -1:
            if not np.array([frame in frames_in_movie for frame in frames_to_process]).all():
                print('Error: Some frame numbers in custom input array,', frames_to_process, ', could not be found in the Tissue Miner SQLite file.')
                sys.exit()
            else:
                return frames_to_process
        else:
            return frames_in_movie


    def load_movie_heightmap_filenames(self):
        """ Create dictionary mapping frame index to heightmap filename.
        
        Assume each frame index in self.frames_to_process corresponds the index in the ordered list self.heightmap_filenames.
        
        Returns:
        -------
        heightmap_filenames_dict : dict
            Map frame index (int) to heightmap filename (string).        
        """

        # First, find all heightmap files in heightmap directory, and sort by filename.
        filename_identifier_end = '.tif'
        filenames = [f for f in os.listdir(self.datapath_heightmap) if os.path.isfile(self.datapath_heightmap + f)]
        self.heightmap_filenames = [f for f in filenames if f.endswith(filename_identifier_end)]
        self.heightmap_filenames.sort()

        if not len(self.heightmap_filenames) == len(self.frames_to_process):
            heightmap_filenames_dict = dict(zip(self.frames_to_process, [''] * len(self.frames_to_process)))

            N_heightmaps = len(self.heightmap_filenames)
            for frameIdx in self.frames_to_process:
                if frameIdx in range(N_heightmaps):
                    heightmap_filenames_dict[frameIdx] = self.heightmap_filenames[frameIdx]
        else:
            heightmap_filenames_dict = dict(zip(self.frames_to_process, self.heightmap_filenames))

        return heightmap_filenames_dict
    
    
    def table_io_per_frame(self, table_name, frameIdx, action='load', table=[]):
        """ Wrapper for Curved Tissue Miner file io """
        return MMTool.table_io_per_frame(self.path_hdf5_DB, table_name, frameIdx, 'cells', action, table = table)