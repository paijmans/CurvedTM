#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wednesday Feb 26 2020

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool, MMGeom, MMdg

def calculate_patches_principle_curvatures(patches_curvatures, ordered = 'value'):
    """ Function returns principle curvatures kappa1 and kappa2 given the mean and gaussian curvature 
        defined on a set of patches (triangles, cells, etc.).
        
        Definition: kappa1 = H + sqrt(H**2 - K), kappa2 = H - sqrt(H**2 - K), 
        where H and K are the mean and gaussian curvature on each patch.
        
        If H^2 < K, the principle curvatures will have an imaginary component. 
        In that case, we set the imaginary part to 0.
        
        Parameters
        ----------
        patches_curvatures : Pandas.DataFrame, size M
            Table indexed with patch id containing the mean and gaussian curvature on each patch.
        ordered : ['value', 'magnitude']
            - value : return such that kappa1 > kappa2
            - magnitude : return such that abs(kappa1) > abs(kappa2)
        
        Returns:
        --------
        patches_principle_curvatures : Pandas.DataFrame, size M
            Dataframe indexed with patch_id with columns ['kappa1', 'kappa2']
    """
    
    assert 'mean_curvature' in patches_curvatures.columns and 'gaussian_curvature' in patches_curvatures.columns

    # First calculate principe curvatures from gaussian and mean curvatures (make sure principle curvatures are not imaginary).
    patches_imaginary_kappa = patches_curvatures['mean_curvature'] ** 2 < patches_curvatures['gaussian_curvature']
    patches_sqrt_term = patches_curvatures['mean_curvature'] ** 2 - patches_curvatures['gaussian_curvature']
    patches_sqrt_term[patches_imaginary_kappa] = 0.0
    print('Fraction of Cij with imaginary Eigenvalues:', sum(patches_imaginary_kappa) / len(patches_curvatures))

    patches_kappa1 = (patches_curvatures['mean_curvature'] + np.sqrt(patches_sqrt_term)).values
    patches_kappa2 = (patches_curvatures['mean_curvature'] - np.sqrt(patches_sqrt_term)).values

    # Return principle curvatures sorted by magnitude: |kappa_1| > |kappa_2|
    if ordered == 'magnitude':
        patches_mag_k1_smaller_than_mag_k2 = abs(patches_kappa1) < abs(patches_kappa2)
        kappa1_values_to_swap = patches_kappa1[patches_mag_k1_smaller_than_mag_k2]
        patches_kappa1[patches_mag_k1_smaller_than_mag_k2] = patches_kappa2[patches_mag_k1_smaller_than_mag_k2]
        patches_kappa2[patches_mag_k1_smaller_than_mag_k2] = kappa1_values_to_swap

    patches_principle_curvatures = pd.DataFrame({'kappa1':patches_kappa1, 'kappa2':patches_kappa2}, index=patches_curvatures.index)

    return patches_principle_curvatures


def calculate_integrated_curvature_tensor_on_dbonds(triangles, triangle_dbonds, triangle_vertices, dbonds_geometry):
    """ Calculate the integrated curvature tensor on each internal dbond in the triangular network.
        
        dbonds that lie on the tissue boundary are set to zero.
        
        Parameters
        ----------
        triangles : Pandas.DataFrame, size M
            Table containing triangle state properties: ['mean_curvature', 'gaussian_curvature', 'area']
        triangle_dbonds : Pandas.DataFrame, size N
            Topology of dbond network in DCEL form.
        triangle_vertices : Pandas.DataFrame size O
            Vertex positions in the triangular network: ['x_pos', 'y_pos', 'z_pos']
        dbonds_geometry : Pandas.DataFrame, size N
            Table containing geometry of dbonds : ['signed_angle', 'length']
        
        Returns:
        --------
        dbonds_int_curvature_tensor : Numpy.ndarray, shape: (N,3,3), indexing as triangle_dbonds table.
            Array with index dbond_id and integrated curvature tensor in matrix form in Eucledean coordinates.
    """
    
    print('Calculating integrated curvature tensor on each dbond...')

    ### Select all internal bonds (not their conjugates) in the network. 
    # First select all dbonds that are not on the boundary.
    dbonds_conj_dbond_id   = triangle_dbonds['conj_dbond_id'].values
    dbonds_not_at_boundary = (triangle_dbonds['triangle_id'] > MMDef.BOUNDARY_TRIANGLE_ID).values &\
                             (triangle_dbonds.iloc[dbonds_conj_dbond_id]['triangle_id'] > MMDef.BOUNDARY_TRIANGLE_ID).values
    int_dbonds             = triangle_dbonds[dbonds_not_at_boundary]

    # Get index of internal dbonds with has a lower index than its conjugate dbond: these are called bonds.
    int_bonds_id      = int_dbonds[int_dbonds.index < int_dbonds['conj_dbond_id']].index.values
    int_bonds         = int_dbonds.loc[int_bonds_id]
    int_conj_bonds_id = int_bonds['conj_dbond_id'].values
    
    ### Create int curvature tensor in eigen basis (diagonal matrix)
    bonds_int_curvature_tensor_eigen_basis = np.array([0.5*length*np.diag([ np.sin(theta) + theta, -np.sin(theta) + theta, 0.])
                                                       for theta, length in dbonds_geometry[['signed_angle', 'length']].loc[int_bonds_id].values])

    ### Find the basis of the int curvature tensor of each bond.
    # Basis vector V1: Bond unit vector.
    bonds_right_vid = int_bonds['vertex_id'].values
    bonds_left_vid  = triangle_dbonds.iloc[ int_bonds['left_dbond_id'].values ]['vertex_id'].values
    triangle_vertices_positions = triangle_vertices[['x_pos', 'y_pos', 'z_pos']].values
    
    bonds_unit_dist_vector = MMTool.unit_vector(triangle_vertices_positions[bonds_left_vid] - triangle_vertices_positions[bonds_right_vid])

    # Basis vector V2: Mean of the adjacent triangle normals of each bond.
    bonds_triangle_id      = int_bonds['triangle_id']
    conj_bonds_triangle_id = triangle_dbonds['triangle_id'].loc[int_bonds['conj_dbond_id'].values]

    bonds_left_triangle_normal  = triangles.loc[bonds_triangle_id][['normal_x', 'normal_y', 'normal_z']].values
    bonds_right_triangle_normal = triangles.loc[conj_bonds_triangle_id][['normal_x', 'normal_y', 'normal_z']].values

    bonds_mean_unit_normal = MMTool.unit_vector(bonds_left_triangle_normal + bonds_right_triangle_normal)
    
    # Basis vector V3: Normal to V1 and V2:
    bonds_basis_v3_unit = MMTool.unit_vector(np.cross(bonds_mean_unit_normal, bonds_unit_dist_vector))

    
    ### Create basis of V1, V2, V3 and the int curvaturet tensor in lab frame.
    bonds_int_curv_tensor_basis = np.array([[v1, v2, v3] for v1, v2, v3 in zip(bonds_basis_v3_unit, bonds_mean_unit_normal, bonds_unit_dist_vector)])
        
    # Transform int curvature tensor from local (daigonal) basis to lab frame.
    bonds_int_curvature_tensor = MMdg.tensor_basis_transformation(bonds_int_curvature_tensor_eigen_basis, bonds_int_curv_tensor_basis)

    
    ### Create DataFrame indexed dbond_id -> 9 columns C11, C12, C13, ..., C32, C33
    # Fill bond tensors and their conjugate bonds.
    bonds_Cint_df      = pd.DataFrame(bonds_int_curvature_tensor.reshape(-1, 9), index = int_bonds_id)
    conj_bonds_Cint_df = pd.DataFrame(bonds_int_curvature_tensor.reshape(-1, 9), index = int_conj_bonds_id)
              
    # Assign zero tensor to boundary dbonds.
    boundary_dbonds_id      = np.setdiff1d(triangle_dbonds.index, triangle_dbonds[dbonds_not_at_boundary].index, assume_unique=True)
    boundary_dbonds_Cint_df = pd.DataFrame([np.zeros(9)]*len(boundary_dbonds_id), index = boundary_dbonds_id)

    dbonds_int_curvature_tensor_df = pd.concat([bonds_Cint_df, conj_bonds_Cint_df, boundary_dbonds_Cint_df])
    dbonds_int_curvature_tensor_df.sort_index(inplace=True)
        
    # Assert every dbond is assigned a int. curv tensor.
    assert sum(dbonds_int_curvature_tensor_df.index.values - triangle_dbonds.index.values) == 0
        
    # Return tensor as numpy.ndarray, matrix.
    return dbonds_int_curvature_tensor_df.values.reshape(-1,3,3)



def calculate_average_curvature_tensor_on_patches(patches_triangle_ids_dict, dbonds_int_curvature_tensor,
                                                  triangles, triangle_dbonds, patches_mean_gaussian_curvatures = []):
    """ Given a dictionary of patches, as triangle indices indexed with patch ids, calculate the area weighted means
        of the gaussian and mean curvature on these patches from the values on the triangles.
        
        Parameters
        ----------
        patches_triangle_ids_dict : dict (patch_id, [triangle indices]), size M
            Dictionary containg patches as lists of triangle indices.
        dbonds_int_curvature_tensor : Numpy.ndarray, shape: (N,3,3), indexed by dbonds
            Array with the integrated curvature tensor on every dbond.
        triangles : Pandas.DataFrame, size O
            Table containing triangle state properties: ['mean_curvature', 'gaussian_curvature', 'area']
        triangle_dbonds : Pandas.DataFrame, size N
            Topology of dbond network in DCEL form.
        patches_mean_gaussian_curvatures : Pandas.DataFrame, size M
            Mean and Gaussian curvature on every patch.
        
        Returns:
        --------
        patches : Numpy.ndarray, shape: M x 3 x 3
            Array indexed as patches_triangle_ids_dict containing the average curvature tensor on each patch.
    """
    
    # Set number of patches.
    Npatches = len(patches_triangle_ids_dict)

    print('Calculating integrated curvature tensor on', Npatches, 'patches...')

    # Calculate the area weighted mean and gaussian curvatures on each patch.
    if len(patches_mean_gaussian_curvatures) == 0:
        patches_mean_gaussian_curvatures =\
            calculate_mean_and_gaussian_curvature_on_patches_from_triangles(triangles, patches_triangle_ids_dict)

    # Find all bond ids of the different patches of triangles. Make sure each bond id appears only once.
    patches_bonds_id = [np.unique(np.concatenate([triangle_dbonds[triangle_dbonds['triangle_id'] == triangle_id]['bond_id'].values
                                         for triangle_id in patch_triangle_ids]))
                        for patch_id, patch_triangle_ids in patches_triangle_ids_dict.items()]
    
    # Add up all intrinsic curvature tensors for the different patches of bond ids.
    patches_total_int_curvature_tensor = np.array([sum(dbonds_int_curvature_tensor[patch_bonds_id]) for patch_bonds_id in patches_bonds_id])
    
    # Get sorted eigenvectors from largest to smallest absolute values from the total integrated curvature tensor.
    patches_int_curvature_basis_eigenval, patches_int_curvature_tensor_principle_basis_vectors = \
        MMTool.calculate_tensor_eigenvalues_and_eigenvector(patches_total_int_curvature_tensor, 'sorted_basis')
    
    # Define curvature tensor in principle basis with eigenvalues.
    patches_principle_curvatures = calculate_patches_principle_curvatures(patches_mean_gaussian_curvatures, ordered='magnitude')
    patches_int_curvature_tensor_in_local_principle_basis = np.array([np.diag([kmax, kmin, 0.0])
                                        for kmax, kmin in patches_principle_curvatures[['kappa1', 'kappa2']].values])
    
    # Express curvature tensor in euclidean basis.
    patches_curvature_tensor_euclidean_basis = \
        MMdg.tensor_basis_transformation(patches_int_curvature_tensor_in_local_principle_basis, patches_int_curvature_tensor_principle_basis_vectors)

    return patches_curvature_tensor_euclidean_basis


def calculate_mean_and_gaussian_curvature_on_patches_from_triangles(triangles, patches_triangle_ids_dict):
    """ Given a dictionary of patches, as triangle indices indexed with patch ids, calculate the area weighted means
        of the gaussian and mean curvature on these patches from the values on the triangles.
        
        Parameters
        ----------
        patches_triangle_ids_dict : dict (patch_id, [triangle indices]), size M
            Dictionary containg patches as lists of triangle indices.
        triangles : Pandas.DataFrame, size N
            Table containing triangle state properties: ['mean_curvature', 'gaussian_curvature', 'area']
        
        Returns:
        --------
        patches : Pandas.DataFrame, size M
            Dataframe indexed with patch_id with columns ['mean_curvature', 'gaussian_curvature']
    """
    
    print('Calculating the mean and gaussian curvature on', len(patches_triangle_ids_dict), 'patches...')

    patches_mean_curvature = MMTool.calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles, triangles['mean_curvature'].values, patches_triangle_ids_dict)
    patches_gaus_curvature = MMTool.calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles, triangles['gaussian_curvature'].values, patches_triangle_ids_dict)

    patches_curvatures = pd.DataFrame({'mean_curvature': patches_mean_curvature, 'gaussian_curvature':patches_gaus_curvature},\
                            index=patches_triangle_ids_dict.keys())

    return patches_curvatures


#Update the triangles table with the mean and gaussian curvature.
def calculate_triangle_mean_and_gaussian_curvature(dbonds, vertices, triangles, dbonds_geometry):
    """ Calculate the integrated mean curvature on all the internal bonds in the triangle network.
        Definition: 0.5 * theta * bond_length, where theta is the _signed_ angle between the normal vectors on the neighbouring triangles.
        The integrated mean curvature Bonds at the network boundary are set to zero.
        
        Parameters
        ----------
        dbonds : Pandas.DataFrame, size M
            Table with network dbonds in DCEL form.
        vertices : Pandas.DataFrame, size N
            Table containing the angular_defect of every vertex: 'angular_defect'
        triangles : Pandas.DataFrame, size P
            Table containing triangle vertex ids and area: ['vertex_id_1', 'vertex_id_2', 'vertex_id_3', 'area']
        dbonds_geometry : Pandas.DataFrame, size M
            Table containing the signed angle, length and integrated mean curvature on every dbond.
        
        Returns:
        --------
        triangles : Pandas.DataFrame, size M
            Extend triangles table with columns mean_curvature and gaussian_curvature
    """
        
    print('Calculating mean and gaussian curvature on all triangles...')
    
    dbonds_by_vertex_id   = dbonds.groupby('vertex_id')
    dbonds_by_triangle_id = dbonds.groupby('triangle_id')
    
    triangles_mean_curvature     = np.zeros(len(triangles))
    triangles_gaussian_curvature = np.zeros(len(triangles))
    
    # Get angular defect and int mean curvature for all vertices and dbonds connected to each triangle.
    triangles_dbonds_id   = [dbonds_by_triangle_id.get_group(tid).index.values for tid in triangles.index]
    triangles_dbonds_id   = np.array(triangles_dbonds_id)
    triangles_vertices_id = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].values.astype(int)
    
    triangles_vertices_angle_defects = vertices['angular_defect'].iloc[triangles_vertices_id.reshape(-1)].values
    triangles_dbonds_int_mean_curvat = dbonds_geometry['int_mean_curvature'].iloc[triangles_dbonds_id.reshape(-1)].values
    
    triangles_vertices_angle_defects = triangles_vertices_angle_defects.reshape(-1, 3)
    triangles_dbonds_int_mean_curvat = triangles_dbonds_int_mean_curvat.reshape(-1, 3)
    
    # Get all triangles connected to each vertex.
    # Remove indinces MMDef.BOUNDARY_TRIANGLE_ID from connected triangles.
    vertices_connected_triangles_id = [dbonds_by_vertex_id.get_group(vid)['triangle_id'].values for vid in vertices.index]
    vertices_connected_triangles_id = [tids[tids != MMDef.BOUNDARY_TRIANGLE_ID] for tids in vertices_connected_triangles_id]
    
    # Get total area of triangles connected to each vertex.
    triangles_area = triangles['area'].values
    vertices_connected_triangles_area = [triangles_area[tids] for tids in vertices_connected_triangles_id]
    vertices_connected_area = [np.sum(conn_triangles_area) for conn_triangles_area in vertices_connected_triangles_area]
    vertices_connected_area = np.array(vertices_connected_area)
    
    triangles_vertices_connected_area = vertices_connected_area[triangles_vertices_id.reshape(-1)]
    triangles_vertices_connected_area = triangles_vertices_connected_area.reshape(-1, 3)
    
    ### There are several definitions for the Gaussian (and mean) curvature on the triangle.
    # We can weight the angular defect \beta_i between the N_i connected triangles around vertex i as:
    # 1. Uniformly     : \beta_i / N_i
    # 2. Area weighted : \beta_i / A_i^tot (A^i_tot is total area of connected triangles at vertex i)
    # 3. Angle weighted: \beta_i * \alpha_ij / \alpha_i^tot 
    #    (\alpha_i^tot is the sum of the angles, sum_j \alpha_ij, that the connected triangles make with the vertex).
    # Def 1:
    #triangles_gausstian_curvature = [triangle_anglular_defects / triangle_vertex_order for triangle_anglular_defects, triangle_vertex_order in\
    # zip(triangles_vertices_angle_defects, triangles_vertices_order)]
    # Def 2:  
    triangles_vertices_weighted_angular_defects = [vertices_defect / (vertices_conn_area + MMDef.EPSILON) 
        for vertices_defect, vertices_conn_area in zip(triangles_vertices_angle_defects, triangles_vertices_connected_area)]
    triangles_gaussian_curvature = np.sum(triangles_vertices_weighted_angular_defects, axis=1)
            
    ## Mean curvature definition. (Filter zero area triangles)
    triangles_non_zero_area = triangles.area > MMDef.EPSILON
    triangles_area = triangles[triangles_non_zero_area].area.values
    triangles_tot_int_mean_curvature = np.sum(triangles_dbonds_int_mean_curvat, axis=1)[triangles_non_zero_area] 
    triangles_mean_curvature[triangles_non_zero_area] = 0.5 * triangles_tot_int_mean_curvature / triangles_area
        
    ## Add mean and gaussian curvatures to triangles table.
    triangles['gaussian_curvature'] = triangles_gaussian_curvature
    triangles['mean_curvature']     = triangles_mean_curvature

    return triangles


def calculate_integrated_mean_curvature_on_dbonds(dbonds, vertices, triangles):
    """ Calculate the integrated mean curvature on all the internal bonds in the triangle network.
        Definition: 0.5 * theta * bond_length, where theta is the _signed_ angle between the normal vectors on the neighbouring triangles.
        The integrated mean curvature Bonds at the network boundary are set to zero.
        
        Parameters
        ----------
        dbonds : Pandas.DataFrame, size M
            Table with network dbonds in DCEL form.
        vertices : Pandas.DataFrame, size N
            Table with vertex positions: ['x_pos','y_pos','z_pos']
        triangles : Pandas.DataFrame, size P
            Table containing triangle normal vectors: ['normal_x', 'normal_y', 'normal_z']
        
        Returns:
        --------
        dbonds_geometry : Pandas.DataFrame, size M
            Return signed_angle, bond_length and integrated mean curvature for every bond in bonds table.
            columns =  ['signed_angle', 'length', 'int_mean_curvature']
    """

    print('Calculating integrated mean curvature for all bonds...')
    
    ### Since a dbond and its conjugate dbond have the same integrated mean curvature, only looks at half of the dbonds.
    bonds = dbonds[dbonds.index < dbonds['conj_dbond_id']]
    
    ### Calculate the length of each bond.
    bonds_right_vertex_id = bonds['vertex_id'].values
    bonds_left_vertex_id  = dbonds.iloc[ bonds['left_dbond_id'] ]['vertex_id'].values
    bonds_dR = vertices[['x_pos','y_pos','z_pos']].iloc[bonds_left_vertex_id].values - vertices[['x_pos','y_pos','z_pos']].iloc[bonds_right_vertex_id].values
    bonds_length = np.sqrt( np.sum(bonds_dR * bonds_dR, axis=1) )
           
    ### Remove bonds at the boundary for which the neighbouring triangle does not exists.
    bonds_is_boundary = (bonds['triangle_id'].values == -1) | (dbonds.iloc[ bonds['conj_dbond_id'] ]['triangle_id'].values == -1)
    bonds_is_internal = np.invert(bonds_is_boundary)
    int_bonds = bonds[bonds_is_internal]
       
    ### Obtain normal vectors on both triangles on the sides.
    triangles_normals =  triangles[['normal_x', 'normal_y', 'normal_z']]
    
    int_bonds_triangle_id  = int_bonds['triangle_id'].values
    int_bonds_conj_triangle_id  = dbonds.iloc[ int_bonds['conj_dbond_id'] ]['triangle_id'].values    
    
    int_bonds_triangle_normal = triangles_normals.iloc[int_bonds_triangle_id].values    
    int_bonds_conj_triangle_normal = triangles_normals.iloc[int_bonds_conj_triangle_id].values
    
    ### Calculate angle between adjacent normals of each bond.
    int_bonds_dot_n1n2 = np.array([n1.dot(n2) for n1, n2 in zip(int_bonds_triangle_normal, int_bonds_conj_triangle_normal)])
    int_bonds_dot_n1n2[abs(int_bonds_dot_n1n2) > 1.] = np.sign(int_bonds_dot_n1n2[abs(int_bonds_dot_n1n2) > 1.])
    int_bonds_theta    = np.arccos(int_bonds_dot_n1n2)
    
    ### Determine sign of angle: are the 2 neigbouring triangles convex (-1) or concave (+1).
    # Find the distance between the plane spanned by the conjugate triangle and the vertex of this triangle that is not connected to the shared bond.
    # This is given by the dot product of the normal on the conj triangle and the distance vector to the outside vertex of this triangle.
    int_bonds_left_left_dbonds_id  = dbonds.iloc[ int_bonds['left_dbond_id'] ]['left_dbond_id'].values
    int_bonds_left_left_vertex_id  = dbonds.iloc[ int_bonds_left_left_dbonds_id ]['vertex_id'].values
    int_bonds_right_vertex_id = int_bonds['vertex_id'].values
    
    int_bonds_triangle_outside_vertex_pos = vertices[['x_pos','y_pos','z_pos']].iloc[int_bonds_left_left_vertex_id].values
    outside_vertex_distance_vec_to_conj_triangle_plane = int_bonds_triangle_outside_vertex_pos - vertices[['x_pos','y_pos','z_pos']].iloc[int_bonds_right_vertex_id].values
    int_bonds_is_convex  = [outside_vertex_pos.dot(n2) for n2, outside_vertex_pos in zip(int_bonds_conj_triangle_normal, outside_vertex_distance_vec_to_conj_triangle_plane)]
    int_bonds_is_convex  = np.sign(int_bonds_is_convex)
    int_bonds_is_concave = -1 * int_bonds_is_convex
    
    ### Calculte integrated mean curvature on each bond.
    int_bonds_length = bonds_length[bonds_is_internal]
    int_bonds_signed_theta = int_bonds_is_concave * int_bonds_theta
    int_bonds_integrated_mean_curvature = 0.5 * int_bonds_signed_theta * int_bonds_length    
    
    ### Fill dbond geometry table for bonds and their conj_bonds
    dbonds_geometry = pd.DataFrame(0.0, columns = ['signed_angle', 'length', 'int_mean_curvature'], index = dbonds.index)
    # Define different groups of sdbond ids.
    bonds_id      = bonds.index
    conj_bonds_id = bonds['conj_dbond_id'].values
    
    int_bonds_id      = int_bonds.index.values
    int_conj_bonds_id = int_bonds['conj_dbond_id'].values
    
    # Fill table with lengths of all dbonds.
    dbonds_geometry.loc[bonds_id, 'length']      = bonds_length
    dbonds_geometry.loc[conj_bonds_id, 'length'] = bonds_length
    # Fill table with signed angle and integrated mean curvature of all internal dbonds. Keep boundary dbonds at zero.
    dbonds_geometry.loc[int_bonds_id, 'signed_angle']       = int_bonds_signed_theta
    dbonds_geometry.loc[int_bonds_id, 'int_mean_curvature'] = int_bonds_integrated_mean_curvature
    
    dbonds_geometry.loc[int_conj_bonds_id, 'signed_angle']       = int_bonds_signed_theta
    dbonds_geometry.loc[int_conj_bonds_id, 'int_mean_curvature'] = int_bonds_integrated_mean_curvature    
    
    return dbonds_geometry


def calculate_angular_defect_per_vertex(vertices, triangles):
    """ Calculate the angular defect on each vertex in the triangular network.
        Definition: 2*pi - sum of angles between consecutive edges that are connected to a vertex.
        
        Parameters
        ----------
        vertices : Pandas.DataFrame, size N
            Table with vertex position
        triangles : Pandas.DataFrame, size P
            Table containing triangle vectors ['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y', 'dr_13_z']
        
        Returns:
        vertices : Pandas.DataFrame, size N
            Extend vertices table with column 'angular_defect'
    
    
    """

    print('Calculating angular defect for all vertices...')

    # Calculate the triangle angles between the triangle vectors for all triangles.
    triangles_E12_xyz = triangles[['dr_12_x', 'dr_12_y', 'dr_12_z']].values
    triangles_E13_xyz = triangles[['dr_13_x', 'dr_13_y', 'dr_13_z']].values
    triangles_E23_xyz = triangles_E13_xyz - triangles_E12_xyz
    
    E12_unit = MMTool.unit_vector(triangles_E12_xyz)
    E13_unit = MMTool.unit_vector(triangles_E13_xyz)
    E23_unit = MMTool.unit_vector(triangles_E23_xyz)

    triangels_inner_E12E13 = np.array([np.dot(E12, E13) for E12, E13 in zip(E12_unit, E13_unit)])
    triangels_inner_E12E23 = np.array([np.dot(-E12, E23) for E12, E23 in zip(E12_unit, E23_unit)])
   
    triangels_inner_E12E13[triangels_inner_E12E13 > 1.] = 1.
    triangels_inner_E12E23[triangels_inner_E12E23 > 1.] = 1.

    triangles_angle1 = np.arccos(triangels_inner_E12E13)
    triangles_angle2 = np.arccos(triangels_inner_E12E23)
    triangles_angle3 = np.pi - (triangles_angle1 + triangles_angle2)     
    
    # Find all triangle corners that coincide at each vertex.
    vertices_total_angle = np.zeros(len(vertices))
    for vertex_id in vertices.index:
        vertices_total_angle[vertex_id] += np.sum(triangles_angle1[ triangles['vertex_id_1'].values == vertex_id ])
        vertices_total_angle[vertex_id] += np.sum(triangles_angle2[ triangles['vertex_id_2'].values == vertex_id ])
        vertices_total_angle[vertex_id] += np.sum(triangles_angle3[ triangles['vertex_id_3'].values == vertex_id ])
    
    vertices_angular_defect = 2*np.pi - vertices_total_angle
    
    # Add angular defects to vertices table.
    vertices['angular_defect'] = vertices_angular_defect
    
    return vertices