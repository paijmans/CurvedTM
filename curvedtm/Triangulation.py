#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 2018

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool, MMGeom, MMCurv, MMdg

class Triangulation:
    """ Creates a triangular mesh from the Curved Tissue Miner cellular network.
    
    Calculate geometric properties such as cell elongation and tissue curvature.
        
    Parameters:
    -----------
    datapath_root : string
        Path to the Tissue Miner root directory of the movie to load.
    network_type : string, default: 'subcellular_triangles'
        Set the triangulation to use. Examples: ['subcellular_triangles', 'dual_lattice_triangles']
    refresh_fundaments : [True, False]
        Whether to set the triangulation fundamentals.
        - Initialize Vertex positions table for triangulation.
        - Initialize Triangle table with 3 verted ids for each triangle.
        - Initialize Doubly Connected Edge List (DCEL) for triangulation.
    refresh_geometry : [True, False]
        Whether to calculate the geometric properties of the triangle network.
        - Triangle area, normal vector and orientation angles Theta_x and theta_y.
        - Vertex defect of each vertex.
        - Integrated mean curvature of each edge.
        - Integrated curvature tensor of each edge.
    refresh_triangle_state : [True, False]
        Whether to calculate the triangle state properties.
        - Triangle mean and gaussian curvatures.
        - Triangle elongation tensor.
    refresh_cell_state : [True, False]
        Whether to calculate the cell state properties.
        - Cell mean and Gaussian curvature.
        - Cell Elongation tensor.
        - Cell curvature tensor.
        
    Methods:
    --------
    run() : 
        Create and updated triangulation in sequence for each frame.
    run_frame(frameIdx) : 
        Create triangulation for specific frame.
    run_parallel(processes_number = 4) : 
        Create triangulation for all frames in parallel.
    """  
    
    def __init__(self, datapath_root, network_type = 'subcellular_triangles', frames_to_process = [],\
                 refresh_fundaments = True, refresh_geometry = True, refresh_triangle_state = True, refresh_cell_state = True):
               
        #Set data paths for Tissue Miner data structure.
        self.movie_name      = os.path.basename(os.path.normpath(datapath_root))
        self.datapath_root   = datapath_root
        self.datapath_frames = os.path.join(self.datapath_root, MMDef.DATAPATH_ROOT_FRAMES + '/')
        self.frames_to_process = frames_to_process
        
        MMTool.check_path_existence(self.datapath_root)
                
        #Set directory for output hdf files.
        self.path_hdf5_DB = self.datapath_frames
        if not os.path.isdir(self.path_hdf5_DB):
            print('Error: Output directory of CurvedTM not found. Run TissueMinerDBLoader first!')
            sys.exit()
            
#         # Check whether hdf5 databse files exist for frames in frames_to_process.
#         present_db_frames = MMTool.check_hdf5_db_files(self.frames_to_process, self.path_hdf5_DB)
#         if not present_db_frames.all():
#             print('Warning: The requested frames', np.where(np.invert(present_db_frames))[0], 'are not present as database files in', self.path_hdf5_DB)
                    
        #Save refresh conditions
        self.refresh_fundaments     = refresh_fundaments
        self.refresh_geometry       = refresh_geometry
        self.refresh_triangle_state = refresh_triangle_state
        self.refresh_cell_state     = refresh_cell_state
        
        #Assign triangulation network type.
        assert network_type in MMDef.valid_networks, "Wrong network type given: " + str(network_type)+". Allowed types: "+str(valid_networks)
        self.network_type = network_type
                                   
        print('Triangulation of movie', self.movie_name, 'for network type', self.network_type, 'inititalized.')

        
    def run(self):
        """ Process movie frames sequentially. """        

        for frameIdx in self.frames_to_process:
            try:
                self.run_frame(frameIdx)
            except Exception as e: 
                print('ERROR: Something went wrong in frame', frameIdx, 'while creating the triangle network.')
                print(e)

        print('Done processing all frames.')
        
    
    def run_parallel(self, processes_number = 4):
        """ Process movie frames in parallel.
        
            Parameters:
            -----------
            processes_number : int (default: 4)
                Number of corres to process frames in parallel.     
        """

        pool = mp.Pool(processes=processes_number)       
        pool.map(self.run_frame, self.frames_to_process)
        pool.close()
        
        print('Done processing', len(self.frames_to_process), 'frames.')

        
    def run_frame(self, frameIdx):
        """ Process specific frame
        
            Parameters:
            -----------
            frameIdx : int
                Index of frame to process.        
        """

        ## Calculate triangle network fundamental data structure: vertices, triangle topology and DCEL.
        if self.refresh_fundaments:
            triangle_vertices, triangles, triangle_network_DCEL_table = self.create_triangle_network_fundamentals(frameIdx)
        else:
            triangle_vertices = self.table_io_per_frame('vertices', frameIdx, self.network_type)
            triangles         = self.table_io_per_frame('triangles', frameIdx, self.network_type)
            triangle_network_DCEL_table = self.table_io_per_frame('dbonds', frameIdx, self.network_type)
        
        ## Calculate triangle area and normal, vertex angular defect and dbonds integrated mean curvature and integrated curvature tensor.
        if self.refresh_geometry:
            triangle_vertices, triangles, dbonds_geometry, dbonds_euclidean_int_curvature_tensor =\
                self.calculate_triangle_network_geometry(frameIdx, triangle_vertices, triangles, triangle_network_DCEL_table)
        else:
            dbonds_geometry = self.table_io_per_frame('dbond_geometry', frameIdx, self.network_type)
            dbonds_euclidean_int_curvature_tensor = self.table_io_per_frame('dbond_int_curvature_tensor', frameIdx, self.network_type)
            
        ## Update triangles table with face properties mean and gaussian curvatures and elongation tensor.
        # Save triangle elongation tensor.
        if self.refresh_triangle_state:
            triangles, triangles_elongation_tensor =\
                self.calculate_triangle_network_state_properties(frameIdx, triangle_network_DCEL_table, triangle_vertices, dbonds_geometry, triangles)
        else:
            triangles_elongation_tensor = self.table_io_per_frame('elongation_tensor', frameIdx, self.network_type)

        ## Update cell table with area weighted averages of the triangle properties.
        # Calculate and save cell elongation and cell curvature tensors.
        if self.refresh_cell_state:
            cells = self.calculate_cell_state_properties(frameIdx, triangle_network_DCEL_table, triangles, triangles_elongation_tensor,\
                                                         dbonds_euclidean_int_curvature_tensor)
        else:
            cells = self.table_io_per_frame('cells', frameIdx, 'cells')
            
        print('Done processing the frame indexed', frameIdx, '.')


    def create_triangle_network_fundamentals(self, frameIdx):
        """ Create triangle network fundamentals:
            - Vertices table
            - Triangle faces topology in ordered vertex ids
            - DCEL of triangle network
        """
        
        ### Create vertex positions and sorted vertex ids of triangles.
        if self.network_type == 'dual_lattice_triangles':
            triangle_vertices, triangles = self.make_dual_lattice_from_cell_network(frameIdx)
        else: 
            triangle_vertices, triangles = self.make_subcellular_triangle_network(frameIdx)

        ### Make doubly connected edge list for triangle network.
        cell_network_DCEL_table = self.table_io_per_frame('dbonds', frameIdx)
        triangle_network_DCEL_table = MMTool.make_triangle_DCEL_table(triangles, cell_network_DCEL_table)
        MMTool.check_dbonds_consistency(triangle_network_DCEL_table, strict = False)

        # Save vertices, triangles and DCEL tables to disk.
        self.table_io_per_frame('vertices',  frameIdx, self.network_type, 'save', triangle_vertices)
        self.table_io_per_frame('triangles', frameIdx, self.network_type, 'save', triangles)
        self.table_io_per_frame('dbonds', frameIdx, self.network_type, 'save', triangle_network_DCEL_table)
            
        return triangle_vertices, triangles, triangle_network_DCEL_table
    
        
    def calculate_triangle_network_geometry(self, frameIdx, triangle_vertices, triangles, triangle_dbonds):
        """ Calculate geometry of triangle network:
            - Triangle area and unit normal vector.
            - Angles theta_x, theta_y of triangle unit normal with the z-axis.
            - Triangle network vertex angular defect.
            - Triangle network edge integrated curvature.      
            - Triangle network integrated curvature tensor on each dbond.        
        """
        
        ### Update triangles table with area, unit normal vector and angles theta_x, theta_y of unit normal with z-axis.
        triangles = MMGeom.calculate_triangle_area_and_unit_normal_vector(triangle_vertices, triangles)

        ### Update vertices table of triangle network with vertex angular defect.
        triangle_vertices = MMCurv.calculate_angular_defect_per_vertex(triangle_vertices, triangles)       

        ### Create dbond geometry table with dbond length, angle and integrated mean curvature.
        dbonds_geometry = MMCurv.calculate_integrated_mean_curvature_on_dbonds(triangle_dbonds, triangle_vertices, triangles)

        ### Create integrated mean curvature table with the int. curvature tensor given in Eucleadean coordinates.
        dbonds_euclidean_int_curvature_tensor = \
            MMCurv.calculate_integrated_curvature_tensor_on_dbonds(triangles, triangle_dbonds, triangle_vertices, dbonds_geometry)
        
        # Save vertices, triangles, dbond_geometry and dbond int curv tensor to disk.
        self.table_io_per_frame('vertices', frameIdx, self.network_type, 'save', triangle_vertices)
        self.table_io_per_frame('triangles', frameIdx, self.network_type, 'save', triangles)        
        self.table_io_per_frame('dbond_geometry', frameIdx, self.network_type, 'save', dbonds_geometry)
        self.table_io_per_frame('dbond_int_curvature_tensor', frameIdx, self.network_type, 'save', pd.DataFrame(dbonds_euclidean_int_curvature_tensor.reshape(-1,9)))
            
        return triangle_vertices, triangles, dbonds_geometry, dbonds_euclidean_int_curvature_tensor
            
        
    def calculate_triangle_network_state_properties(self, frameIdx, triangle_dbonds, triangle_vertices, dbonds_geomtery, triangles):
        """ Calculate the triangle state properties:
            - Unique triangle id.
            - Gaussian curvature
            - Mean curvature
            - Elongation norm and angle.        
        """
                    
        print('Calculating triangle state properties: mean and gaussian curvatures and elongation tensor')

        ### Update triangle table with unique triangle id.
        triangles = MMTool.generate_triangle_ids(triangle_dbonds, triangles)

        ### Update triangle table with mean and gaussian curvatures.
        triangles = MMCurv.calculate_triangle_mean_and_gaussian_curvature(triangle_dbonds, triangle_vertices,\
                                                                          triangles, dbonds_geomtery)
        
        ### Update triangle table with elongation norm and angle two_phi.
        triangles, triangles_elongation_tensor = MMGeom.calculate_triangle_elongation_tensor(triangles)
            
        ### Calculate curvature tensor on each triangle from integrated curvature tensors on the 3 dbonds.
#         triangles_triangle_idxs_dict = dict(zip(triangles.index, [np.array([tid]) for tid in triangles.index]))
#         triangles_int_curvature_tensor_dict = MM.calculate_average_curvature_tensor_on_patches(triangles_triangle_idxs_dict,
#                                                                                          dbonds_euclidean_int_curvature_tensor_dict,
#                                                                                          triangles, triangle_dbonds)

#         self.table_io_per_frame('curvature_tensor', frameIdx, network_type=self.network_type,
#                               action='save', table=triangles_int_curvature_tensor_dict)

        #Save triangle elongation tensor and triangles to disk.
        self.table_io_per_frame('elongation_tensor', frameIdx, self.network_type, 'save', pd.DataFrame(triangles_elongation_tensor.reshape(-1, 9)))
        self.table_io_per_frame('triangles', frameIdx, self.network_type, 'save', triangles)

        return triangles, triangles_elongation_tensor
    
    
    def calculate_cell_state_properties(self, frameIdx, triangle_dbonds, triangles, triangles_elongation_tensor, dbonds_euclidean_int_curvature_tensor):
        """ Update cell table with area weighted averages of the triangle properties.
            - Mean and Gaussian curvature
            - Curvature tensor.
            - Elongation tensor.
        """
        
        print('Calculating cell state properties from triangles: mean and gaussian curvatures and elongation and curvature tensor...')
        
        ### Load tensors and cells from files.
        cells = self.table_io_per_frame('cells', frameIdx, 'cells')
        
        ### Calculate cell elongation tensor.
        cells, cells_elongation_tensor = MMTool.update_cell_table_with_tensors_from_triangle_tensors(cells, triangles,\
                                                                                                     triangles_elongation_tensor, 'elongation_tensor')        

        ### Calculate cell mean and gaussian curvature.
        cells = MMTool.update_cell_table_with_curvature_from_triangle_curvatures(cells, triangles)
        
        ### Calculate cell curvature tensor.
        triangles_grouped_by_cell_id = triangles.groupby('cell_id')
        cells_triangle_idxs = np.array([triangles_grouped_by_cell_id.get_group(cid).index.values for cid in cells.index])
        cells_triangle_idxs_dict = dict(zip(cells.index, cells_triangle_idxs))

        cells_euclidean_curvature_tensor =\
            MMCurv.calculate_average_curvature_tensor_on_patches(cells_triangle_idxs_dict, dbonds_euclidean_int_curvature_tensor,\
                                                                 triangles, triangle_dbonds)
        
        # Save cell elongation tensor, curvature tensor and cells table to disk.
        self.table_io_per_frame('elongation_tensor', frameIdx, 'cells', 'save', table=pd.DataFrame(cells_elongation_tensor.reshape(-1, 9)))
        self.table_io_per_frame('curvature_tensor', frameIdx, 'cells', 'save', table=pd.DataFrame(cells_euclidean_curvature_tensor.reshape(-1, 9)))        
        self.table_io_per_frame('cells', frameIdx, 'cells', 'save', table=cells)
        
        return cells
    

    def make_subcellular_triangle_network(self, frameIdx):
        """ Produce tables with vertices and faces (triangles) for the subcellular triangle network. 
        
            Parameters:
            -----------
            frameIdx : int
                Frame index to create triangulation for.
        
            Returns:
            --------
            dual_vertices, triangles : Pandas.DataFrame, Pandas.DataFrame
                Vertex positions and triangulation topology in vertex_ids triple per triangle.
        """
        
        print('Making the vertex and triangle tables for the subcellular traingle network.')
        ### Load cell network data first.
        cells                  = self.table_io_per_frame('cells', frameIdx)
        dbonds                 = self.table_io_per_frame('dbonds', frameIdx)
        vertices               = self.table_io_per_frame('vertices', frameIdx)
        sorted_dbonds_per_cell = self.table_io_per_frame('sorted_cell_dbonds_per_frame', frameIdx) 
                
        ### Make table with vertices
        # Concatonate cell center and cell boundary vertices into one table.
        subcell_vertices = cells[['center_x', 'center_y', 'center_z']].copy()
        subcell_vertices.rename(columns={'center_x':'x_pos', 'center_y':'y_pos', 'center_z':'z_pos'}, inplace=True)
        cell_network_vertex_indices = np.unique(dbonds['vertex_id'])

        # Re-assign cell_ids of cell centers to new vertex ids. Make dictionary to map every cell id to new vertex id.
        max_cell_vertex_id = cell_network_vertex_indices.max()
        cell_id_to_vertex_index = dict( zip(cells.index, range(max_cell_vertex_id + 1, max_cell_vertex_id + len(cells) + 1)) )
        # Add boundary cell and map to boundary triangle.
        cell_id_to_vertex_index[MMDef.BOUNDARY_CELL_ID] = MMDef.BOUNDARY_TRIANGLE_ID
        subcell_vertices['vertex_id'] = np.array([cell_id_to_vertex_index[cell_id] for cell_id in cells.index])
        subcell_vertices.set_index('vertex_id', inplace=True, drop=True)
        # Add cell center vertices with cell network vertices.
        subcell_vertices = pd.concat([ vertices[['x_pos','y_pos','z_pos']].loc[cell_network_vertex_indices], subcell_vertices ], axis = 0)

        
        ### Make the triangle table from the cells.
        # Make a dictionary that maps each cell_id to the its vertex_ids in cc direction.
        # We loop over this dictionary to produce a list of triples of triangle vertex ids.
        number_of_triangles = sum(cells['neighbour_number'].values)
        triangles_vertex_ids = [[]]*number_of_triangles
        triangles_cell_id = [-1]*number_of_triangles
        
        vertices_indices_per_cell_list = [dbonds.iloc[ sorted_dbonds_per_cell[cell_id] ]['vertex_id'].values for cell_id in cells.index]
        sorted_vertices_per_cell_id = dict(zip(cells.index, vertices_indices_per_cell_list))        

        # Now find the three vertex ids for each triangle by looping over all cells.
        triangle_index = 0
        for cell_id, sorted_cell_vertex_ids in sorted_vertices_per_cell_id.items():

            cell_center_vertex_id = cell_id_to_vertex_index[cell_id]
            cell_neighbour_number = len(sorted_cell_vertex_ids)
            triangle_vertex_ids_in_cell = [np.array([vidi, vidip1, cell_center_vertex_id]) 
                                               for vidi, vidip1 in zip(sorted_cell_vertex_ids, np.roll(sorted_cell_vertex_ids, -1))]
            triangles_vertex_ids[triangle_index:triangle_index+cell_neighbour_number] = triangle_vertex_ids_in_cell
            triangles_cell_id[triangle_index:triangle_index+cell_neighbour_number] = np.full(cell_neighbour_number, cell_id)
            triangle_index += cell_neighbour_number

        triangles = self.initialize_triangles_dataframe( dict( zip(range(number_of_triangles), triangles_vertex_ids) ), triangles_cell_id )

        return subcell_vertices, triangles
        

    def initialize_triangles_dataframe(self, sorted_vertex_ids_per_3_vertex_id, triangles_cell_id = []):
        """ Initializing triangles dataframe. Remove triangles connected to outside the tissue. """

        ### Convert dictionary to array.
        triangles_sorted_vertex_ids = np.array(list(sorted_vertex_ids_per_3_vertex_id.values()))
        Ntriangles = len(triangles_sorted_vertex_ids)
        
        ### Create triangles DataFrame.
        triangles_columns = ['triangle_id', 'cell_id', 'vertex_id_1', 'vertex_id_2', 'vertex_id_3', 
                             'dr_12_x', 'dr_12_y', 'dr_12_z', 'dr_13_x', 'dr_13_y', 'dr_13_z', 
                             'area', 'normal_x', 'normal_y', 'normal_z', 'rotation_angle_x', 'rotation_angle_y', 'rotation_angle_z',
                             'elongation_tensor_norm', 'elongation_tensor_twophi', 'mean_curvature', 'gaussian_curvature']
        
        triangles = pd.DataFrame(0, columns=triangles_columns, index=range(Ntriangles))
        
        ### Add vertex ids to DataFrame
        triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']] = triangles_sorted_vertex_ids
        #triangles = triangles[(triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']] != MMDef.BOUNDARY_TRIANGLE_ID).all(axis=1)]
        #triangles.reset_index(inplace=True, drop=True)

        #If triangles cell id is given, assign cell id to triangles table. Only meaningfull for subcellular network.
        if len(triangles_cell_id) == len(triangles):
            triangles['cell_id'] = np.array(triangles_cell_id).astype(dtype=int)
            #triangles.loc[-1, 'cell_id'] = BOUNDARY_CELL_ID

        triangles[['dr_12_x', 'dr_12_y', 'dr_12_z', 'dr_13_x', 'dr_13_y', 'dr_13_z',\
                   'area', 'normal_x', 'normal_y', 'normal_z', 'rotation_angle_x', 'rotation_angle_y', 'rotation_angle_z',\
                   'elongation_tensor_norm', 'elongation_tensor_twophi', 'gaussian_curvature', 'mean_curvature']].astype(dtype = float, copy=False)
        
        return triangles
            

    def process_higher_order_vertices(self, sorted_cell_ids_per_higher_order_vertex_id, dual_vertices, triangles):
        """ Create vertices and triangles for the triangle network stemming from higher order vertices.
        
        For all vertices connected to 4 or more dbonds, create new dual vertex and the connected triangles.
        Only needed for the dual vertex network, not for the subcellular network.        
        """

        #Find higher order vertices, make new dual vertices defined as the centroid of the connected vertices
        dual_vertex_ids_to_idx = dict( zip(dual_vertices.index, range(len(dual_vertices))) )
        for higher_order_vertex_index, connected_dual_vertex_ids in sorted_cell_ids_per_higher_order_vertex_id.items():

            #If higher order vertex is connected to the tissue boundary, skip the vertex.
            if (np.array(connected_dual_vertex_ids) == BOUNDARY_TRIANGLE_ID).any():
                continue

            #Produce new id and new dual vertex associated with the higher order vertex.
            new_dual_vertex_xyz, new_dual_vertex_id = MM.calculateCenterOfMassAndID_dual(connected_dual_vertex_ids, dual_vertices)

            #Add new dual vertex to dual_vertices positions table and dual_vertices connectivity tables.
            dual_vertices.at[new_dual_vertex_id, ['x_pos', 'y_pos', 'z_pos']] = new_dual_vertex_xyz

            #Add new triangles to triangle topology table.
            next_connected_dual_vertex_ids = np.roll(connected_dual_vertex_ids,-1)
            last_triangle_index = triangles.index[-1]
            for dual_vertex_id, next_dual_vertex_id in zip(connected_dual_vertex_ids, next_connected_dual_vertex_ids):

                last_triangle_index += 1
                triangles.at[last_triangle_index, ['vertex_id_1', 'vertex_id_2', 'vertex_id_3']] =\
                np.array([dual_vertex_id, next_dual_vertex_id, new_dual_vertex_id], dtype=np.int64)
                    
                
    # Produce tables with vertices, their connectivity and faces (triangles) of the dual latice from the cells network.
    def make_dual_lattice_from_cell_network(self, frameIdx):
        """ Create dual lattice triangulation of cell network.
        
            Parameters:
            -----------
            frameIdx : int
                Frame index to create triangulation for.
        
            Returns:
            --------
            dual_vertices, triangles : Pandas.DataFrame, Pandas.DataFrame
                Vertex positions and triangulation topology in vertex_ids triple per triangle.
        """

        print('Making the vertex and triangle tables for the dual lattice of the cell network.')
        ### Load cell network data.
        cells                  = self.table_io_per_frame('cells', frameIdx)
        dbonds                 = self.table_io_per_frame('dbonds', frameIdx)
        sorted_dbonds_per_cell = self.table_io_per_frame('sorted_cell_dbonds_per_frame', frameIdx)

        ### Make table with vertices from the cell centers of the cells table.
        # Reset cell_id to new vertex id.
        # dual_vertices = MM.calculate_dual_vertex_positions_from_cells(self.dbonds, self.vertices, self.sorted_dbonds_per_cell)
        dual_vertices = cells[['center_x', 'center_y', 'center_z']].copy()
        dual_vertices.rename(columns={'center_x': 'x_pos', 'center_y': 'y_pos', 'center_z': 'z_pos'}, inplace=True)
        cell_id_to_vertex_index = dict(zip(cells.index, range(len(cells))))
        # Add boundary cell and map to boundary triangle.
        cell_id_to_vertex_index[BOUNDARY_CELL_ID] = BOUNDARY_TRIANGLE_ID
        dual_vertices['vertex_id'] = np.array([cell_id_to_vertex_index[cell_id] for cell_id in cells.index])
        dual_vertices.set_index('vertex_id', inplace=True, drop=True)

        ### Create triangles table from all order 3 vertices. Deal with higher order vertices later.
        # Find connected cells to every vertex, and split between order 3 and higher order (ignore order 2 vertices at the tissue boundary).
        sorted_cell_ids_per_2_vertex_id, sorted_cell_ids_per_3_vertex_id, sorted_cell_ids_per_higher_order_vertex_id = \
            MM.get_vertex_connected_cell_ids_cc(dbonds)

        # Translate cell ids into vertex ids.
        sorted_vertex_ids_per_3_vertex_id = dict.fromkeys([key for key in sorted_cell_ids_per_3_vertex_id])
        for vertex_id, sorted_cell_ids in sorted_cell_ids_per_3_vertex_id.items():
            sorted_vertex_ids_per_3_vertex_id[vertex_id] = np.array(
                [cell_id_to_vertex_index[cid] for cid in sorted_cell_ids])

        sorted_vertex_ids_per_higher_order_vertex_id = dict.fromkeys(
            [key for key in sorted_cell_ids_per_higher_order_vertex_id])
        for vertex_id, sorted_cell_ids in sorted_cell_ids_per_higher_order_vertex_id.items():
            sorted_vertex_ids_per_higher_order_vertex_id[vertex_id] = np.array(
                [cell_id_to_vertex_index[cid] for cid in sorted_cell_ids])

        # Create triangles table from 3-way vertices. Remove triangles that are connected to outside the tissue.
        # (Triangles are created from vertices and not from cell neighbours, because this would conflict with zero-area cells.)
        triangles = self.initialize_triangles_dataframe(sorted_vertex_ids_per_3_vertex_id)

        # Deal with higher order vertices in cell network: produce extra dual vertices and triangles.
        self.process_higher_order_vertices(sorted_vertex_ids_per_higher_order_vertex_id, dual_vertices, triangles)

        # Make sure all dual vertex ids are storred as integers.
        triangles = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].astype(dtype=int)

        return dual_vertices, triangles
    
    
    def table_io_per_frame(self, table_name, frameIdx, network_type = 'cells', action='load', table=[]):
        """ Wrapper for Curved Tissue Miner file io. """
        return MMTool.table_io_per_frame(self.path_hdf5_DB, table_name, frameIdx, network_type, action, table=table)
    
    
    
    
    
    
    def calculate_triangle_deformation_rate(self, frameIdx_this_frame):
        """ DEPRICATED: Calculate triangle deformation tensor.
        
        Given two consecutive frames, calculate the deformation rate for all triangles and the tissue area weighted average.
        First find all trinagles that are present in both frames.
        Only caclulate deformation rate for these triangles.
        """

        frameIdx_next_frame = frameIdx_this_frame + 1

        print('Calculating deformation rate tensors for triangles and tissue from frame', frameIdx_this_frame, 'to', frameIdx_next_frame)

        ### Find the indices of triangles present in both consecutive frames.

        # Load triangles of two consecutive frames: this_frame and next_frame.
        triangles_this_frame = self.table_io_per_frame('triangles', frameIdx_this_frame, self.network_type)
        triangles_next_frame = self.table_io_per_frame('triangles', frameIdx_next_frame, self.network_type)

        # Find all triangle ids that are unique in this frame and have a non-zero area.
        # (Ignore triangles constructed from bonds that are between the same two cells.)
        print('Assign unique ids to all triangles in consecutive frames and find all triangles present in both frames')

        triangles_id_this_frame = triangles_this_frame[triangles_this_frame['area'] > EPSILON]['triangle_id']
        tid_multiplicity = np.array([np.sum(triangles_id_this_frame == tid) for tid in triangles_id_this_frame])
        tid_unique_index = np.where(tid_multiplicity == 1)[0]
        unique_triangle_ids_this_frame = triangles_id_this_frame.iloc[tid_unique_index]

        # Find all triangle ids that are unique in next frame.
        triangles_id_next_frame = triangles_next_frame[triangles_next_frame['area'] > EPSILON]['triangle_id']
        tid_multiplicity = np.array([np.sum(triangles_id_next_frame == tid) for tid in triangles_id_next_frame])
        tid_unique_index = np.where(tid_multiplicity == 1)[0]
        unique_triangle_ids_next_frame = triangles_id_next_frame.iloc[tid_unique_index]

        # Find triangle ids present in both consecutive frames.
        triangle_ids_this_frame_in_next_frame = np.array(
            [(unique_triangle_ids_next_frame == tid_this_frame).any() for tid_this_frame in
             unique_triangle_ids_this_frame])
        triangle_ids_consecutive_frames = unique_triangle_ids_this_frame[triangle_ids_this_frame_in_next_frame]

        # Find indices of the selected triangles in both triangle tables.
        triangles_selected_index_this_frame = triangle_ids_consecutive_frames.index
        triangles_selected_index_next_frame = np.array(
            [np.where(triangles_next_frame['triangle_id'] == selected_tid)[0][0] for selected_tid in
             triangle_ids_consecutive_frames])


        ### Calculate 3x3 triangle deformation rate tensor.
        #Get triangle vectors in this and next frame.
        #triangles_deformation_rate_tensor = \
        #    MMdg.get_triangles_deformation_rate_tensor_Uij(triangles_this_frame.loc[triangles_selected_index_this_frame],
        #                                                   triangles_next_frame.loc[triangles_selected_index_next_frame],
        #                                                   self.delta_t_per_hour[frameIdx_this_frame])

        ### Calculate in-plane triangle deformation rate tensor (shear, elongation change + corotational part and area change).

        triangles_deformation_rate_tensor, tissue_deformation_rate_tensor, triangles_shear_tensor =\
            MM.calculate_triangle_deformation_rate_tensor(triangles_this_frame.loc[triangles_selected_index_this_frame],
                                                       triangles_next_frame.loc[triangles_selected_index_next_frame],
                                                       self.delta_t_per_hour[frameIdx_this_frame],
                                                       self.deformation_rate_interpolation_steps)

        self.table_io_per_frame(table_name='deformation_rate', frameIdx=frameIdx_this_frame,
                              network_type='subcellular_triangles', action='save', table=triangles_deformation_rate_tensor)
        self.table_io_per_frame(table_name='deformation_rate', frameIdx=frameIdx_this_frame,
                              network_type='tissue_average', action='save', table=tissue_deformation_rate_tensor)
        self.table_io_per_frame(table_name='shear_tensor', frameIdx=frameIdx_this_frame,
                              network_type='subcellular_triangles', action='save', table=triangles_shear_tensor)


        ### Calculate cells deformation rate from triangle deformation rates.
        print('Update cells table with deformation rate averaged over the triangles.')
        cells = self.table_io_per_frame('cells', frameIdx_this_frame)

        #Calculate cell shear tensor.
        cells, cells_shear_tensor = MM.calculate_cell_tensor_from_triangle_tensors(cells, triangles_this_frame, triangles_shear_tensor, 'shear_tensor')
        self.table_io_per_frame('shear_tensor', frameIdx_this_frame, 'cells', 'save', cells_shear_tensor)
        
        #Calculate cell growth rate.
        cells = MM.update_cell_table_with_scalar_from_triangle_scalars(cells, triangles_this_frame, triangles_deformation_rate_tensor, 'delta_area')
        self.table_io_per_frame('cells', frameIdx_this_frame, 'save', cells)

        print('Done with triangle and cell deformation rate calculations.')