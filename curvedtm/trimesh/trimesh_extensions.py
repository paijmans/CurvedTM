#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thurday November 28 2019

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

import numpy as np
import pandas as pd
import pwd
import os
import sys

from curvedtm import MMDef, MMTool, MMGeom, MMCurv


def face_adjaceny_edges_order(mesh):
    """
    Determine the order of the vertices in mesh.face_adjaceny_edges compared to the vertex order in the faces.
    Parameters:
     ---------
    mesh : Trimesh.mesh object

    Returns:
    np.array, boolean, len(mesh.face_adjacency_edges)
        For every adjacency edge, reurn whether the order of the vertices is the same as the order of the vertices in the first face in mesh.face_adjacency.
    ------
    """

    # First determine whether the order of the vertices in the adjacency edges match the order of the vertices in the first triangle in
    v1, v2 = mesh.face_adjacency_edges.T
    v3_cellA, v3_cellB = mesh.face_adjacency_unshared.T
    adjacency_edge_vertex_order = np.array([v1, v2, v3_cellA]).T

    #Generate all cyclic permutations of the vertex indices: [v1,v2,v3], [v3,v1,v2], [v2,v3,v1]
    facesA_vertex_order_v1v2v3 = np.array(mesh.faces[mesh.face_adjacency.T[0]])
    facesA_v123_all_cyclic_perm = [np.array([v123, np.roll(v123, 1), np.roll(v123, 2)]) for v123 in facesA_vertex_order_v1v2v3]

    #Find wether adjaceny edge has same permutation as the vertices of cellA
    cellA_vertex_order_matched_adjacency_edge_vertex_order =\
        [(faceA_v123_cyclic_perm == v123_edge).all(axis=1).any() for faceA_v123_cyclic_perm, v123_edge in zip(facesA_v123_all_cyclic_perm, adjacency_edge_vertex_order)]
    cellA_vertex_order_matched_adjacency_edge_vertex_order = np.array(cellA_vertex_order_matched_adjacency_edge_vertex_order)

    edge_wrong_vertex_order = np.invert(cellA_vertex_order_matched_adjacency_edge_vertex_order)

    return edge_wrong_vertex_order


def face_adjacency_signed_angles(mesh):
    """
    Calculate signed angles between the adjacent face normals.

    Parameters:
     ---------
    mesh : Trimesh.mesh object

    Returns:
    np.array, float, len(mesh.face_adjacency_edges)
        A signed version of mesh.face_adjacency_angles, with positive values for adjacent face normals that point away from wach other and negative
        values for face normals that point towards each other.
    ------
    """
    face_adjacency_signed_angles = np.array([-1.0]*len(mesh.face_adjacency))
    face_adjacency_signed_angles[mesh.face_adjacency_convex] = 1.0
    face_adjacency_signed_angles *= mesh.face_adjacency_angles

    return face_adjacency_signed_angles


def create_moviedata_vertex_table(mesh):
    """
    Create pandas DataFrame of the vertices in the moviedata format

    Parameters:
    -----------
    mesh : Trimesh.mesh object

    Returns:
    --------
    vertices : pandas.DataFrame, len(mesh.vertices)
        Vertices positions table in the moviedata format.
        columns = ['x_pos', 'y_pos', 'z_pos', 'angular_defect']
    """
    
    vertices_column_names = ['x_pos', 'y_pos', 'z_pos', 'angular_defect']
    vertices = pd.DataFrame(0.0, columns = vertices_column_names, index = range(len(mesh.vertices)))

    vertices.loc[:, ['x_pos', 'y_pos', 'z_pos']] = mesh.vertices
    
    vertices['angular_defect'] = mesh.vertex_defects
                 
    return vertices


def create_moviedata_triangle_table(mesh):
    """
    Create triangles table in the moviedata format

    Parameters:
    ----------
    mesh : Trimesh.mesh object

    Returns:
    -------
    triangles : pandas.DataFrame, len(mesh.faces)
        Triangles table in the moviedata format.
        columns = ['vertex_id_1', 'vertex_id_2', 'vertex_id_3', 'area' ]
    """

    triangles_column_names = ['vertex_id_1', 'vertex_id_2', 'vertex_id_3', 'area', 'normal_x', 'normal_y', 'normal_z']
    triangles = pd.DataFrame(0.0, columns = triangles_column_names, index = range(len(mesh.faces)))
       
    ## Save traingles vertex ids
    triangles.loc[:, ['vertex_id_1', 'vertex_id_2', 'vertex_id_3']] = mesh.faces
    triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']] = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].astype(int)
    
    ## Save triangles area
    triangles['area'] = mesh.area_faces
    
    ## Save triangles normal vectors
    triangles[['normal_x', 'normal_y', 'normal_z']] = mesh.face_normals
    
    return triangles


def create_moviedata_dbonds_geometry_table(mesh, dbonds, vertices):
    """
    Create pandas DataFrame of bond properties in the moviedata format

    Parameters:
    ----------
    mesh   : Trimesh.mesh object
        The loaded mesh in Trimesh
    dbonds : Pandas.DataFrame
        DCEL corresponding to the mesh
    vertices : Pandas.DataFrame
        Table with vertex positions ['x_pos', 'y_pos', 'z_pos']
        
    Returns:
    -------
    dbonds_geometry : pandas.DataFrame, len(dbonds)
        Table with the geometric properties of each dbond in the mesh 
        columns = ['int_mean_curvature', 'signed_angle', 'length']
    dbonds : Pandas.DataFrame, len(dbonds)
        DCEL table, extended with the column 'face_adjaceny_edges_to_my_dbonds_idx',
        which gives the trimesh index of each dbond.
    """
    
    #First we need a mapping from every dbond index to every face_adjaceny index used in the trimesh package.
    if 'face_adjaceny_edges_to_my_dbonds_idx' not in dbonds.columns:
        dbonds = trimesh_to_my_dbonds_table_translation(mesh, dbonds)
    
    dbonds_geometry_column_names = ['int_mean_curvature', 'signed_angle', 'length']            
    dbonds_geometry = pd.DataFrame(0.0, columns = dbonds_geometry_column_names, index = range(len(dbonds)))
            
    trimesh_to_my_translation_table = dbonds['face_adjaceny_edges_to_my_dbonds_idx'].values

    ### Calculate and store signed angle for each dbond.
    # 2 consecutive entries in 'trimesh_to_my_translation_table' refer the the bond and its conjugate.
    face_adjaceny_signed_angles = face_adjacency_signed_angles(mesh)
    dbonds_geometry.loc[trimesh_to_my_translation_table[::2], 'signed_angle']  = face_adjaceny_signed_angles
    dbonds_geometry.loc[trimesh_to_my_translation_table[1::2], 'signed_angle'] = face_adjaceny_signed_angles    
        
    ### Calculate dbond length
    dbonds_right_vertex_id = dbonds['vertex_id'].values
    dbonds_left_vertex_id  = dbonds.iloc[dbonds['conj_dbond_id']]['vertex_id'].values
    dbonds_right_vertex_pos = vertices[['x_pos', 'y_pos', 'z_pos']].iloc[dbonds_right_vertex_id].values
    dbonds_left_vertex_pos  = vertices[['x_pos', 'y_pos', 'z_pos']].iloc[dbonds_left_vertex_id].values
    
    dbonds_geometry['length'] = np.sqrt(np.sum((dbonds_left_vertex_pos - dbonds_right_vertex_pos)**2, axis=1))
    
    ### Calculate dbonds integrated mean curvature
    dbonds_geometry['int_mean_curvature'] = 0.5 * dbonds_geometry['length'] * dbonds_geometry['signed_angle'] 
    
    return dbonds_geometry, dbonds
                 
    
def trimesh_to_my_dbonds_table_translation(mesh, dbonds):
    """ Find the face_adjaceny index from the trimesh package for every dbond index in the DCEL table.
    
    Since both the DCEL table and the face_adjaceny table use the same vertex indices, we can create
    unique ids for every dbond and face_adjaceny edge from the vertex ids they connect and determine which edges correspond.
    
    Parameters:
    -----------
    mesh : Trimesh.mesh object
        Mesh generated by trimesh package
    dbonds : Pandas.DataFrame
        Doubly connected edge list of mesh.
        
    Returns:
    --------
    dbonds : Pandas.DataFrame
        dbonds table with extra column 'face_adjaceny_edges_to_my_dbonds_idx'
    
    """

    ### Generate unique ids for interal edges defined in trimesh based on vertex ids.
    trimesh_internal_dbonds_id = [[MMTool.pairing_function([vid1, vid2]), MMTool.pairing_function([vid2, vid1])] for vid1, vid2 in mesh.face_adjacency_edges]
    trimesh_internal_dbonds_id = np.array(trimesh_internal_dbonds_id).flatten()

    ### Generate unique ids for internal edges in my dbonds table based on vertex ids.
    dbonds_right_vertex_id = dbonds['vertex_id'].values
    dbonds_left_vertex_id  = dbonds.iloc[dbonds['conj_dbond_id']]['vertex_id'].values
    my_internal_dbonds_id = [MMTool.pairing_function([vid1, vid2]) for vid1, vid2 in zip(dbonds_right_vertex_id, dbonds_left_vertex_id)]
    my_internal_dbonds_id = np.array(my_internal_dbonds_id)

    ### Generate translation table between trimesh and my dbonds table.
    trimesh_to_my_translation_table = [np.where(my_internal_dbonds_id == trimesh_dbond_id)[0][0] for trimesh_dbond_id in trimesh_internal_dbonds_id]
    trimesh_to_my_translation_table = np.array(trimesh_to_my_translation_table)
    
    dbonds['face_adjaceny_edges_to_my_dbonds_idx'] = trimesh_to_my_translation_table
    
    return dbonds