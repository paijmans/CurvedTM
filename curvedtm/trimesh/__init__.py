""" Functions to use in combination of the Python Trimesh package 
    (https://trimsh.org/)
    
    Used for calculating curvatures on triangular surfaces extracted from z-stack images.
"""
from . import trimesh_extensions as trimext