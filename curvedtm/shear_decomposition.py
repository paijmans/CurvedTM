#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wednesday Feb 18 2018

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool, MMGeom, MMCurv, MMdg

def calculate_triangle_transformation_matrix(triangles_this_frame, triangles_next_frame):
    """ Calculate the 3D transformation matrix M in euclidean coordiantes on every triangle.
        Definition: M_{alpha beta} = S'_{alpha beta}.S^{-1}_{alpha beta} =  R'_{alpha beta}.R^{-1}_{alpha beta}
    
    Parameters
    ----------
    triangles_this_frame : pandas.DataFrame, size N
        Triangle vectors in this frame.
        columns = {['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y' ,'dr_13_z']}
    triangles_next_frame : pandas.DataFrame, size N
        Triangle vectors in the next frame.
        columns = {['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y' ,'dr_13_z']}

    Returns
    -------
    triangles_M : numpy.array (float), size N
        Transformation tensor for each triangle.
    """
    
    assert len(triangles_this_frame) == len(triangles_next_frame), "Triangle tables have unequal length."
    
    
    triangles_this_frame_E12 = triangles_this_frame[['dr_12_x', 'dr_12_y', 'dr_12_z']].values
    triangles_this_frame_E13 = triangles_this_frame[['dr_13_x', 'dr_13_y', 'dr_13_z']].values
    triangles_this_frame_N   = triangles_this_frame[['normal_x', 'normal_y', 'normal_z']].values

    triangles_next_frame_E12 = triangles_next_frame[['dr_12_x', 'dr_12_y', 'dr_12_z']].values
    triangles_next_frame_E13 = triangles_next_frame[['dr_13_x', 'dr_13_y', 'dr_13_z']].values
    triangles_next_frame_N   = triangles_next_frame[['normal_x', 'normal_y', 'normal_z']].values
    
    # Convert the 3 lists of vectors into a (3x3) matrix for each triangle, where each column in the matrix is a triangle vector.
    # Eg: R = [E12, E13, N]
    triangles_vecs_this_frame = np.array([triangles_this_frame_E12.T, triangles_this_frame_E13.T, triangles_this_frame_N.T]).T
    triangles_vecs_next_frame = np.array([triangles_next_frame_E12.T, triangles_next_frame_E13.T, triangles_next_frame_N.T]).T
    
    # Invert triangle vector matrix in this frame.
    triangles_vecs_this_frame_inv = np.linalg.inv(triangles_vecs_this_frame)
    
    # Matrix multiply triangle vector matrix of the next frame with inverted triangle vector matrix in this frame.
    triangles_M = [np.dot(R_next, R_this_inv) for R_next, R_this_inv in zip(triangles_vecs_next_frame, triangles_vecs_this_frame_inv)]
    
    return np.array(triangles_M)
    
    
def calculate_triangle_deformation_tensor(triangles_this_frame, triangles_next_frame, delta_t=1.0):
    """ Calculate the 3D deformation matrix U in euclidean coordiantes on every triangle.
        Definition: U_{alpha beta} = Transpose[M_{alpha beta}] - Unity
    
    Parameters
    ----------
    triangles_this_frame : pandas.DataFrame, size N
        Triangle vectors in this frame.
        columns = {['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y' ,'dr_13_z']}
    triangles_next_frame : pandas.DataFrame, size N
        Triangle vectors in the next frame.
        columns = {['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y' ,'dr_13_z']}
    delta_t : float, default: None
        Time step between given frames. If given, deformation rate tensor is returned.

    Returns
    -------
    triangles_U : numpy.array (float), size N
        Deformation [rate]tensor for each triangle.
    """    
    
    assert len(triangles_this_frame) == len(triangles_next_frame), "Triangle tables have unequal length."
    
    N_triangles = len(triangles_this_frame)
    
    # Calculate triangle transformation tensor.
    triangles_M = MS3D.calculate_triangle_transformation_matrix(triangles_this_frame, triangles_next_frame)
    
    #triangles_U = triangles_M - np.array([np.eye(3)]*N_triangles)
    triangles_U = np.transpose(triangles_M,axes=(0,2,1)) - np.array([np.eye(3)]*N_triangles)
    
    #Normalize deformation by time step.
    triangles_U /= delta_t
        
    return triangles_U


def get_triangle_state_parameters_deformation(triangles_this_frame, triangles_next_frame, delta_t=1.0):
    """ Get small changes in the triangle state parameters
    
        Function calculates changes in changes of
        - Triangle elongation norm
        - Triangle angles twophi, theta_x, theta_y and theta_z
        - Triangle area change
    
    Parameters
    ----------
    triangles_this_frame : pandas.DataFrame, size N
        Triangle table in this frame.
        Assuming following columns are present in this and next triangle table:
        columns = {['area', elongation_tensor_norm', 'elongation_tensor_twophi', 'rotation_angle_x', 'rotation_angle_y', 'rotation_angle_z']}
    triangles_next_frame : pandas.DataFrame, size N
        Triangle table in the next frame.
    delta_t : float, default: None
        Time step between given frames. If given, changes in parameters are returned as rates.

    Returns
    -------
    triangles_deform :pandas.DataFrame, size N
        Small changes in state parameters in triangle table.
    """      
    
    assert len(triangles_this_frame) == len(triangles_next_frame), "Triangle tables have unequal length."

    triangle_deform_columns = ['del_area', 'del_elongation_norm', 'del_angle_phi', 'del_elongation_xx', 'del_elongation_xy', 'del_angle_x', 'del_angle_y', 'del_angle_z']
    
    # Calculate change in area
    triangles_delta_area   = triangles_next_frame['area'] - triangles_this_frame['area']
    # Calculate change in eongation norm
    triangles_delta_q_norm = triangles_next_frame['elongation_tensor_norm'] - triangles_this_frame['elongation_tensor_norm']
    # Calculate change in elongation angle twophi
    triangles_delta_phi = np.array(list(map(MMTool.angle_difference, triangles_this_frame['elongation_tensor_twophi'], triangles_next_frame['elongation_tensor_twophi'])))/2.0
    # Calculate change in normal vector angle theta_x
    triangles_delta_thetax = np.array(list(map(MMTool.angle_difference, triangles_this_frame['rotation_angle_x'], triangles_next_frame['rotation_angle_x'])))
    # Calculate change in normal vector angle theta_y
    triangles_delta_thetay = np.array(list(map(MMTool.angle_difference, triangles_this_frame['rotation_angle_y'], triangles_next_frame['rotation_angle_y'])))
    # Calculate change in in-plane orientation theta_z
    triangles_delta_thetaz = np.array(list(map(MMTool.angle_difference, triangles_this_frame['rotation_angle_z'], triangles_next_frame['rotation_angle_z'])))
    # Calculate change in elongation tensor components.
    triangles_this_frame_q = MS3D.get_elongation_tensor(triangles_this_frame, 2)
    triangles_next_frame_q = MS3D.get_elongation_tensor(triangles_next_frame, 2)    
    triangles_delta_q      = triangles_next_frame_q - triangles_this_frame_q
    
    triangles_delta_qxx = triangles_delta_q.T[0][0]
    triangles_delta_qxy = triangles_delta_q.T[0][1]
        
    #Normalize parameter change by time step.
    triangles_delta_area   /= delta_t
    triangles_delta_q_norm /= delta_t
    triangles_delta_phi    /= delta_t
    triangles_delta_thetax /= delta_t
    triangles_delta_thetay /= delta_t
    triangles_delta_thetaz /= delta_t
    triangles_delta_qxx    /= delta_t
    triangles_delta_qxy    /= delta_t
    
    # Create Pandas DataFrame of changes in triangle state parameters. 
    triangle_deform_data = [triangles_delta_area, triangles_delta_q_norm, triangles_delta_phi, triangles_delta_qxx, triangles_delta_qxy, 
                            triangles_delta_thetax, triangles_delta_thetay, triangles_delta_thetaz ]
    triangles_deform = pd.DataFrame(dict(zip(triangle_deform_columns, triangle_deform_data)), index = triangles_this_frame.index)
    
    return triangles_deform
    
    
### Trace part of shear decomposition
def def_trace(triangles, triangles_deform):
    """ Return the trace of the triangle deformation tensor, calculated from changes in the triangle state tensor.
        Definition: Trace[triangle_def] = delta_area / area
              
        Parameters
        ----------
        triangles : Pandas DataFrame, size N
            Triangle table containing state properies ['area']
        triangles_deform : Pandas DataFrame, size N
            Triangle table containing change in triangle state parameters between 2 consecutive frames, containing ['delta_area']
    
        Returns
        -------
        triangles_deformation_trace : numpy array, size N x 1
            Trace of each triangle.    
    """
    
    triangles_deformation_trace = triangles_deform['del_area'] / (triangles['area'] + MMDef.EPSILON)
    
    return triangles_deformation_trace.values
    
    
### Symmetric traceless parts of shear decomposition.
def def_symmetric_traceless_in_plane(triangles_this_frame, triangles_deform):
    """ Return the traceless symmetric in-plane part of the deformation tensor, calculated from changes in the triangle state tensor.
        Definition: delta_q + j + k
              
        Parameters
        ----------
        triangles_this_frame : Pandas DataFrame, size N
            Triangle table containing state properies ['elongation_tensor_norm', 'elongation_tensor_twophi', 'rotation_angle_y']
        triangles_deform : Pandas DataFrame, size N
            Triangle table containing change in triangle state parameters between 2 consecutive frames.
    
        Returns
        -------
        triangles_deformation_trace : numpy array, size N x 1
            Trace of each triangle.    
    """    
    
    # Define usefull aliases.
    del_q_norm    = triangles_deform['del_elongation_norm'].values
    del_angle_phi = triangles_deform['del_angle_phi'].values
    del_q_xx      = triangles_deform['del_elongation_norm'].values
    del_q_xy      = triangles_deform['del_elongation_norm'].values
        
    del_angle_x   = triangles_deform['del_angle_x'].values
    del_angle_y   = triangles_deform['del_angle_y'].values
    del_angle_z   = triangles_deform['del_angle_z'].values
    
    tensor_q = MS3D.get_elongation_tensor(triangles_this_frame, dimension=3)
    q_norm   = triangles_this_frame['elongation_tensor_norm'].values
    
    sin_theta_y = np.sin(triangles_this_frame['rotation_angle_y'].values)
    
    # Generator of rotations along the z-axis.
    Lz = np.array([[0,-1., 0], [1., 0, 0], [0, 0, 0]])
    
    # Calculate tensor perpendicular to triangle elongation, Lz.q. 
    tensor_Lz_q = np.array([np.dot(Lz,q) for q in tensor_q])

    tensor_delta_q = [ np.array([[del_qxx, del_qxy, 0.], [del_qxy, -del_qxx, 0.], [0. ,0., 0.]])
        for del_qxx, del_qxy in zip(triangles_deform['del_elongation_xx'], triangles_deform['del_elongation_xy']) ]
    tensor_delta_q = np.array(tensor_delta_q)
    
    # Calculate factor g
    g = np.sinh(2 * q_norm)/(2 * q_norm + MMDef.EPSILON)
    # Calculate the prefactor of the corotational term j_{ij}
    j_prefac = g * del_angle_z + (1.0 - g) * del_angle_phi
    j_prefac *= -2.0
    # Calculate the prefactor of the corotational term due to out of plane rotations.
    k_prefac = del_angle_x * sin_theta_y * g
    
    # Add contributions proportional to delta_q and Lz.q 
    deformation_symmetric_traceless_in_plane  = tensor_delta_q
    deformation_symmetric_traceless_in_plane += (j_prefac + k_prefac) * tensor_Lz_q
    
    return deformation_symmetric_traceless_in_plane


def def_symmetric_traceless_out_plane(triangles_this_frame, triangles_deform):
    """ Return the traceless symmetric in-plane part of the deformation tensor, calculated from changes in the triangle state tensor.
        Definition: delta_q + j + k
              
        Parameters
        ----------
        triangles_this_frame : Pandas DataFrame, size N
            Triangle table containing state properies ['elongation_tensor_norm', 'elongation_tensor_twophi', 'rotation_angle_y']
        triangles_deform : Pandas DataFrame, size N
            Triangle table containing change in triangle state parameters between 2 consecutive frames.
    
        Returns
        -------
        triangles_deformation_trace : numpy array, size N x 1
            Trace of each triangle.    
    """
    
    # Define usefull aliases.
    del_q_norm    = triangles_deform['del_elongation_norm'].values
    del_angle_phi = triangles_deform['del_angle_phi'].values
    del_q_xx      = triangles_deform['del_elongation_norm'].values
    del_q_xy      = triangles_deform['del_elongation_norm'].values
        
    del_angle_x   = triangles_deform['del_angle_x'].values
    del_angle_y   = triangles_deform['del_angle_y'].values
    del_angle_z   = triangles_deform['del_angle_z'].values
    
    tensor_q = MS3D.get_elongation_tensor(triangles_this_frame, dimension=3)
    q_norm   = triangles_this_frame['elongation_tensor_norm'].values
    
    sin_theta_y = np.sin(triangles_this_frame['rotation_angle_y'].values)
    theta_z     = 
    
    angle_alpha = triangles_this_frame['rotation_angle_z'].values - triangles_this_frame['rotation_angle_twophi'].values
    
    del_angle_x * 
    

### Skew symmetric (anti-symmetric) parts of shear decomposition.
def def_skew_symmetric_in_plane():
    """ Return the trace of the triangle deformation tensor, calculated from changes in the triangle state tensor.
        Definition: Trace[triangle_def] = delta_area / area
              
        Parameters
        ----------
        triangles : Pandas DataFrame, size N
            Triangle table containing state properies ['area']
        triangles_deform : Pandas DataFrame, size N
            Triangle table containing change in triangle state parameters between 2 consecutive frames, containing ['delta_area']
    
        Returns
        -------
        triangles_deformation_trace : numpy array, size N x 1
            Trace of each triangle.    
    """    
    pass

def def_skew_symmetric_out_plane():
    """ Return the trace of the triangle deformation tensor, calculated from changes in the triangle state tensor.
        Definition: Trace[triangle_def] = delta_area / area
              
        Parameters
        ----------
        triangles : Pandas DataFrame, size N
            Triangle table containing state properies ['area']
        triangles_deform : Pandas DataFrame, size N
            Triangle table containing change in triangle state parameters between 2 consecutive frames, containing ['delta_area']
    
        Returns
        -------
        triangles_deformation_trace : numpy array, size N x 1
            Trace of each triangle.    
    """    
    pass


def get_elongation_tensor(triangles, dimension=3):
    
    elongation_xx = triangles['elongation_tensor_norm'] * np.cos(triangles['elongation_tensor_twophi'])
    elongation_xy = triangles['elongation_tensor_norm'] * np.sin(triangles['elongation_tensor_twophi'])
    
    if dimension == 2:
        triangles_qxy = [np.array([[qxx, qxy], [qxy, -qxx]]) for qxx, qxy in zip(elongation_xx, elongation_xy)]
        
    if dimension == 3:
        triangles_qxy = [np.array([[qxx, qxy, 0], [qxy, -qxx, 0], [0, 0, 0]]) for qxx, qxy in zip(elongation_xx, elongation_xy)]
        
    return np.array(triangles_qxy)