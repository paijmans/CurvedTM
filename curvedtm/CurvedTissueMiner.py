#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Friday June 19 2020

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool, MMGeom, MMCurv, TMLoader, Triangulation

class CurvedTissueMiner:
    """ Load Tissue Miner database and create curved triangulation.    
    
    Wrapper for the TissueMinerDBLoader class and the Triangulation class.
    
    Please consult the docstring of these classes for more detailed explanation of the class instantiation parameters.
    
    Parameters:
    -----------
    datapath_root : string
        Path to the Tissue Miner root directory of the movie to load.
    frames_to_process : list like, default: [-1]
        List with indices of movie frames to load. For [-1] it loads all available frames.
    network_type : string, default: subcellular_triangles
        Set the triangulation to use. Examples: ['subcellular_triangles', 'dual_lattice_triangles']
    refresh_DB_tables : [True, False]
        Whether to (re)load the data from the Tissue Miner SQLite database. Has to be true for first load.
    refresh_... = [True, False]
        Whether to recalculate certain parts of the data. Check docstrings of related class for more info.

    Methods:
    --------
    run() : 
        Create and update triangulation in sequence for each frame.
    run_frame(frameIdx) : 
        Create triangulation for specific frame.
    run_parallel(processes_number = 4) : 
        Create triangulation for all frames in parallel.
    get_data_table_names(frameIdx) :
        Get available tables in database.
    load_data_tables(self, frameIdx, tables_name = ['all']) :
        Load specified tables from database.
    merge_tables(self, table_name = 'cells', network_type='cells', frames = [])
        Merge the table from multiple (all) frames into one table.
    """  
    
    def __init__(self, datapath_root, network_type = 'subcellular_triangles',  frames_to_process = [-1],
                 refresh_DB_tables = True, 
                 refresh_fundaments = True,
                 refresh_geometry = True,
                 refresh_triangle_state = True,
                 refresh_cell_state = True):
                        
        ### Initialze TissueMinerDBLoader.
        self.tm_loader = TMLoader.TissueMinerDBLoader(datapath_root, 
                                                      refresh_DB_tables, 
                                                      frames_to_process)
        
        self.frames_to_process = self.tm_loader.frames_to_process

        
        ### Initialze Triangulation.
        self.network_type  = network_type
        self.triangulation = Triangulation.Triangulation(datapath_root, 
                                                         self.network_type, 
                                                         self.frames_to_process,
                                                         refresh_fundaments, 
                                                         refresh_geometry, 
                                                         refresh_triangle_state, 
                                                         refresh_cell_state)
        
        ### Set path to Curved Tissue Miner database
        self.path_hdf5_DB = self.tm_loader.path_hdf5_DB
        
        print('Execute \'run()\',\'run_parallel()\' or \'run_frame(frameIdx)\' to start the Curved Tissue Miner.')

        
    def run(self):
        """ Create and update cell network from sqlite table, processing frames in sequence. """
        self.tm_loader.run()
        self.triangulation.run()
        
            
    def run_frame(self, frameIdx):
        """ Update triangle network for specific frame.
        Run 'update_DB_tables_and_split_by_frame()' to create all frames first!
        
        Parameters:
        -----------
        frameIdx : int
            Index of frame to process.
        """
        self.tm_loader.run_frame(frameIdx)
        self.triangulation.run_frame(frameIdx)
        
        
    def run_parallel(self, processes_number = 4):
        """ Create and update all frames in parallel.
        
        Parameters:
        -----------
        processes_number : int, default: 4
            Numer of processes to run in parallel. Should not be larger than available cores.
        """
        
        self.tm_loader.run_parallel(processes_number)
        
        print('\n\n\n')
        
        self.triangulation.run_parallel(processes_number)

        
    def get_data_table_names(self, frameIdx):
        """ Return names of all tables inside hdf5 file.
        
            Parameters:
            -----------
            frameIdx : int
                Index of required frame.
            
            Returns:
            --------
                HDFStore.keys()
        """
        
        filepath_hdf5 = os.path.join(self.path_hdf5_DB, MMTool.create_hdf5_filename(frameIdx))
        with HDFStore(filepath_hdf5) as file_hdf5_obj:
            return file_hdf5_obj.keys()
        
        
    def load_data_tables(self, frameIdx, tables_name = ['all'], return_type = 'dict'):
        """" Load tables from Curved Tissue Miner data base files and return as dict or list.

       Available tables:
       ['cells', 'vertices', 'dbonds', 'sorted_cell_dbonds_per_frame', 'shear_tensor', 'elongation_tensor', 'deformation_rate', 'curvature_tensor']

        Parameters:
        -----------
        frameIdx : int
            Frame index to load data for.
        tables_name : list like, default: ['all']
            List of table names to load. If ['all'], all available tables are loaded.
        return_type : ['dict', 'list']
            Whether to return the required tables as a dict or list.
        
        Returns:
        --------
        Depends on setting of 'return_type': ['dict', 'list']:
        data_container : [dict, list]
            Dictionary mapping table name to requated table(s) or list of tables.
        """
        
        filepath_hdf5 = os.path.join(self.path_hdf5_DB, MMTool.create_hdf5_filename(frameIdx))
        
        with HDFStore(filepath_hdf5) as file_hdf5_obj:
            
            #print(file_hdf5.info())
            hdf5_tables_name = file_hdf5_obj.keys()
            
            if tables_name[0] == 'all':
                tables_name = hdf5_tables_name
            else:
                for tn in tables_name:
                    if tn not in hdf5_tables_name:
                        print('WARNING:', tn, 'not in table.')
            
            if return_type == 'list':
                data_container = []
                for tn in tables_name:
                    data_container.append(file_hdf5_obj.get(tn))
            else:
                data_container = dict(zip(hdf5_tables_name, []*len(hdf5_tables_name)))
                for tn in tables_name:
                    data_container[tn] = file_hdf5_obj.get(tn) 
        
        return data_container
    
    
    def load_triangles_table(self, frameIdx):
        """ Shorthand for getting triangles faces table. """
        
        return self.load_data_tables(frameIdx, 
                                     tables_name=['/' + self.network_type + '/triangles'],
                                     return_type='list')[0]
    
    
    def load_cells_table(self, frameIdx):
        """ Shorthand for getting cells faces table. """
        
        return self.load_data_tables(frameIdx, 
                                     tables_name=['/cells/cells'],
                                     return_type='list')[0]
    
    
    def merge_tables(self, table_name = 'cells', network_type='cells', frames = []):
        """ Merge output tables over different frames

            Parameters:
            -----------
            table_name : ['cells', 'vertices', 'dbonds', 'sorted_cell_dbonds_per_frame', 'shear_tensor', 'elongation_tensor', 'deformation_rate', 'curvature_tensor']
                Name of the table to load and merge.
            network_type : ['cells', 'dual_lattice_triangles', 'subcellular_triangles', 'cell_tensor', 'triangle_tensor', 'tissue_average']
                Network type the required table is defined on.
            frames : list
                Frame indices to load. Default: merge over all frames.

            Returns:
            --------
            df : Pandas.DataFrame
                Table with tables concatenated over frames. Contains extra column 'frame'.
        """ 
        
        # When no frames are specified, use all available frames.
        if len(frames) == 0:
            frames = self.frames_to_process
        
        return MMTool.merge_tables(self.path_hdf5_DB, frames, table_name, network_type)