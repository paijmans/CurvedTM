#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Monday March 09 2020

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool


def calculate_cell_neighbour_number(sorted_dbonds_per_cell, cell_ids = []):
    """ Calculate the neigbour numbers of the cells given by their cell ids.
    
    Parameters:
    -----------
        sorted_dbonds_per_cell : dict (cell_id, [sorted_dbonds_id])
    
    Returns:
    -------
    neignbour_number_per_cell : Pandas.DataFrame, size M
        Table containing neigbour number of each cell, indexed by cell_id.
    """
    
    if len(cell_ids) == 0:
        cell_ids = sorted_dbonds_per_cell.keys()
    
    cell_id_nn_list = [[cell_id, len(sorted_dbonds_id)] for cell_id, sorted_dbonds_id in sorted_dbonds_per_cell.items()]
    cell_id_nn_list = np.array(cell_id_nn_list)
    
    neignbour_number_per_cell = pd.DataFrame(cell_id_nn_list.T[1], columns=['neighbour_number'], index=cell_id_nn_list.T[0])
    
    return neignbour_number_per_cell


def calculate_cell_centroid(cell_vertices_id, vertices):   
    """ Calculate centroid of single cell face.
        Definition: cell_center = sum(vertices) / len(vertices)
    
        Returns:
        --------
        cell_center : python.array, dim 3x1
            Array with cell center position.
    """    
    return sum( vertices[['x_pos','y_pos','z_pos']].loc[cell_vertices_id].values ) / len(cell_vertices_id)


def calculate_cell_centers(dbonds, vertices, sorted_dbonds_per_cell_id):
    """ Calculate cell centroids of all cells listed in sorted_dbonds_per_cell_id dict.
    
        Returns:
        --------
        cells_center : Pandas.DataFrame, size M
            Table with cell center positions: ['x_pos','y_pos','z_pos']
    """
   
    # Obtain vertex ids for each cell.
    dbonds_vertex_id  = dbonds['vertex_id'].values
    cells_vertices_id = [dbonds_vertex_id[sorted_dbonds_id] for cell_id, sorted_dbonds_id in sorted_dbonds_per_cell_id.items()] 
    
    # Calculate centroid of each cell.
    cells_center_xyz = [calculate_cell_centroid(cell_vertices_id, vertices) for cell_vertices_id in cells_vertices_id]

    # Create dataframe of cell centers indexed by cell id.   
    cells_center = pd.DataFrame(cells_center_xyz, columns = ['x_pos','y_pos','z_pos'], index=sorted_dbonds_per_cell_id.keys())

    return cells_center


# For each cell in cells, calculate its area and mean unit normal vector given the centroid.
def calculate_cell_area_and_unit_normal_vector(vertices, cells, dbonds, sorted_dbonds_per_cell_id):
    """ Calculate the area and unit normal vector of each cell.
    
        Defenitions:
        - cell area: Sum of triangle areas spanned by the cell geometric center and the cell perimeter vertices.
        - cell normal: area weighted mean of the normal vectors on the triangles.
    
        Parameters:
        ----------
        vertices : Pandas.DataFrame, size M
            Vertex positions in the cell network: ['x_pos', 'y_pos', 'z_pos']
        cells : Pandas.DataFrame, size N
            Table with cell properties ['center_x', 'center_y', 'center_z']
        dbonds : Pandas.DataFrame, size O
            Topology of cell network in DCEL form.
        sorted_dbonds_per_cell_id : dict (cell_id, [sorted_dbonds_id]), size N
            Dictionary with cell_id, list of sorted dbonds id - pair.     
    
        Returns:
        --------
        (cell_area, cell_normal) : Tuple of np.array size Nx1 and np.array size Nx3
            Array of cell area and array with cell unit normal [nx, ny, nz].
    """
    Ncells = len(cells)
    
    # Make list of lists with cell vertex ids.
    dbonds_vertex_id = dbonds['vertex_id'].values
    vertices_idx_per_cell_list = [dbonds_vertex_id[sorted_dbonds_per_cell_id[cell_id]] for cell_id in cells.index]
    
    # Make list of lists with cell vertex positions [x, y, z].
    vertices_pos_xyz = vertices[['x_pos', 'y_pos', 'z_pos']].values
    cells_vertices_xyz = [vertices_pos_xyz[vertices_idx_per_cell_list[cell_idx]] for cell_idx in range(Ncells)]

    # Calculate cell area and normal vector.
    cells_normal_vector = [[0., 0., 0.]]*Ncells
    cells_area = [0.]*Ncells
    for cell_idx, vertices_xyz in enumerate(cells_vertices_xyz):

        # cells with only two dbonds have no defined area or normal vector; Assume area and normal are zero.
        if len(vertices_xyz) < 3:
            continue

        cell_center_xyz = cells[['center_x', 'center_y', 'center_z']].iloc[cell_idx].values

        next_vertices_xyz = np.roll(vertices_xyz, -1, axis=0)

        # Find vectors between adjacent vertices (in cc direction)
        inter_vertex_vectors_cc      = next_vertices_xyz - vertices_xyz
        vertex_to_cell_center_vector = cell_center_xyz - vertices_xyz

        cell_triangles_normal_vectors = np.cross(inter_vertex_vectors_cc, vertex_to_cell_center_vector)

        # Calculate cell unit normal vector from the area weighted average of normal vectors on triangles.
        cell_normal_vector     = sum(cell_triangles_normal_vectors)
        cell_normal_vector_mag = np.sqrt(cell_normal_vector.dot(cell_normal_vector))
        cell_normal_vector    /= (cell_normal_vector_mag + MMDef.EPSILON)

        # Calculate cell area from the area of the triangles (half norm of triangle normal vectrors).
        cell_area = 0.5 * sum([np.sqrt(normal.dot(normal)) for normal in cell_triangles_normal_vectors])

        cells_area[cell_idx] = cell_area
        cells_normal_vector[cell_idx] = cell_normal_vector

    return np.array(cells_area), np.array(cells_normal_vector)


# Update triangles table with area and unit normal vector given vertices and table with triangle vertex ids.
def calculate_triangle_area_and_unit_normal_vector(vertices, triangles):
    """ Calculate the area, unit normal and normal rotation angles given the vertex positions.
        
        Area and normal are calculated using the cross vectors of the triangle vectors E12 and E13.
        Triangle vectors E12 = V2 - V1 and E13 = V3 - V1, where V1, V2 and V3 are the 3 vertex position of each triangle.
        
        Rotation angles definition:
        N = Rot_y(theta_y).Rot_x(theta_x).z
            where N is the unit normal on a triangle, Rot_x and Rot_y are the rotation matrices along the x- and y- axis and 
            z is the unit normal along the z-axis.
    
    Parameters
    ----------
    vertices : pandas.DataFrame, size N
        Table containing the vertex positions of the N vertices in the triangle network.
        columns = {['x_pos', 'y_pos', 'z_pos']}
    triangles : pandas.DataFrame, size M
        Table containing the triangle vertex ids of M triangles.
        columns = {['vertex_id_1', 'vertex_id_2', 'vertex_id_3']}

    Returns
    -------
    triangles : pandas.DataFrame, size M
        Triangles table, extenden with columns:
            - Triangle vectors:
            ['dr_12_x', 'dr_12_y', 'dr_12_z', 'dr_13_x', 'dr_13_y' ,'dr_13_z', 'normal_x', 'normal_y', 'normal_z']
            - Triangle area and normal roatation angles:
            ['area', 'rotation_angle_x', 'rotation_angle_y']
    """   
    
    print('Calculating area and unit-normal for all triangles...')

    vertices_positions = vertices[['x_pos', 'y_pos', 'z_pos']].values
    triangles_pos_1 = vertices_positions[triangles['vertex_id_1'].values]
    triangles_pos_2 = vertices_positions[triangles['vertex_id_2'].values]
    triangles_pos_3 = vertices_positions[triangles['vertex_id_3'].values]

    triangles_E12 = triangles_pos_2 - triangles_pos_1
    triangles_E13 = triangles_pos_3 - triangles_pos_1

    triangles_normal_vector      = np.cross(triangles_E12, triangles_E13)
    triangles_normal_magnitude   = np.sqrt(np.sum(triangles_normal_vector**2, axis=1))    
    triangles_unit_normal_vector = np.array([n/(n_mag + MMDef.EPSILON) for n, n_mag in zip(triangles_normal_vector, triangles_normal_magnitude)])
    triangles_unit_normal_vector[triangles_normal_magnitude < MMDef.EPSILON] = np.zeros(3)
    
    # Express the orientation of the normal vector in two angles, for a rotation around the x and y axis.
    # These angles is how much to rotate along the respective axis to rotate the z unit vector to the triangle normal.
    nx, ny, nz = triangles_unit_normal_vector.T
    # 1st definition: Define Theta_x, Theta_y such that: unit_N = RotX_ThetaX . RotY_ThetaY . unit_z
    triangles_normal_ThetaX = -np.arctan2(ny, nz)
    triangles_normal_ThetaY = np.arctan2(nx, np.sqrt(1 - np.multiply(nx, nx)))
    # 2nd definition: Define Theta_x, Theta_y such that: unit_N = RotY_ThetaY . RotX_ThetaX . unit_z
    #triangles_normal_ThetaY = np.arctan2(nx, nz)
    #triangles_normal_ThetaX = -np.arctan2(ny, np.sqrt(1 - np.multiply(ny, ny)))    

    triangles['dr_12_x'] = triangles_E12.T[0]
    triangles['dr_12_y'] = triangles_E12.T[1]
    triangles['dr_12_z'] = triangles_E12.T[2]
    
    triangles['dr_13_x'] = triangles_E13.T[0]
    triangles['dr_13_y'] = triangles_E13.T[1]
    triangles['dr_13_z'] = triangles_E13.T[2]

    triangles['area'] = 0.5 * triangles_normal_magnitude
    
    triangles['normal_x'] = triangles_unit_normal_vector.T[0]
    triangles['normal_y'] = triangles_unit_normal_vector.T[1]
    triangles['normal_z'] = triangles_unit_normal_vector.T[2]
  
    triangles['rotation_angle_x'] = triangles_normal_ThetaX
    triangles['rotation_angle_y'] = triangles_normal_ThetaY

    return triangles


# Given the norm of the symmetric and anti-symmetric part of the
# decomposed state tensor, calculate the elongation norm.
# For zero-area triangles, set elongation to high value 100.
def Qnorm(AabsSq, BabsSq):
    if type(AabsSq) == float or type(AabsSq) == np.float64:
        AabsSq = np.array([AabsSq])
        BabsSq = np.array([BabsSq])

    triangles_Q_norm = np.zeros(len(AabsSq))

    non_zero_area_triangles_idx = np.where(AabsSq - BabsSq  > MMDef.EPSILON)[0]
    zero_area_triangles_idx     = np.where(AabsSq - BabsSq <= MMDef.EPSILON)[0]

    AabsSq = AabsSq[non_zero_area_triangles_idx]
    BabsSq = BabsSq[non_zero_area_triangles_idx]
    triangles_Q_norm[non_zero_area_triangles_idx] = np.arcsinh(np.sqrt(BabsSq) / np.sqrt(AabsSq - BabsSq))

    return triangles_Q_norm


# Split the tensor T into trace, anti-symmetric part and a nematic part: T = A + B_nematic
# Return angles and norms of both tensors.
def splitTensor_angleNorm(T):
    # T = A + B
    # A = [[a,-b],
    #     [b, a]]
    # B = [[c, d],
    #     [d,-c]]
    a = 0.5 * (T[0, 0] + T[1, 1])
    b = 0.5 * (T[1, 0] - T[0, 1])
    c = 0.5 * (T[0, 0] - T[1, 1])
    d = 0.5 * (T[0, 1] + T[1, 0])

    theta = np.arctan2(b, a)
    AabsSq = a * a + b * b

    twophi = theta + np.arctan2(d, c)
    BabsSq = c * c + d * d

    return theta, AabsSq, twophi, BabsSq


# Split the tensor T into trace, anti-symmetric part and a nematic part: T = A + B_nematic
# Return components of both tensors.
def splitTensor_components(T):
    # T = A + B
    # A = [[a,-b],
    #     [b, a]]
    # B = [[c, d],
    #     [d,-c]]
    a = 0.5 * (T[0, 0] + T[1, 1])
    b = 0.5 * (T[1, 0] - T[0, 1])
    c = 0.5 * (T[0, 0] - T[1, 1])
    d = 0.5 * (T[0, 1] + T[1, 0])

    return a, b, c, d


# Given the two in-plane side-vectors of a triangle E12 and E13, calculate the triangle state tensor S = R.Inverse[C].
# Return S decomposed into the norms and angles of its symmetric and anti-symmetric parts.
def calculate_triangle_state_tensor_symmetric_antisymmetric(E12_xy, E13_xy, A0=1):
    
    # Define inverse of equilateral triangle tensor.
    l = np.sqrt(4 * A0 / np.sqrt(3))
    EquiTriangle = [[l, 0.5 * l],
                    [0, 0.5 * np.sqrt(3) * l]]
    EquiTriangleInv = np.linalg.inv(EquiTriangle)

    # Convert triangle vectors into triangle state tensor R.
    triangle_state_tensors_R = np.stack((E12_xy, E13_xy), axis=-1)
    # Convert triangle tensor R in triangle tensor S = R.C^{-1}
    triangle_state_tensors_S = np.array([np.dot(triangleR, EquiTriangleInv) for triangleR in triangle_state_tensors_R])

    # Split triangle tensor S in traceless symmetric part and trace anti-symmetric part: theta, AabsSq, twophi, BabsSq.
    if len(np.shape(triangle_state_tensors_S)) > 2:
        triangles_state_tensor_decomposed = np.array(list(map( splitTensor_angleNorm, triangle_state_tensors_S)))
    else:
        triangles_state_tensor_decomposed = np.array( splitTensor_angleNorm(triangle_state_tensors_S))

    return triangles_state_tensor_decomposed


def rotate_triangles_into_xy_plane(triangles):
    """ Rotate the triangle vectors such that the normal vector in each triangle coincides with the z-axis.
        
    Defenition: E_xy = Rot_x(-theta_x).Rot_y(-theta_y).E_xyz
    Where E_xy is the triangle vector in the xy-plane, E_xyz the same vector but now in an arbitrary plane.
    And Rot the rotation matrices around the x and y axis.
            
    Parameters
    ----------
    triangles : pandas.DataFrame, size N
        Table containing the triangle vectors E12, E13 and the unit normal rotation angles.
        columns = {['dr_12_x', 'dr_12_y', 'dr_12_z'], ['dr_13_x', 'dr_13_y', 'dr_13_z'], ['rotation_angle_x', 'rotation_angle_y']}

    Returns
    -------
    triangles_E12_xy, triangles_E13_xy : tulple (numpy.array, numpy.array), both size N
        2 lists of 2D vectors with E12 and E13 rotated in the xy plane. 
    """
    
    # For all triangles, get triangle vectors E12 and E13 and normal vector orientation.
    triangles_E12_xyz = triangles[['dr_12_x', 'dr_12_y', 'dr_12_z']].values
    triangles_E13_xyz = triangles[['dr_13_x', 'dr_13_y', 'dr_13_z']].values
    triangles_normal_ThetaX = triangles['rotation_angle_x'].values
    triangles_normal_ThetaY = triangles['rotation_angle_y'].values

    # Rotate the triangle vectors into the XY plain.
    # Minus signs for angles because you rotate normal vector towards z-unit vector.
    cosX, sinX = np.cos(-triangles_normal_ThetaX), np.sin(-triangles_normal_ThetaX)
    cosY, sinY = np.cos(-triangles_normal_ThetaY), np.sin(-triangles_normal_ThetaY)

    # Define Rot_x . Rot_y rotation matrix for every triangle.
    Inv_rotation_x = [np.array(((1, 0, 0), (0, cX, -sX), (0, sX, cX))) for cX, sX in zip(cosX, sinX)]
    Inv_rotation_y = [np.array(((cY, 0, sY), (0, 1, 0), (-sY, 0, cY))) for cY, sY in zip(cosY, sinY)]
    triangles_Inverse_RotY_RotX = [np.dot(Inv_RotY, Inv_RotX) for Inv_RotX, Inv_RotY in
                                   zip(Inv_rotation_x, Inv_rotation_y)]

    # Find triangle side vectors in the xy plane.
    triangles_E12_xy = np.array(list(map(np.dot, triangles_Inverse_RotY_RotX, triangles_E12_xyz)))
    triangles_E13_xy = np.array(list(map(np.dot, triangles_Inverse_RotY_RotX, triangles_E13_xyz)))
    # Make 2d vectors out of in-plane vectors.
    triangles_E12_xy = np.stack((triangles_E12_xy.T[0], triangles_E12_xy.T[1]), axis=-1)
    triangles_E13_xy = np.stack((triangles_E13_xy.T[0], triangles_E13_xy.T[1]), axis=-1)

    return triangles_E12_xy, triangles_E13_xy


#Given an array of 3d matrices describing a quantity in the xy-plane,
#function returns matrix transformed to the plane of the triangle.
def transform_matrices_from_xy_plane_to_triangle_plane(matrices_2d, triangles):
    """ Rotate 2d tensors into the plane of the respective triangles.
        
    Rotate all the tensors such that their normal vectors coincides with the triangle normal.
    
    Parameters
    ----------
    matrices_2d : numpy array, size N
        2D tensors (either (2x2) or (1x2)) defined on each triangle.
    triangles : Pandas DataFrame, size N
       Triangle table with columns of rotation angles along the x- and y-axis.
       columns = ['rotation_angle_x', 'rotation_angle_y']

    Returns
    -------
    matrices_3d : numpy array (either (3x3) or (1x3)), size N
        List of 3D tensors defined on the plane of each triangle. 
    """
        
    #Check if input arrays have correct dimensions.
    assert len(matrices_2d) == len(triangles)

    triangles_normal_ThetaX = triangles['rotation_angle_x'].values
    triangles_normal_ThetaY = triangles['rotation_angle_y'].values
    
    # Rotate the triangle vectors into the XY plain.
    cosX, sinX = np.cos(triangles_normal_ThetaX), np.sin(triangles_normal_ThetaX)
    cosY, sinY = np.cos(triangles_normal_ThetaY), np.sin(triangles_normal_ThetaY)

    # Define Rot_x.Rot_y and its inverse rotation matrix for every triangle.
    rotation_x = [np.array(((1, 0, 0), (0, cX, -sX), (0, sX, cX))) for cX, sX in zip(cosX, sinX)]
    rotation_y = [np.array(((cY, 0, sY), (0, 1, 0), (-sY, 0, cY))) for cY, sY in zip(cosY, sinY)]

    triangles_RotX_RotY = [np.dot(RotX, RotY) for RotX, RotY in zip(rotation_x, rotation_y)]
    triangles_Inverse_RotX_RotY = [ np.linalg.inv(RxRy) for RxRy in triangles_RotX_RotY ]

    matrices_3d = np.array([RotRxRy.dot(M2d.dot(RotRxRy_inv))
                            for M2d, RotRxRy, RotRxRy_inv in zip(matrices_2d, triangles_RotX_RotY, triangles_Inverse_RotX_RotY)])
    
    return matrices_3d


#This function calculates the elongation tensor in 3D:
# Rot_x . Rot_y . Rot_z |q| Diag(-1, 1, 0) Inverse( Rot_x . Rot_y . Rot_z ) or
# Rot_x . Rot_y Q_2d . Inverse( Rot_x . Rot_y )
#It adds the elongation norm and orientation to the triangle table.
def calculate_triangle_elongation_tensor(triangles):
    """ Calculate triangle elongation tensors in 3D.
        
    Elongation tensors calculated by rotating the triangles into the xy-plane, applying the 2D definition,
    and rotatig the 2D tensors back into the labframe.
    
    Parameters:
    -----------
    triangles : pandas.DataFrame, length N
        Triangles table containing the triangle vectors.
        
    Returns:
    --------
        triangles : pandas.DataFrame, length N
            Triangles table with added columns elongation_tensor_norm, elongation_tensor_twophi and rotation_angle_z
        triangles_q_3d, numpy.ndarray, shape: N x 3 x 3
            3D elongation tensors expressed in the lab frame.
    """

    print('Calculate elongation tensor for all triangles.')

    triangles_dR12_xy, triangles_dR13_xy = rotate_triangles_into_xy_plane(triangles)

    triangles_state_decomp =  calculate_triangle_state_tensor_symmetric_antisymmetric(triangles_dR12_xy, triangles_dR13_xy)

    # Calculate the norm of nematic tensor and area of all triangles.
    triangles_elongation_norm =  Qnorm(triangles_state_decomp.T[1], triangles_state_decomp.T[3])

    #In-plane angle of nematic is twice its orientation angle.
    elongation_twophi                     = triangles_state_decomp.T[2]
    triangles['rotation_angle_z']         = triangles_state_decomp.T[0]
    triangles['elongation_tensor_norm']   = triangles_elongation_norm
    triangles['elongation_tensor_twophi'] = elongation_twophi

    #For all triangles, calculate the components of the elongation tensor qxx and qxy.
    cos_two_phi, sin_two_phi = np.cos( elongation_twophi ), np.sin( elongation_twophi )
    triangles_q_xx, triangles_q_xy = triangles_elongation_norm * cos_two_phi, triangles_elongation_norm * sin_two_phi

    #Generate the 2d elongation tensors in the xy-plane, written in 3d matrix form.
    triangles_q_2d = [ np.array([[qxx,  qxy, 0.],
                                 [qxy, -qxx, 0.],
                                 [ 0.,   0., 0.]]) for qxx, qxy in zip(triangles_q_xx, triangles_q_xy) ]

    triangles_q_3d =  transform_matrices_from_xy_plane_to_triangle_plane(triangles_q_2d, triangles)

    return triangles, triangles_q_3d


def calculate_triangle_vorticity_vector(triangles_deformation_rate_tensor_Uij):
    """ Calculate the vorticity vector, \omega_i from the deformation rate tensor, v_ij, on every triangle.
        omega^1 = u_23 - u_32
        omega^2 = u_31 - u_13
        omega^3 = u_12 - u_21
        # TODO: Check defintion of levi-civita symol: e_ij = sqrt(g) [[0,1],[-1,0]]
        
        Parameters:
        -----------
        triangles_deformation_rate_tensor_Uij : numpy.ndarray, N x 3 x 3
            Deformation rate tensor in 3D for every triangle.
    
        Returns:
        --------
        triangles_vorticity_vector : numpy.array, N x 3
            Vorticity vector of every triangle.    
    """

    triangles_vorticity = np.array([[0., 0., 0.] * len(triangles_deformation_rate_tensor_Uij)])

    triangles_vorticity_1 = triangles_deformation_rate_tensor_Uij.T[1][2] - triangles_deformation_rate_tensor_Uij.T[2][1]
    triangles_vorticity_2 = triangles_deformation_rate_tensor_Uij.T[2][0] - triangles_deformation_rate_tensor_Uij.T[0][2]
    triangles_vorticity_3 = triangles_deformation_rate_tensor_Uij.T[0][1] - triangles_deformation_rate_tensor_Uij.T[1][0]

    triangles_vorticity_vector = np.array([triangles_vorticity_1, triangles_vorticity_2, triangles_vorticity_3]).T

    return triangles_vorticity_vector


def calculate_triangle_centroid(triangles, triangles_vertices):
    """ Calculate the centroid of every triangle.
    
    Parameters:
    -----------
    triangles : Pandas.DataFrame
        Triangles table with the vertex ids.
    triangles_vertices_xyz : Pandas.DataFrame
        Vertex positions of every vertex in the triangle network.
    
    Returns:
    --------
    triangles_centers_xyz : Numpy.ndarray, shape: N x 3
        Array with all vertex positions, indexed by triangle id.
    """
        
    Ntriangles = len(triangles)
        
    vertices_xyz = triangles_vertices[['x_pos', 'y_pos', 'z_pos']].values
        
    triangles_vertex_ids = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].values.astype(dtype=int).reshape(-1)
        
    triangles_vertices_xyz = vertices_xyz[triangles_vertex_ids].reshape(-1, 3, 3)
    triangles_centers_xyz  = np.sum(triangles_vertices_xyz, axis = 1)
    triangles_centers_xyz /= 3.0
        
    return triangles_centers_xyz












#########################################
### Depricated functions             ###
#########################################


def Calculate_Minkowsi_tensors(triangles, vertices, nu, r, s, patch=[]):
    """ Calculate the minkowski tensor (nu, r, s) on every triangle or coarse grain over a patch.
    
    The Minkowski tensor M_(nu)^(r,s) is defined as
      
        M_(nu)^(r,s) = \int dS G_nu \vec(x)^r x \vec(n)^s
      
    where 0 <= r + s < 3 and 'x' is the outer product of vectors. 
    \vec(x) is the surface position vector and \vec(n) is the surface unit normal vector.
    G_nu for 1, 2, 3 is:  1, mean curvature and Gaussian curvature, respectively.
    
    Parameters:
    -----------
    triangles : Pandas.DataFrame
        Triangles table.
    vertices : Pandas.DataFrame
        Vertices table of the triangulation.
    nu : [0, 1, 2, 3]
        Minkowsi weighting factor index.
    r  : [0, 1, 2]
        Power of the surface position vector.
    s  : [0, 1, 2]
        Power of the surfacec unit normal vector.
    patch : list like, default = []
        List of collections of triangle ids defining one or more patches. If list is empty, all triangles are assumed one patch.
    
    Returns:
    --------
    M : list like
        Minkowsi tensor for every triangle or on every patch.
    """
    assert r < 0, 'Error: r smaller than 0'
    assert s < 0, 'Error: s smaller than 0'
    assert r + s < 3, 'Error: r + s > 2'
    
    ### Set weight factor G_nu for every triangle.
    if nu == 0:
        print('Error: Minkowsi weight for nu=0 not implemented.')
        return []
    elif mu == 1:
        triangles_G_nu = np.ones(len(triangles))
    elif mu == 2:
        triangles_G_nu = triangles['mean_curvature'].values
    elif mu == 3:
        triangles_G_nu = triangles['gaussian_curvature'].values

    if r > 0:
        triangles_x =  calculate_triangle_centroid(triangles, triangles_vertices)
    if s > 0:
        triangles_n = triangles[['normal_x', 'normal_y', 'normal_z']].values
        
    triangles_area = triangles['area'].values
        
    ### Minkowsi scalars:
    if (r == 0) & (s == 0):
        triangles_M = np.ones(len(triangles))
    ### Minkowsi vectors:
    elif (r == 1) != (s == 0):
        if r == 1:
            triangles_M = triangles_x
        if s == 1:
            triangles_M = triangles_n
    ### Minkowsi rank-2 tensors
    elif r == 2:
        triangles_M = np.outer(triangles_x, triangles_x)
    elif s == 2:
        triangles_M = np.outer(triangles_n, triangles_n)
    elif (r == 1) & (s == 1):
        triangles_M = np.outer(triangles_x, triangles_n)
        
    ### Multiply tensors by area and Minkwosi weight.
    triangles_M *= (triangles_G_nu * triangles_area)
    
    ### Intergrate the Minkowsi tensor on every triangles over defined patches. 
    # If len(patch) == 0, assume all triangles in the patch.
    if len(patch) == 0:
        pass
    
    M = np.sum(triangles_M, axis=0)
        
    return M
        
    





# Calculate the shear on each triangle, decompose it into elongation, corotation and correlation
# and calculate tissue wide area weighted averages.
def calculate_triangle_deformation_with_interpolation(E12_xy_this_frame, E13_xy_this_frame, E12_xy_next_frame,
                                                      E13_xy_next_frame, triangles_deformation_rate_tensor, \
                                                      N_interpolation_steps=100):
    """ DEPRICATED - Calculate the shear decomposition of every triangle with linear interpolation of vertex movements.
    
        Parameters:
        -----------
        TODO
        
        Returns:
        --------
        TODO
    
    """
    # Decompose triangle vectors dR12 and dR13 into state properties without checks.
    def calculate_triangle_state_tensor_orientation_area_elongation(E12_xy, E13_xy):
        splitTrianglesS =  calculate_triangle_state_tensor_symmetric_antisymmetric(E12_xy, E13_xy)
        triangles_q_norm = np.array(list(map(lambda S:  Qnorm(*S), splitTrianglesS)))
        triangles_q_xx = triangles_q_norm * np.cos(splitTrianglesS.T[2])
        triangles_q_xy = triangles_q_norm * np.sin(splitTrianglesS.T[2])

        # theta, area, two_phi, q_norm, q_xx, q_xy
        return splitTrianglesS.T[0], splitTrianglesS.T[1] - splitTrianglesS.T[3], splitTrianglesS.T[2],\
               triangles_q_norm, triangles_q_xx, triangles_q_xy

    # Given triangle tensors dR = [dR12, dR13] in this and next frame,
    # calculate the shear tensor between frames and decompose in dArea, dPsi and the shear nematic dnemUxx, dnemUxy.
    def calculate_triangle_shear_tensor(E12_xy_this_frame, E13_xy_this_frame, E12_xy_next_frame, E13_xy_next_frame):
        # Create triangle state matrix [E12, E13]
        E12E13_next_frame = np.array(list(zip(zip(E12_xy_next_frame.T[0], E13_xy_next_frame.T[0]),
                                              zip(E12_xy_next_frame.T[1], E13_xy_next_frame.T[1]))))

        # Invert triangle tensors in this frame.
        # First check if triangles have non-zero determinant, in case of one or two vertex cells.
        #E12E13_this_frame_determinant = E12_xy_this_frame.T[0] * E13_xy_this_frame.T[1] - E13_xy_this_frame.T[0] * E12_xy_this_frame.T[1]
        #zero_determinant_index = np.where(E12E13_this_frame_determinant == 0.0)[0]
        #E12_xy_this_frame.T[0][zero_determinant_index] += MMDef.EPSILON
        #E13_xy_this_frame.T[1][zero_determinant_index] += MMDef.EPSILON

        E12E13_this_frame = np.array(list(zip(zip(E12_xy_this_frame.T[0], E13_xy_this_frame.T[0]),
                                              zip(E12_xy_this_frame.T[1], E13_xy_this_frame.T[1]))))
        E12E13_this_frame_inverse = list(map(np.linalg.inv, E12E13_this_frame))

        # Calculate deformation tensors for the triangles: M = R'.(R)^-1
        triangles_transformation_tensor = list(map(np.dot, E12E13_next_frame, E12E13_this_frame_inverse))
        triangles_transformation_tensor_transposed = list(map(np.transpose, triangles_transformation_tensor))

        # Calculate shear tensor the triangles.
        triangles_shear_tensor = list(
            map(lambda M_transposed: np.subtract(M_transposed, [[1, 0], [0, 1]]), triangles_transformation_tensor_transposed))

        # Decompose shear tensors in a trace, rotation and pure shear (nematic): U = A + Bnem
        # dU = 1/2dUkk * del_ij + dnemU_ij - dpsi * eps_ij
        triangles_shear_tensor_decomposed = np.array(list(map( splitTensor_components, triangles_shear_tensor)))

        delUkk = np.multiply(triangles_shear_tensor_decomposed.T[0], 2)
        delnemUxx = triangles_shear_tensor_decomposed.T[2]
        delnemUxy = triangles_shear_tensor_decomposed.T[3]
        delPsi = np.multiply(triangles_shear_tensor_decomposed.T[1], -1)

        return delUkk, delnemUxx, delnemUxy, delPsi


    print('Performing triangle deformation interpolation for', len(E12_xy_this_frame), 'triangles with', N_interpolation_steps, 'interpolation steps.' )

    #Initialize tissue_deformation_rate_tensor Series.
    tissue_deformation_rate_tensor = pd.Series(0.0, index=MMDef.tissue_deformation_rate_tensor_columns)

    # Calculate the difference between triangle vectors between this and the next movie frame.
    delta_E12_xy = E12_xy_next_frame - E12_xy_this_frame
    delta_E13_xy = E13_xy_next_frame - E13_xy_this_frame
    delta_E12_xy_per_step = delta_E12_xy / N_interpolation_steps
    delta_E13_xy_per_step = delta_E13_xy / N_interpolation_steps

    # Now we linearly interpolate the triangle trajectories into N_interpolation_steps
    prev_E12_xy = E12_xy_this_frame
    prev_E13_xy = E13_xy_this_frame
    prev_theta, prev_area, prev_q_twophi, prev_q_norm, prev_q_xx, prev_q_xy = \
        calculate_triangle_state_tensor_orientation_area_elongation(prev_E12_xy, prev_E13_xy)
    start_total_area = sum(prev_area)
    prev_total_area = start_total_area
    for step in range(1, N_interpolation_steps + 1):
        next_E12_xy = prev_E12_xy + delta_E12_xy_per_step
        next_E13_xy = prev_E13_xy + delta_E13_xy_per_step

        # Find all triangle vectors which should be swapped.
        triangles_normal = np.cross(next_E12_xy, next_E13_xy)
        triangles_wrong_order_index = np.where(triangles_normal < 0)[0]
        # Swap the wrongly ordered triangles.
        temp = next_E12_xy[triangles_wrong_order_index]
        next_E12_xy[triangles_wrong_order_index] = next_E13_xy[triangles_wrong_order_index]
        next_E13_xy[triangles_wrong_order_index] = temp

        ### For all triangles, calculate deformation gradient tensor for this interpolation step.
        # Decompose triangle vectors in the next step into triangle state tensor.
        next_theta, next_area, next_q_twophi, next_q_norm, next_q_xx, next_q_xy = \
            calculate_triangle_state_tensor_orientation_area_elongation(next_E12_xy, next_E13_xy)

        # Decompose triangles transformation tensor u^m_ij into trance, traceless-symmetric and anti-symmetric parts.
        u_kk, u_nem_xx, u_nem_xy, del_psi = calculate_triangle_shear_tensor(prev_E12_xy, prev_E13_xy,
                                                                            next_E12_xy, next_E13_xy)
        # Calculate the mean corotation j_nematic
        del_phi = np.array(list(map(MMTool.angle_difference, prev_q_twophi, next_q_twophi))) / 2.0
        # g = np.sinh(2 * q_mean_norm) / (2 * q_mean_norm)
        # del_nem_j_prefac = -2*(g * del_theta + (1 - g) * del_phi)
        c = np.tanh(2 * prev_q_norm) / (2 * prev_q_norm + MMDef.EPSILON)
        del_nem_j_prefac = -2 * (c * del_psi + (1 - c) * del_phi)
        j_nematic_xx = del_nem_j_prefac * -1 * prev_q_xy
        j_nematic_xy = del_nem_j_prefac * prev_q_xx

        triangles_deformation_rate_tensor['delta_area'] += u_kk
        triangles_deformation_rate_tensor['delta_elongation_xx'] += next_q_xx - prev_q_xx
        triangles_deformation_rate_tensor['delta_elongation_xy'] += next_q_xy - prev_q_xy
        triangles_deformation_rate_tensor['delta_elongation_twophi'] += del_phi
        triangles_deformation_rate_tensor['shear_xx']  += u_nem_xx
        triangles_deformation_rate_tensor['shear_xy']  += u_nem_xy
        triangles_deformation_rate_tensor['delta_psi'] += del_psi
        triangles_deformation_rate_tensor['shear_corotational_xx'] += j_nematic_xx
        triangles_deformation_rate_tensor['shear_corotational_xy'] += j_nematic_xy


        ### Calculate tissue wide area weighted averages of deformation rate tensor.

        # Calculate tissue shear and rotation.
        tissue_deformation_rate_tensor['shear_xx']  += np.sum(u_nem_xx * prev_area) / prev_total_area
        tissue_deformation_rate_tensor['shear_xy']  += np.sum(u_nem_xy * prev_area) / prev_total_area
        tissue_deformation_rate_tensor['delta_psi'] += np.sum(del_psi * prev_area) / prev_total_area
        # tissue_deformation_rate_tensor['shear_corotational_xx'] += ...
        # tissue_deformation_rate_tensor['shear_corotational_xy'] += ...

        # Calculate covariance between elongation and area change.
        tissue_deformation_rate_tensor['covariance_elongation_xx'] += \
            np.sum(u_kk * prev_q_xx * prev_area) / prev_total_area
        tissue_deformation_rate_tensor['covariance_elongation_xy'] += \
            np.sum(u_kk * prev_q_xy * prev_area) / prev_total_area
        # Calculate covariance between corotation and area change.
        tissue_deformation_rate_tensor['covariance_corotation_xx'] += np.sum(j_nematic_xx * prev_area) / prev_total_area
        tissue_deformation_rate_tensor['covariance_corotation_xy'] += np.sum(j_nematic_xy * prev_area) / prev_total_area


        ### Safe next triangle as this triangle for next step.
        prev_theta, prev_area, prev_q_twophi, prev_q_norm, prev_q_xx, prev_q_xy = next_theta, next_area, next_q_twophi, next_q_norm, next_q_xx, next_q_xy
        prev_E12_xy = next_E12_xy
        prev_E13_xy = next_E13_xy
        prev_total_area = sum(next_area)


    tissue_deformation_rate_tensor['delta_area'] = (prev_total_area - start_total_area) / start_total_area

    return triangles_deformation_rate_tensor, tissue_deformation_rate_tensor