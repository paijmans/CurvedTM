""" 
CurvedTM - An extention of Tissue Miner for curved surfaces
===========================================================

** curvedtm ** is a python package to extend the Tissue Miner method 
(https://github.com/mpicbg-scicomp/tissue_miner) to calculate tissue 
and cell properties of nonplanar polygonal cell networks.

Modules within this package
------------------------------------
MMDef                - Definitions of the package.
MMTool               - General tools.
MMGeom               - Functions for calculating geometric properies of the mesh.
MMdg                 - Differential geometry functions.
MMCurv               - Functions for calculating (integrated) curvatures on the mesh.
TMLoader             - Class for loading cell network from Tissue Miner database.
Triangulation        - Class to create triangulation from cell network.
CurvedTissueMiner    - Class to wrap for the cell network and triangulation creators.
MMCoarse             - Functions for coarse graining on the surface and ontaining defects.
trimesh/trimext      - Functions to be used with the Python trimesh package.

Quantities calculated from mesh
-------------------------------
We first load the sqlite database created by Tissue Miner and Tissue Analyser 
and combine it with the tissue heightmaps to extend the vertex positions of 
the cell network from 2D to 3D. Then we create a triangular mesh 
from the curved surface of cell polygons in order to calulate several properties
of the surface and the cells.

For triangles:
  - Area  
  - Unit normal vector
  - Elongation tensor
  - Mean and Gaussian curvature

For cells
  - Area
  - Elongation tensor
  - Unit normal vector
  - Neighbor number
  - Mean and gaussian curvature
  - Integrated curvature tensor
"""

# Import modules from this package
from . import defs as MMDef
from . import tools as MMTool
from . import geometry as MMGeom
from . import diff_geometry as MMdg
from . import curvature as MMCurv
from . import TissueMinerDBLoader as TMLoader
from . import Triangulation as Triangulation
from . import CurvedTissueMiner as CurvedTissueMiner
from . import coarsegraining as MMCoarse
from . import trimesh as trimesh
from . import geometry_creation as geometry_creation

# Aliases
CurvedTM   = CurvedTissueMiner.CurvedTissueMiner
load_table = MMTool.table_io_per_frame