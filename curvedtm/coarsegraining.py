#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thursday July 15 2020

@author: Joris Paijmans - paijmans@pks.mpg.de
"""

# Import the essential python packages
import sys
import os
import pwd
import multiprocessing as mp
import traceback
import matplotlib.pyplot as plt
import matplotlib.collections as collections

import numpy as np
import pandas as pd
from pandas import HDFStore

from curvedtm import MMDef, MMTool, MMGeom, MMCurv

import igraph as ig
from collections import Counter


##########################################################################
#### Functions for obtaining patches with preferred area on surface. #####
##########################################################################
def create_triangles_neighbors_array(triangle_dcel):
    """ Create a list with for each triangle the 3 neighbor ids 
    
    Parameters:
    -----------
    triangle_dcel : Pandas.DataFrame, length N
        Table with DCEL of triangle network.
        
    Returns:
    --------
    triangles_neighbors_id : Numpy.ndarray, N x 3
        Array with for every triangle gives 3 ids of triangle neigbors.
    """

    dcel     = triangle_dcel.copy()
    dcel_int = dcel[dcel['triangle_id'] > -1].set_index('triangle_id').sort_index()

    # Set boundary triangle id. 
    BOUNDARY_TRIANGLE_ID = dcel_int.index.max() + 1
    
    # Get the 3 conjugate dbond ids for each triangle.
    triangles_conj_dbonds_id = dcel_int['conj_dbond_id'].values

    # Get 3 neighboring triangle ids for each triangle.
    triangles_neighbors_id  = dcel.iloc[triangles_conj_dbonds_id]['triangle_id'].values
    
    # Reset boundary triangle id from -1 to len(triangles).
    boundary_triangles_idx   = np.where(triangles_neighbors_id == -1)[0]
    triangles_neighbors_id[boundary_triangles_idx] = BOUNDARY_TRIANGLE_ID

    # Reset array to Nx3
    triangles_neighbors_id = triangles_neighbors_id.reshape(-1, 3)
       
    return triangles_neighbors_id


def create_graph_without_boundary(triangles_neighbors_id):
    """ Create igraph regular Graph object from the triangles neighbour list, where boundary triangles are removed.
    
    This means no edges in the graph that connects a node on the boundary with the 'outide'.
    
    Parameters:
    -----------
    triangles_neighbors_id : List like, size Nx3
        For each triangle, list the 3 triangle neighbour ids.
        
    Returns:
    --------
    g : ipython regular Graph object
        The graph of the triangular mesh.
    """

    BOUNDARY_TRIANGLE_ID = triangles_neighbors_id.flatten().max()
    
    # Filter boundary triangles from triangle neighbors.
    graph_vertices_target_nb = [[target_id for target_id in vertex_target_ids if target_id != BOUNDARY_TRIANGLE_ID] 
                                for vertex_target_ids in triangles_neighbors_id]

    # Create graph source vertex ids.
    graph_vertices_source_nb = [[tid]*len(neighbors_id) for tid, neighbors_id in enumerate(graph_vertices_target_nb)]

    # Create graph source tid -> target id pairs.
    graph_vertices_source_nb = [b for a in graph_vertices_source_nb for b in a]
    graph_vertices_target_nb = [b for a in graph_vertices_target_nb for b in a]

    graph_edges = np.array([graph_vertices_source_nb, graph_vertices_target_nb]).T

    # Create igraph object
    g = ig.Graph(n=BOUNDARY_TRIANGLE_ID, edges=list(graph_edges), directed=False)
    g.vs['id'] = range(len(g.vs))
    
    return g


def create_graph_with_boundary(triangles_neighbors_id):
    """ (WARNING: Not finalized) Create igraph regular Graph object from the triangles neighbour list, including boundary triangles.
    
    TODO:
    There is one boundary triangle id that all triangles at the border refer to. This triangle only refers to itself.
    This should probably be a directed graph. In a undirected graph, this neighborhood is messed up this way.
    
    Parameters:
    -----------
    triangles_neighbors_id : List like, size Nx3
        For each triangle, list the 3 triangle neighbour ids.
        
    Returns:
    --------
    g : ipython regular Graph object
        The graph of the triangular mesh.
    """
    
    BOUNDARY_TRIANGLE_ID = len(triangles_neighbors_id)

    # Create graph target vertex ids.
    graph_vertices_target_wb = np.append(triangles_neighbors_id, np.array([[BOUNDARY_TRIANGLE_ID, BOUNDARY_TRIANGLE_ID, BOUNDARY_TRIANGLE_ID]]), axis=0)
    graph_vertices_target_wb = graph_vertices_target_wb.reshape(-1)

    # Create graph source vertex ids.
    graph_vertices_source_wb = list(range(0, BOUNDARY_TRIANGLE_ID+1))
    graph_vertices_source_wb_3 = np.array([graph_vertices_source_wb, graph_vertices_source_wb, graph_vertices_source_wb]).T.reshape(-1)
    graph_vertices_source_wb_3 = graph_vertices_source_wb_3.reshape(-1)

    # Create graph source tid -> target id pairs.
    graph_edges = np.array([graph_vertices_source_wb_3, graph_vertices_target_wb]).T

    # Create igraph object
    g = ig.Graph(n=BOUNDARY_TRIANGLE_ID+1, edges=list(graph_edges), directed=False)
    g.vs['id'] = range(len(g.vs))
    
    return g


def get_patches_order_with_preferred_area(patches_triangle_id, triangles, g, starting_order = 15, patch_prefered_area = 500):
    """ Obtain the neighborhood order for each patch such that the earea of the patch is closest to the reference area.
    
    Parameters:
    -----------
    patches_triangle_id : Numpy.array, length N
        Triangle id of the triangle centered in each patch.
    triangles : Pandas.DataFrame
        Triangles table with array 'area'
    g : igraph generic Graph object
        Graph of the mesh topology used for the neighborhood search.
    starting_order : int, default: 15
        Neighborhood order to start from for each patch.
    patch_prefered_area : double, default: 500.
        Prefered area of each patch.
    
    Returns:
    --------
    patches_order : Numpy.array, length N, int
        Neighborhood order for each patch such that the area is closest to patch_prefered_area.
    """
    
    N_patches           = len(patches_triangle_id)
    triangles_area      = triangles.area.values
    triangle_mean_area  = triangles_area.mean()

    patches_order        = np.array([starting_order]*N_patches)
    patches_is_converged = np.array([False]*N_patches)
    patches_old_dA     = np.zeros(N_patches) - 10*patch_prefered_area
    patches_new_dA     = np.zeros(N_patches)
    
    while patches_is_converged.sum() != N_patches:
        
        for patch_idx, patch_order in enumerate(patches_order): 
            if patches_is_converged[patch_idx]:
                continue
                
            patch_triangles_id = g.neighborhood(patch_idx, order=patch_order)
            patch_area         = triangles_area[patch_triangles_id].sum()
            patches_new_dA[patch_idx] = patch_area - patch_prefered_area

        patches_is_now_converged = (np.abs(patches_new_dA) + triangle_mean_area >= np.abs(patches_old_dA)) & (np.invert(patches_is_converged))
        patches_order[patches_is_now_converged] -= np.sign(patches_new_dA)[patches_is_now_converged].astype(np.int)

        patches_is_converged[patches_is_now_converged] = True
        patches_not_converged = np.invert(patches_is_converged)

        patches_order[patches_not_converged] -= np.sign(patches_new_dA)[patches_not_converged].astype(np.int)
        patches_old_dA = patches_new_dA.copy()
        
        #print('\rPatches defined:', patches_is_converged.sum(), '/', N_patches, end='')
        
    return patches_order


def get_patches_triangle_ids(patches_triangle_id, patches_order, g):
    """ Get all triangle ids that lie in a patch.
    
    patches_triangle_id : Numpy.array, length N
        Triangle id of the triangle centered in each patch.
    patches_order : numpy.array, length N, int
        Order of the neighborhood for each patch.
    g : igraph generic Graph object
        Graph of the mesh topology used for the neighborhood search.    
    
    Returns:
    --------
    patches_triangles_id, list, length N, list of ints
        List of list with triangle ids that are contained in each patch.
    """

    patches_triangles_id = []
    for patch_id, order in zip(patches_triangle_id, patches_order):

        patch_tids = g.neighborhood(patch_id, order=order)
        patches_triangles_id.append(patch_tids)
        
    return patches_triangles_id


def get_patches_through_mesh_neighborhood(frameIdx, ctm, patches_triangle_id = [], starting_order = 15, patch_prefered_area = 500,
                                          network_type = 'subcellular_triangles', ignore_mesh_boundary=True, return_graph=False):
    """ Get (overlapping) patches of triangle ids centered around the triangles in 'patches_triangle_id' with an area 'patch_prefered_area'.
    
    Parameters:
    -----------
    frameIdx : int
        Index of frame to define patches on.
    ctm : CurvesTissueMiner object
        Used for loading data.
    patches_triangle_id : list like
        List of triangle ids of triangles centered on the patch positions. If not specified, every triangle is an origin.
    starting_order : int, default: 15
        Neighborhood order to start from for each patch.
    patch_prefered_area : double, default: 500.
        Prefered area of each patch.
    ignore_mesh_boundary : [True, False]
        Whether to ignore the boundary or to include dummy triangles such that patches at the boundary are more symmetric.
    return_graph : [False, True]
        Whether to return the created graph.
        
    Returns:
    --------
    patches_triangles_id, list, length N, list of ints
        List of lists with triangle ids that are contained in each patch.
    """
    
    # Load required tables.
    required_tables = ['/subcellular_triangles/dbonds', '/subcellular_triangles/triangles']
    triangle_dbonds, triangles = ctm.load_data_tables(frameIdx, tables_name=required_tables, return_type='list')

    # Find neigbours of all triangles.
    triangles_neighbors_id =  create_triangles_neighbors_array(triangle_dbonds)

    # Create graph from triangle neighbors list.
    if ignore_mesh_boundary:
        g = create_graph_without_boundary(triangles_neighbors_id)
    else:
        g = create_graph_with_boundary(triangles_neighbors_id)

    # If no patches are specified, set every triangle as patch origin.
    if len(patches_triangle_id) == 0:
        patches_triangle_id = triangles.index.values          
        
    # Find the neighborhood order for each patch such that its area is close to preferred area. 
    patches_order = get_patches_order_with_preferred_area(patches_triangle_id, triangles, g, starting_order, patch_prefered_area)

    # Get triangle ids of all triangles inside the patch of given order.
    patches_triangles_id = get_patches_triangle_ids(patches_triangle_id, patches_order, g)
    
    if return_graph:
        return patches_triangles_id, g
    else:
        return patches_triangles_id






###################################################################
#### Functions for to finding the contour around every patch. #####
###################################################################

def smoothen_patches(patches_triangles_id, triangles_neighbors_id):
    """ Remove all triangles from a patch with only one neighbor (rough edges) """
    
    patches_smoothened_triangles_id = [[] for i in range(len(patches_triangles_id))]
    for patch_idx, triangles_id in enumerate(patches_triangles_id):

        d = Counter(triangles_neighbors_id[triangles_id].flatten())
        dk = np.array(list(d.keys()))
        dv = np.array(list(d.values()))

        new_triangles_id = dk[np.where(dv > 1)]
        new_triangles_id = np.intersect1d(new_triangles_id, triangles_id, assume_unique=True)

        patches_smoothened_triangles_id[patch_idx] = new_triangles_id
        
    return patches_smoothened_triangles_id


def get_vertex_id_to_triangles_id(triangles_dbonds_table):
    """ Create a list, indexed by vertex_id, that returns a list of ids of all connected triangles.
    
        Parameters:
        -----------
            triangles_dbonds_table : Pandas.DataFrame
                Table of DCEL of triangle network.
            
        Returns:
        --------
            vertex_id_to_triangle_ids : list of lists    
    """
    
    dcel = triangles_dbonds_table.copy()
    
    vertices_triangles_table = dcel.set_index('vertex_id').sort_index()['triangle_id']

    vertices_id_order_dict = Counter(vertices_triangles_table.index.values)
    vertices_order         = np.array(list(vertices_id_order_dict.values()))

    vertices_order_cumsum = vertices_order.cumsum()
    vertices_order_cumsum = np.append(0, vertices_order_cumsum)

    vertices_triangles        = vertices_triangles_table.values
    vertex_id_to_triangle_ids = [vertices_triangles[start:stop] for start, stop in zip(vertices_order_cumsum[:-1], vertices_order_cumsum[1:])]
    
    return vertex_id_to_triangle_ids


def get_patches_contour_triangles_id_unsorted(patches_triangles_id, triangles_dbonds_table):
    """ Create a list, indexed by patch_id, that returns a list of ids of all triangles on the contour.
    
        Parameters:
        -----------
        patches_triangles_id: list od lists
            For every patch, this contains a list of triangle ids that for the patch.
        triangles_dbonds_table : Pandas.DataFrame
            Table of DCEL for triangle network.
            
        Returns:
        --------
        patches_contour_triangles_id : list of lists    
            For every patch, the triangle ids of the triangles that form a closed contour around every patch.
    """
    
    dcel = triangles_dbonds_table.copy()
    vertex_id_to_triangle_ids = get_vertex_id_to_triangles_id(dcel)    
    
    ### Find the ids of all bonds that lie on the boundary of each patch / neighborhood.
    # Obtain all the bond ids in each patch.
    patches_triangles_nbr        = np.array([len(tids) for tids in patches_triangles_id])
    patches_triangles_nbr_cumsum = patches_triangles_nbr.cumsum()
    patches_triangles_nbr_cumsum = np.append(0, patches_triangles_nbr_cumsum)

    patches_triangles_id_flatt = np.array([tid for tids in patches_triangles_id for tid in tids])
    dcel_internal      = dcel[dcel['triangle_id'] >= 0]
    triangles_bonds_id = dcel_internal['bond_id'].values.reshape(-1,3)

    patches_bonds_id_flatt = triangles_bonds_id[patches_triangles_id_flatt]
    patches_bonds_id = [patches_bonds_id_flatt[start:stop].flatten() for start, stop in zip(patches_triangles_nbr_cumsum[:-1], patches_triangles_nbr_cumsum[1:])]
    
    # Count the number of times a bond id occurs in each patch: 
    # - 1: bond on the patch boundary.
    # - 2: internal bond 
    patches_contour_bonds_id = [[] for i in range(len(patches_triangles_nbr))]
    for patch_idx, patch_bonds_id in enumerate(patches_bonds_id):
        counter_dict = Counter(patch_bonds_id)
        bonds_multiplicty = np.array(list(counter_dict.values()))
        bonds_id = np.array(list(counter_dict.keys()))
        patches_contour_bonds_id[patch_idx] = bonds_id[bonds_multiplicty == 1]

    ### Find vertices, connected to the bonds, that lie on the contour
    dcel['lvertex_id'] = dcel.iloc[dcel['conj_dbond_id'].values]['vertex_id'].values
    bond_id_to_vertex_id = dcel[dcel['bond_id'] == dcel.index.values][['vertex_id', 'lvertex_id', 'bond_id']].set_index('bond_id')
    bond_id_to_vertex_id = bond_id_to_vertex_id.sort_index()
    patches_contour_vertices_id = [np.unique(bond_id_to_vertex_id.loc[bonds_id].values.flatten()) for bonds_id in patches_contour_bonds_id]

    ### Find triangles, connected to the contour vertices and lie in the patch, that form the contour.
    patches_contour_triangles_id = [[] for i in range(len(patches_triangles_nbr))]
    for patch_idx, (patch_contour_vertices_id, patch_triangles_id) in enumerate(zip(patches_contour_vertices_id, patches_triangles_id)):
        contour_triangles_id_inc = [vertex_id_to_triangle_ids[vid] for vid in patch_contour_vertices_id]
        contour_triangles_id_inc = np.unique([ tid for triangles_id in contour_triangles_id_inc for tid in triangles_id ])
        contour_triangles_id_exl = np.intersect1d(contour_triangles_id_inc, patch_triangles_id, assume_unique=True)
        patches_contour_triangles_id[patch_idx] = contour_triangles_id_exl
        
    return patches_contour_triangles_id


def order_contour_triangles_id_counterclockwise(patches_contour_unsorted_triangles_id, g, triangles_center_xyz, triangles_normal):
    """ Order triangles on the conour around each patch in counterclockwise direction """
    
    Npatches = len(patches_contour_unsorted_triangles_id)
    patches_contour_ordered_triangles_id =[[] for i in range(len(patches_contour_unsorted_triangles_id))]
    for patch_id, contour_unsorted_triangles_id in enumerate(patches_contour_unsorted_triangles_id):

        #print('\rContours ordered:', patch_id, '/', Npatches, end='')

        subg = g.subgraph(contour_unsorted_triangles_id, implementation="create_from_scratch")
        subg_tid_to_g_tid = np.array(subg.vs['id'])

        #Define vector from start node to first neighbor.
        for start_id in range(len(subg.vs)):
            
            # Find neighbors of start node.
            start_neighbors_subg_id = np.unique(subg.neighbors(start_id))
            # If only one neighbor, continue to next start node.
            if len(start_neighbors_subg_id) < 2:
                continue

            # Find all paths between 2 consecutive nodes.
            paths     = subg.get_all_simple_paths(start_id, start_neighbors_subg_id[0])
            paths_len = np.array([len(p) for p in paths])
            long_paths_len = paths_len[ paths_len > len(contour_unsorted_triangles_id)/2 ]
            
            if len(long_paths_len) == 0:
                continue
                        
            long_paths = [path for path in paths if len(path) > len(contour_unsorted_triangles_id)/2]
            contour_triangles_ordered_id = subg_tid_to_g_tid[ long_paths[long_paths_len.argmin()] ]
            break
            
        #In case all nodes were considered as starting node, but no suitable path could be obtained.
        if start_id == len(subg.vs)-1:
            print('\nWarning: Could not obtain ordered contour. Unorderd contour length:', len(subg.vs))
            patches_contour_ordered_triangles_id[patch_id] = subg_tid_to_g_tid

            
        ### Determine wheter triangles are ordered in clockwisse or counterclockwise direction.
        start, neighbor1 = contour_triangles_ordered_id[0], contour_triangles_ordered_id[1]
        start_to_neighbor1_vector = triangles_center_xyz[neighbor1] - triangles_center_xyz[start]

        #Define vector from start node to patch centroid. contour_unsorted_triangles_id
        patch_centroid           = np.mean(triangles_center_xyz[contour_unsorted_triangles_id], axis=0)
        start_to_centroid_vector = patch_centroid - triangles_center_xyz[start]

        # Find orientation of the contour from start node to first neighbor node.
        # If contour_orientation = 1, the step start -> neighbor1 is in counterclockwise direction,
        # This means that the longest path we will find (around the whole contour) is in clockwise direction.
        start_centroid_normal = np.cross(start_to_neighbor1_vector, start_to_centroid_vector)
        contour_orientation   = np.sign(np.sum(start_centroid_normal * triangles_normal[start]))
            
        # In case triangle ordering is clockwise, flip ordered triangle array.
        if contour_orientation < 0:
            contour_triangles_ordered_id = np.flip(contour_triangles_ordered_id)
            
        patches_contour_ordered_triangles_id[patch_id] = contour_triangles_ordered_id
    
    return patches_contour_ordered_triangles_id


def rotate_tensors_in_plane(nematic_tensors, nematic_surface_normals, return_rank2=False):
    """ Rotate rank-2 tensors into the xy plane, such that the eigenvector with eigenvalue closest to zero is paralell with the system z-axis.
    
    Parameters:
    -----------
    nematic_tensors : numpy.ndarray
        Array of rank2 tensors  on each triangle
    nematic_surface_normals : Numpy.ndarray
        Normal vector on every triangle (Required to determine orientation of surface).
    return_rank2 : [False, True]
        When True, return 3x3 matrix, else return eigenvector with largest eigenvalue.
        
    Returns:
    --------
    nematic_tensors_inplane : Numpy.ndarray
        For every triangle a 3x3 matrix, representing the rank 2 tensor rotated into the xy-plane.
    """
    
    # Find normal vector to the elongation tensor, make sure orientation is the same as surface
    temp, nematic_tensors_normal = MMTool.calculate_tensor_eigenvalues_and_eigenvector(nematic_tensors, 'abs_min')
    nematic_tensors_normal_orientation = np.sign(np.sum(nematic_surface_normals * nematic_tensors_normal, axis=1))
    nematic_tensors_normal = nematic_tensors_normal_orientation.reshape(-1,1) * nematic_tensors_normal

    # Obtain rotatin angle and vector for rotating elongation tensor normal to the z-axis.
    nematic_tensors_to_z_angle  = np.arccos(nematic_tensors_normal.T[2])
    nematic_tensors_to_z_rotdir = np.cross(nematic_tensors_normal, np.array([0,0,1]))
    nematic_tensors_to_z_rotdir_norm = np.sqrt(np.sum(nematic_tensors_to_z_rotdir**2, axis=1))
    nematic_tensors_to_z_rotdir = nematic_tensors_to_z_rotdir / (nematic_tensors_to_z_rotdir_norm.reshape(-1,1) + MMDef.EPSILON)

    # Rotate tensors in plane.
    if return_rank2:
        return MMTool.rotation(nematic_tensors, nematic_tensors_to_z_angle, nematic_tensors_to_z_rotdir)
    else:
        temp, nematic_tensors_max_eigenvec = MMTool.calculate_tensor_eigenvalues_and_eigenvector(nematic_tensors, 'max')
        return MMTool.rotation(nematic_tensors_max_eigenvec, nematic_tensors_to_z_angle, nematic_tensors_to_z_rotdir)
    

def get_ordered_triangles_on_contours(frameIdx, ctm, patches_triangles_id, g=None):
    """ Get a closed contour of triangles around each patch, ordered in counterclockwise direction wrt surface normal.
    
    Parameters:
    -----------
    frameIdx : int
        Index of the frame to use.
    ctm : CurvedTissueMiner object
        Used to load data.
    patches_triangles_id : list of lists
        For every patch, a list of the triangle ids inside the patch.
    g : None, igraph object
        Graph of the triangle network of the mesh. If none, create new one.
        
    Returns:
    --------
    patches_contour_ordered_triangles_id : list of lists
        For every patch, a list of triangle ids ordered in counterclockwise direction around the contour of the patch.
    """
        
    #print('\rObtain required data...', end='')
    
    required_tables = ['/subcellular_triangles/dbonds',\
                       '/subcellular_triangles/triangles',\
                       '/subcellular_triangles/vertices']
    dcel, triangles, triangle_vertices = ctm.load_data_tables(frameIdx, tables_name=required_tables, return_type='list')
    
    # Get unit normal and centroid for every triangle
    triangles_normal   = triangles[['normal_x', 'normal_y', 'normal_z']].values
    triangles_centroid = MMGeom.calculate_triangle_centroid(triangles, triangle_vertices)
    
    # Create graph for mesh if not given.
    if g==None:
        triangles_neighbors_id = create_triangles_neigbours_array(dcel)
        g =  create_graph_without_boundary(triangles_neighbors_id)
        g.vs['id'] = range(len(g.vs))

    # Find closed contour of triangles around each patch.
    #print('\rObtain triangles on contour of each patch...', end='')
    patches_contour_triangles_id = get_patches_contour_triangles_id_unsorted(patches_triangles_id, dcel)
    #print('\r\n', end='')

    # Order triangles in contour.
    #print('\rOrder triangles on contour of each patch...', end='')
    patches_contour_ordered_triangles_id = order_contour_triangles_id_counterclockwise(patches_contour_triangles_id, g, triangles_centroid, triangles_normal)
    #print('\r\n', end='')
    
    return patches_contour_ordered_triangles_id


def calculate_patch_contour_tensor_rotation_angle(patches_contour_ordered_triangles_id, patches_elongation_tensor, triangles_normal, use_inplane_tensors=True):
    """ Calculate the total angle of rotation of the elongation tensor moving around a directed contour.
        Angle is normalized by 2\pi.
        
    Parameters:
    -----------
    patches_contour_ordered_triangles_id : list of lists
        For every patch, a list of ordered triangle ids around the contour of the patch.
    patches_elongation_tensor : Numpy.ndarray
        For every triangle on the surface, a 3x3 matrix of the coarsegrained elongation tensor.
    triangles_normal : Numpy.ndarray
        The unit normal vector on every triangle.
    use_inplane_tensors : [True, False]
        When True, rotate all tensors in patches_elongation_tensor in the xy plane before calculating total rotation angle.
    
    Returns:
    --------
    patches_contour_elongation_total_rotation : Numpy.array
        Total rotation angle for every contour, normalized by 2\pi.
    """
    
    #print('\rCalculate angular defects on surface...', end='')
    
    # Get unit eigenvector for every triangle elongation tensor. 
    patches_tensor = patches_elongation_tensor.copy()
    if use_inplane_tensors:
        patches_elongation_unit_vector =  rotate_tensors_in_plane(patches_tensor, triangles_normal)
    else:
        patches_elongation_norm, patches_elongation_unit_vector = MMTool.calculate_tensor_eigenvalues_and_eigenvector(patches_tensor)    
    
    patches_contour_elongation_total_rotation = np.zeros(len(patches_contour_ordered_triangles_id))
    for patch_id, contour_triangles_ordered_id in enumerate(patches_contour_ordered_triangles_id):
        
        # Obtain elongation unit vectors and triangle normals on the contour.
        contour_patches_elongation_unit_vector = patches_elongation_unit_vector[contour_triangles_ordered_id]
        contour_triangles_normal               = triangles_normal[contour_triangles_ordered_id]
        contour_patches_elongation_unit_vector_next = np.roll(contour_patches_elongation_unit_vector, shift=-1, axis=0)
        
        #Calculate the orientation of the rotation vector of two consecutive vectors v1,v2 and the surface normal n: (v1 x v2).n > 0 (counterclockwise rotation)
        contour_consecutive_patch_elongation_orientation      = np.cross(contour_patches_elongation_unit_vector, contour_patches_elongation_unit_vector_next)
        #contour_consecutive_patch_elongation_orientation_norm = np.sqrt(np.sum(contour_consecutive_patch_elongation_orientation**2, axis=1))
        #contour_consecutive_patch_elongation_orientation_unit =\
        #    contour_consecutive_patch_elongation_orientation/(contour_consecutive_patch_elongation_orientation_norm.reshape(-1,1) + MMDef.EPSILON)
        contour_consecutive_patch_elongation_rot_direction    = np.sign(np.sum(contour_triangles_normal * contour_consecutive_patch_elongation_orientation, axis=1))

        # Calculate smallest angle between two vectors v1, v2: min(arccos(v1.v2), \pi - arccos(v1.v2)) \in [0, \pi]
        contour_consecutive_patch_elongation_tensor_cos_dphi = np.sum(contour_patches_elongation_unit_vector * contour_patches_elongation_unit_vector_next, axis=1)
        contour_consecutive_patch_elongation_tensor_cos_dphi[np.abs(contour_consecutive_patch_elongation_tensor_cos_dphi) >= 1.] = 1.
        contour_consecutive_patch_elongation_tensor_dphi     = np.arccos(contour_consecutive_patch_elongation_tensor_cos_dphi)
        contour_consecutive_patch_elongation_tensor_dphi     = np.minimum(contour_consecutive_patch_elongation_tensor_dphi,\
                                                                          np.pi - contour_consecutive_patch_elongation_tensor_dphi)

        # Add up directed rotation angles +/- [0,\pi] along the contour
        contour_cumsum_angle   = np.sum(contour_consecutive_patch_elongation_rot_direction * contour_consecutive_patch_elongation_tensor_dphi)
        contour_angular_defect = contour_cumsum_angle / (2*np.pi)

        patches_contour_elongation_total_rotation[patch_id] = contour_angular_defect
        
    #print('\r\n', end='')
        
    return patches_contour_elongation_total_rotation


def calculate_nematic_field_and_defects(frameIdx, datapath_movie_frames, ctm,\
                                        coarsegrain_area, coarsegrain_starting_order, contour_patch_area,\
                                        contour_starting_order, scalars_to_coarsegrain = [], use_inplane_tensors = True, refresh=False):
    """ Calculate coarsegrained nematic field on mesh and the nematic defects in this field.
    
    Paramerers:
    -----------
    frameIdx : int
        Index of frame to process.
    datapath_movie_frames : string
        Datapath to hdf5 database files directory.
    ctm : CurvedTissueMiner object
        For loading data.
    coarsegrain_area : float
        Preferred area of the patches used for coarsegraining elongation tensor on surface. 
    coarsegrain_starting_order : int
        Initial neighborhood order. The closer this is to the median value for the given preferred coarsegrained area, the faster the convergence.
    contour_patch_area : float
        Preferred area of the patches used for creating the contour for finding the nematic defect. 
    contour_starting_order : int
        Initial neighborhood order for contour patches.
    scalars_to_coarsegrain : [], list of strings
        Column names in triangles table for a scalar quantity to be coarse grained using the obtained patches.
    use_inplane_tensors : [True, False]
        When True, rotate all tensors in patches_elongation_tensor in the xy plane before calculating total rotation angle.
    refresh = [False, True]
        Whether to perform smoothing and defect detection if these properties are already present in the loaded tables.
    
    Returns:
    --------
    True
        Nothing is returned, but triangles table is updated with defect angles and a table of coarse grained triangle elongation tensors is saved.
    """

    # Set table name for smooth elongation tensors.
    elongation_tensor_table_path      = '/patches_' + str(coarsegrain_area) + '/elongation_tensor'
    
    # Set column name in triangles table for defect angle output.
    contour_total_rotation_angle_name = 'patches_' + str(coarsegrain_area) + '_contour_' + str(contour_patch_area)
    if use_inplane_tensors:
        contour_total_rotation_angle_name += '_in_plane_tensors'

    ### If the coarsegrained elongation tensor is not available, create coarsegrain patches and average.
    if elongation_tensor_table_path not in ctm.get_data_table_names(frameIdx) or refresh:
        patches_triangles_id =  get_patches_through_mesh_neighborhood(frameIdx, ctm, starting_order = coarsegrain_starting_order,\
                                                                      patch_prefered_area = coarsegrain_area)
        coarse_grain_tensors(frameIdx, datapath_movie_frames, patches_triangles_id, coarsegrain_area, scalars_to_coarsegrain)

    ### Load coarsegrained elongation tensors and normal unit vector on every triangle for surface orientation.
    required_tables = [elongation_tensor_table_path, '/subcellular_triangles/triangles']
    patches_elongation_tensor, triangles = ctm.load_data_tables(frameIdx, tables_name=required_tables, return_type='list')
    patches_elongation_tensor = patches_elongation_tensor.values.reshape(-1,3,3)            
    
    ### Create patches for contour if not created yet.
    if contour_total_rotation_angle_name not in triangles.columns or refresh:
        patches_triangles_id, g = get_patches_through_mesh_neighborhood(frameIdx, ctm, starting_order = contour_starting_order,\
                                                                        patch_prefered_area = contour_patch_area, return_graph=True)
    else:
        return True
    
    ### Get ordered contour of each path     
    patches_contour_ordered_triangles_id = get_ordered_triangles_on_contours(frameIdx, ctm, patches_triangles_id, g)

    ### Calulate the angle of rotation of the tensor around every contour. 
    triangles_normal = triangles[['normal_x', 'normal_y', 'normal_z']].values
    patches_contour_elongation_total_rotation =  calculate_patch_contour_tensor_rotation_angle(patches_contour_ordered_triangles_id,\
                                                                                                       patches_elongation_tensor,\
                                                                                                       triangles_normal,\
                                                                                                       use_inplane_tensors=use_inplane_tensors)    

    ### Save data to disk.
    triangles[contour_total_rotation_angle_name] = patches_contour_elongation_total_rotation
    triangles= MMTool.table_io_per_frame(datapath_movie_frames, table_name = 'triangles', frameIdx = frameIdx, network_type = 'subcellular_triangles',
                                         action = 'save', table = triangles)
    
    return True



#######################################
####          Misc functions      #####
#######################################

def plot_patch(triangles_id, triangle_vertices, triangles, plotIds=False):
    """ Plot set of triangles of the mesh.
    
    Parameters:
    -----------
    triangles_id : list
        List of triangles ids to plot.
    triangle_vertices : Pandas.DataFrame
        Table with vertex positions of triangle network.
    triangles : Pandas.DataFrame
        Table with properties of triangle faces
    plotIds : [False, True]
        Whether to plot the triangle ids on top of triangles.    
    
    Returns:
    -------
    fig, ax : Matplotlib (figure, axis)    
    """
    
    fig, ax = plt.subplots( nrows=1, ncols=1, figsize = (5,5), frameon = True, dpi=300 )
    ax.set_aspect('equal')
    ax.axis('off')  

    triangles_vertices_idx = triangles[['vertex_id_1', 'vertex_id_2', 'vertex_id_3']].values.reshape(-1)
    triangles_vertices_xy  = triangle_vertices[['x_pos', 'y_pos']].iloc[triangles_vertices_idx].values.reshape((-1, 3, 2))

    tx, ty = np.transpose(triangles_vertices_xy[triangles_id], (2,1,0))

    plt.xlim(0.99*tx.min(), 1.01*tx.max())
    plt.ylim(0.99*ty.min(), 1.01*ty.max())

    ### Load triangles for plotting.
    triangleFaces = []
    for triangle_idx in triangles_id:
        triangleFaces.append( triangles_vertices_xy[triangle_idx] )

    triangles_collection = collections.PolyCollection(triangleFaces, edgecolors='red', alpha=1, lw = 1, linestyle='-', zorder = 1)
    ax.add_collection(triangles_collection)
    triangles_collection.set_facecolor(c='plum')
    
    if plotIds:
        triangles_center_xy = np.sum(triangles_vertices_xy[triangles_id], axis=1)/3
        list(map(lambda x, y, txtstr: plt.text(x, y , txtstr, va = 'center', ha = 'center', color='black',  fontsize=3, zorder=13),
                 triangles_center_xy.T[0], triangles_center_xy.T[1], map(str, triangles_id)))  

    return fig, ax


def coarse_grain_tensors(frameIdx, datapath_movie_frames, patches_triangles_id, patch_area, scalars_to_coarsegrain = []):
    """ Coarsegrain tensors over patches.    
    """

    network_name = 'patches_' + str(patch_area)
    
    #scalars_to_coarsegrain = ['elongation_tensor_norm', 'gaussian_curvature',  'mean_curvature', 'area']
    
    triangles_elongation_tensor = MMTool.table_io_per_frame(datapath_movie_frames, 'elongation_tensor', frameIdx, 'subcellular_triangles')
    triangles = MMTool.table_io_per_frame(datapath_movie_frames, 'triangles', frameIdx, 'subcellular_triangles')
       
    # Define patch_id -> patch_triangles_id dict.
    patches_triangles_id_dict = dict(zip(triangles.index.values, patches_triangles_id))
    
    # Calculate area weighted average of the tensors on each patch.
    triangles_smooth_elongation_tensor = MMTool.calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles,\
                                                                                                          triangles_elongation_tensor,\
                                                                                                          patches_triangles_id_dict)
    
# Calculate angle of smooth elongation tensors.
# triangles_normal = triangles[['normal_x', 'normal_y', 'normal_z']].values
# triangles_smooth_elongation_tensor_inplane = rotate_tensors_in_plane(triangles_smooth_elongation_tensor, triangles_normal, return_rank2=True)
# triangles_smooth_elongation_tensor_twophi  = np.arctan2(triangles_neighborhood_elongation_tensor_inplane.T[0][1],\
#                                                         triangles_neighborhood_elongation_tensor_inplane.T[0][0])
# triangles_neighborhood_elongation_tensor_phi = triangles_neighborhood_elongation_tensor_twophi/2
# triangles_neighborhood_elongation_tensor_phi[triangles_neighborhood_elongation_tensor_phi<0] += np.pi 
# triangles[network_name + '_elongation_phi'] = triangles_neighborhood_elongation_tensor_phi
    
    # Calculate the area weighted average of the scalars on each patch.
    for scalar_name in scalars_to_coarsegrain:

        if scalar_name not in triangles.columns:
            continue
            
        triangles_scalar_coarsegrained = MMTool.calculate_area_weighted_mean_tensor_from_triangle_tensors(triangles,\
                                                                                                          triangles[scalar_name].values,\
                                                                                                          patches_triangles_id_dict)
        triangles[network_name + '_' + scalar_name] = triangles_scalar_coarsegrained

    # Save tables.
    MMTool.table_io_per_frame(datapath_movie_frames, table_name = 'elongation_tensor', frameIdx = frameIdx, network_type = network_name,
                              action = 'save', table = pd.DataFrame(triangles_smooth_elongation_tensor.reshape(-1, 9)))
    MMTool.table_io_per_frame(datapath_movie_frames, table_name = 'triangles', frameIdx = frameIdx, network_type = 'subcellular_triangles',
                              action = 'save', table = triangles)